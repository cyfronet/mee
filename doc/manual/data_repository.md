
#### Integration

The Model Execution Environment (MEE) facilitates seamless data access on external data repositories through its API.
Users have the capability to effortlessly download particular files or entire datasets from data repository storage.
Additionally, the environment enables users to upload data generated during simulations using advanced liquid tags in the step scripts.
It's crucial to ensure the accuracy of your Data Repository configuration, since pipelines utilizing the Liquid tags described below may become inoperable.

So far, MEE is supporting the Dataverse and Zenodo data repositories. For more information and usage check sections below.


#### Configuration

In order to use data repository integration features your organization must have its **url address** configured.
To access data you need to provide **API access token** in your user profile.
The data repository access token is configured per organization! That means if you belong to multiple organizations with enabled data repository integration, you have to set up a token in your profile in each of them.

---

# Dataverse

---
Documentation of the Dataverse can be found on the project site: [Dataverse Guide](https://guides.dataverse.org/en/latest/)


#### Dataverse liquid tags

**{% dataverse_file_stage_out filePath persistentID metadata %}**

This tag is used to upload a file located at `filePath` to an existing dataset with the specific `persistentID` at the configured Dataverse instance.

- `filePath`: The path to the file that you want to upload to Dataverse. If file path includes whitespaces, this parameter must be passed as a predefined variable.
- `persistentID`: The identifier for the target dataset in Dataverse. It supports some persistent identifier types, e.g. DOI (Digital Object Identifier) which should be provided in a specific format: "doi:<actual_doi>".
For example, "doi:10.12326/shoulder/23GF4H". Note that DOIs contain only uppercase letters except for the "shoulder" part.
- `metadata`: An optional parameter that can be included in JSON format to provide additional file metadata. <b>(OPTIONAL)</b>
For example:

```json
  {"directoryLabel":"dir1/subdir","categories":["Data"], "restrict":"false"}
```

The JSON attributes and their descriptions can be found at the Dataverse guide:  [Dataverse Add File API](https://guides.dataverse.org/en/latest/api/native-api.html#add-file-api)

**{% dataverse_file_stage_in persistentID target %}**

This tag is used to download a file with the specified `persistentID` from the Dataverse instance.

- `persistentID`: The identifier for the target file in Dataverse, for example, provided in the DOI format (e.g., "doi:10.12326/shoulder/23GF4H").
- `target`: The name of the file where downloaded content should be saved. <b>(OPTIONAL)</b>

**{% dataverse_dataset_stage_in persistentID target %}**

This tag is used to download the content of a dataset, preserving the directory-tree structure, with the specified `persistentID` from the Dataverse instance.
By default, the dataset will be extracted to the current directory - if `target` file is not provided.

- `persistentID`: The identifier for the target dataset in Dataverse, for example, provided in the DOI format (e.g., "doi:10.12326/shoulder/23GF4H").
- `target`: The name of the zip file where the downloaded content should be saved. <b>(OPTIONAL)</b>

***NOTE***

*Please make sure to follow the specified formats for the persistent identifier and metadata (if used) to ensure correct functionality of the liquid tags.*
*Please ensure that you are using quoted file/target paths when they contain spaces.*

---

# Zenodo

---

Documentation of the Zenodo can be found on the project site: [Zenodo Guide](https://help.zenodo.org/)

#### Zenodo liquid tags

**{% zenodo_file_stage_out filePath depositID %}**

This tag is used to upload a file located at `filePath` to an existing data deposition with the specific `depositID` at the configured Zenodo instance.

- `filePath`: The path to the file that you want to upload to Zenodo. If file path includes whitespaces, this parameter must be passed as a predefined variable.
- `depositID`: The identifier for the upload deposition in Zenodo.

**{% zenodo_file_stage_in recordID filename target %}**

This tag is used to download a file with a name `filename` from the dataset/record with `recordID` in Zenodo instance.

- `recordID`: The identifier of the record (dataset) containing the file in Zenodo.
- `filename`: The filename of the file that you want to download.
- `target`: The name of the file where downloaded content should be saved. <b>(OPTIONAL)</b>

**{% zenodo_dataset_stage_in recordID target %}**

This tag is used to download the content of a dataset, preserving the directory-tree structure, with the specified `recordID` from the Zenodo instance.
By default, the dataset will be extracted to the current directory - if `target` file is not provided.

- `recordID`: The identifier of the record (dataset) in Zenodo.
- `target`: The name of the compressed file (e.g. zip) file where the downloaded content should be saved. <b>(OPTIONAL)</b>

***NOTE***
*Please ensure that you are using quoted file/target paths when they contain spaces.*

---

# InvenioRDM

---

Documentation of the InvenioRDM can be found on the project site: [InvenioRDM webpage](https://inveniordm.docs.cern.ch)

#### InvenioRDM liquid tags

**{% invenio_file_stage_out filePath recordId %}**

This tag is used to upload a file located at `filePath` to an existing record with the specific `recordId` at the configured InvenioRDM instance.

- `filePath`: The path to the file that you want to upload to InvenioRDM. If file path includes whitespaces, this parameter must be passed as a predefined variable.
- `recordId`: The identifier for the upload record in InvenioRDM.

**{% invenio_file_stage_in recordID filename target %}**

This tag is used to download a file with a name `filename` from the dataset with `recordID` in InvenioRDM instance.

- `recordID`: The identifier of the record (dataset) containing the file in InvenioRDM.
- `filename`: The filename of the file that you want to download.
- `target`: The name of the file where downloaded content should be saved. <b>(OPTIONAL)</b>

**{% invenio_dataset_stage_in recordID target %}**

This tag is used to download the content of a dataset, preserving the directory-tree structure, with the specified `recordID` from the InvenioRDM instance.
By default, the dataset will be extracted to the current directory - if `target` file is not provided.

- `recordID`: The identifier of the record in InvenioRDM.
- `target`: The name of the compressed file (e.g. zip) file where the downloaded content should be saved. <b>(OPTIONAL)</b>

***NOTE***
*Please ensure that you are using quoted file/target paths when they contain spaces.*
