Instead of creating patient manually, you can add them through `zip` file upload
For upload to succeed, the file has to have correct structure
To create a cohort from zip file, you need to make sure the file has a correct structure.
Inside of a `zip` file you should have a folder for each patient with the folder name equal to patient name eg. 'patient1', 'patient2'
Any files inside of patient folder will be saved as a patient input file.

**Warining**
- If any patient name is not unique (patient with such name already exists), the upload will fail
- If any of patient directories contains a directory, the upload will fail. This
is because Patient inputs have a flat structure.
