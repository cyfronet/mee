# frozen_string_literal: true

class Organization::NotFoundError < NameError; end
