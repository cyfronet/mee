# frozen_string_literal: true

class DataFile
  delegate_missing_to :@blob

  def initialize(blob, organization)
    @blob = blob
    @organization = organization
  end

  def raw
    @blob
  end

  def types
    @data_file_types ||=
      @organization.data_file_types.select { |dft| dft =~ filename.to_s }
  end

  def viewers
    @viewers ||= types.filter_map(&:viewer)
  end
end
