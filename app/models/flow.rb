# frozen_string_literal: true

class Flow < ApplicationRecord
  include Discard::Model, OrganizationUnit

  belongs_to :user, optional: true, default: -> { Current.user }

  before_destroy :destroy_destroyable_steps, prepend: true

  has_many :flow_steps,
           -> { order("position asc") },
           dependent: :destroy,
           inverse_of: :flow

  has_many :steps, through: :flow_steps
  has_many :pipelines, dependent: :destroy

  accepts_nested_attributes_for :flow_steps, allow_destroy: true

  has_rich_text :description

  validates :name, presence: true
  validate :set_position, on: :create
  validates :position, presence: true, numericality: true
  validates :flow_steps, presence: true

  default_scope { order(position: :asc) }

  def self.cleanup
    discarded.reject(&:used?).each(&:destroy)
  end

  def set_position
    if position.blank?
      self.position = Flow
        .where(organization:)
        .maximum(:position).to_i + 1
    end
  end

  def discard_or_destroy
    used? ? discard : destroy
  end

  def destroy_destroyable_steps
    steps.map { |step| step.destroy if step.destroyable?(self) }
  end

  def used?
    pipelines.any?
  end
end
