# frozen_string_literal: true

class GitRepository::GithubClient < GitRepository
  attr_reader :access_token

  def initialize(attrs)
    @access_token = attrs[:private_token]

    super(attrs)
  end

  def versions(force_reload: false)
    Rails.cache.fetch("github-versions/#{@host}/#{@path}",
                      force: force_reload) { fetch_versions }
  rescue
    error_msg = "Requested project #{@path} not found"
    Rails.logger.error(error_msg)
    nil
  end

  def content_and_revision_for(file_path, tag_or_branch)
    ref = client.tree(@path, tag_or_branch).sha
    result = client.contents(@path, path: file_path, ref:)
    [Base64.decode64(result.content), ref]
  rescue
    error_msg = "Requested file #{file_path} not found in branch/tag "\
                "#{tag_or_branch} of project #{@path}"
    Rails.logger.error(error_msg)
    raise ComputationError, error_msg
  end

  private
    def fetch_versions
      rescued_invocation(branches: [], tags: [], default_branch: nil) do
        branches = client.branches(@path, per_page: 100)
        default_branch = client.repo(@path).default_branch
        tags = client.tags(@path, per_page: 100)
        {
          branches: branches.collect(&:name),
          default_branch: [ default_branch ],
          tags: tags.collect(&:name)
        }
      end
    end

    def client
      @client ||= Octokit::Client.new(access_token:)
    end

    def rescued_invocation(default)
      yield
    rescue Octokit::Unauthorized, Octokit::Forbidden
      Rails.logger.error("Github operation invoked with no valid credentials.")
      default
    rescue OpenSSL::SSL::SSLError, SocketError
      Rails.logger.error("Unable to establish Github connection. Check your Github host config.")
      default
    rescue Octokit::BadGateway, Octokit::BadRequest
      Rails.logger.error("Github API returned bad response.")
      default
    end
end
