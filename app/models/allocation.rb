# frozen_string_literal: true

class Allocation < ApplicationRecord
  include TimeScope, OrganizationUnit

  has_many :allocation_typings, dependent: :destroy
  has_many :resource_types, through: :allocation_typings
  has_many :allocation_sites, dependent: :destroy
  has_many :sites, through: :allocation_sites

  validates :name, presence: true
  validates :resource_types, presence: true
  validates :allocation_sites, presence: true
end
