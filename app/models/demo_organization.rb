# frozen_string_literal: true

class DemoOrganization
  include ActiveModel::API
  include ActionDispatch::TestProcess::FixtureFile

  attr_accessor :name, :logo_path, :ssh_key_path,
                :gitlab_api_private_token,
                :github_api_private_token,
                :plgrid_team_id, :allocations

  validates :name, presence: true
  validates :ssh_key_path, presence: true
  validates :gitlab_api_private_token, presence: true
  validates :github_api_private_token, presence: true
  validates :plgrid_team_id, presence: true

  def self.create!(params)
    self.new(params).save!
  end

  def save!
    org = Organization.find_by(name:)
    if org
      STDOUT.puts "#{name} organization already exits. Do you want to reset it [y/n]"
      return unless STDIN.gets.chomp == "y"

      org.destroy!
    end

    Organization.transaction do
      create_organization.tap do |org|
        types = create_data_types(org)
        create_allocations(org)
        create_flows(org, types)
        create_patients(org)
        create_pipelines(org)
        create_artifacts(org)
        create_cohort(org)
      end
    end
  end

  private
    def create_organization
      download_key = File.read(ssh_key_path)
      git_config = GitConfig::Gitlab.new(host: "gitlab.com",
                                        private_token: gitlab_api_private_token,
                                        download_key:)

      Organization.create!(name:,
                          git_config:,
                          plgrid_team_id:).
      tap do |org|
        User.admins.each do |user|
          Membership.create!(user:, organization: org, state: :approved, roles: [:admin])
        end

        if logo_path && File.exist?(logo_path)
          org.logo.attach(io: File.open(logo_path), filename: File.basename(logo_path))
        end
      end
    end

    def create_data_types(org)
      [
        [:demo_numbers, /^numbers.*\.txt$/, "text", "Numbers to be sorted"],
        [:demo_steps, /^steps.*\.txt$/, "text", "Sorting steps"],
        [:demo_animation, /^plot.*\.gif$/, "graphics", "Sorting animation"]
      ].to_h do |record|
        dft = DataFileType.find_or_initialize_by(data_type: record[0], organization: org)
        dft.pattern = record[1].source
        dft.name = record[3]
        dft.viewer = record[2]
        dft.user = user
        dft.save!

        [record[0], dft]
      end
    end

    def create_allocations(org)
      Allocation.create(allocations.each { |h| h[:organization] = org })
    end

    def create_site
      Site.find_or_create_by!(name: "ares",
                              host: "ares.cyfronet.pl", host_key: "ares")
      Site.find_or_create_by!(name: "athena",
                              host: "athena.cyfronet.pl", host_key: "athena")
    end

    def create_flows(org, types)
      create_gitlab_flows(org, types)
      create_github_flows(org, types)
      create_artifact_flow(org)
      create_stage_in_all_flow(org)
    end

    def create_gitlab_flows(org, types)
      owner = org.users.first
      site = Site.find_by host_key: "ares"

      Step.create!(name: "Generate numbers gitlab",
        file: "0_generate_input.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:)
      Step.create!(name: "Sort numbers gitlab",
        file: "1_sort.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_numbers]],
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:)
      Step.create!(name: "Generate animation gitlab",
        file: "2_visualization.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_steps]],
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:)

      Flow.create!(name: "Demo with number generation gitlab",
                  position: 2, organization: org, user: owner, flow_steps_attributes: [
                    { step: Step.first, position: 1 },
                    { step: Step.second, position: 2 },
                    { step: Step.third, position: 3 }
                  ])

      Flow.create!(name: "Demo without number generation gitlab",
                  position: 1, organization: org, user: owner,
                  flow_steps_attributes: [
                    { step: Step.second, position: 1 },
                    { step: Step.third, position: 2 }
                  ])
    end

    def create_github_flows(org, types)
      download_key_file = File.new(ssh_key_path)
      git_config_attributes = {
        host: "github.com",
        git_type: "github",
        private_token: github_api_private_token,
        download_key_file:
      }
      owner = org.users.first
      site = Site.find_by host_key: "ares"

      step1 = Step.create!(name: "Generate numbers github",
        file: "0_generate_input.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:, git_config_attributes:)
      download_key_file.rewind

      step2 = Step.create!(name: "Sort numbers github",
        file: "1_sort.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_numbers]],
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:, git_config_attributes:)
      download_key_file.rewind

      step3 = Step.create!(name: "Generate animation github",
        file: "2_visualization.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_steps]],
        organization: org, resource_type: cpu_resource_type,
        user: owner, site:, git_config_attributes:)

      Flow.create!(name: "Demo with number generation github",
                  position: 2, organization: org, user: owner, flow_steps_attributes: [
                    { step: step1, position: 1 },
                    { step: step2, position: 2 },
                    { step: step3, position: 3 }
                  ])

      Flow.create!(name: "Demo without number generation github",
                  position: 1, organization: org, user: owner,
                  flow_steps_attributes: [
                    { step: step2, position: 1 },
                    { step: step3, position: 2 }
                  ])
    end

    def create_artifact_flow(org)
      owner = org.users.first
      site = Site.find_by(host_key: "ares")
      step = Step.create!(name: "Sort artifact numbers",
        file: "1_artifact_sort.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        organization: org, resource_type: cpu_resource_type,
        user: owner,
        site:)
      Flow.create!(name: "Artifact sort",
                  position: 3, organization: org, user: owner,
                  flow_steps_attributes: [{ step:, position: 1 }])
    end

    def create_stage_in_all_flow(org)
      owner = org.users.first
      site = Site.find_by(host_key: "ares")
      step = Step.create!(name: "Stage in all",
      file: "stage_in_all.job.bash.liquid",
      repository: "cyfronet/mee-demo-steps",
      organization: org, resource_type: cpu_resource_type,
      user: owner, site:)

      Flow.create!(name: "Stage in all test",
                  position: 4, organization: org, user: owner,
                  flow_steps_attributes: [
                    { step:, position: 1 }
                  ])
    end

    def create_patients(org)
      Patient.create!(case_number: "123", organization: org)
      patient = Patient.create!(case_number: "demo", organization: org)
      patient.inputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers1.txt")
      patient.inputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers2.txt")
      Patient.create!(case_number: "patient", organization: org)
    end

    def create_pipelines(org)
      flow = Flow.find_by(name: "Demo with number generation gitlab")

      patient_pipeline1 = Pipeline.create!(mode: :manual, name: "Pipeline 1", flow:, user: org.users.first, runnable: Patient.first)
      Computation.create!(step: flow.steps.first, pipeline: patient_pipeline1, status: :created)
      Computation.create!(step: flow.steps.second, pipeline: patient_pipeline1, status: :created)
      Computation.create!(step: flow.steps.third, pipeline: patient_pipeline1, status: :created)
      patient_pipeline1.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers.txt")
      patient_pipeline1.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers2.txt")

      patient_pipeline2 = Pipeline.create!(mode: :manual, name: "Pipeline 2", flow:, user: org.users.first, runnable: Patient.first)
      Computation.create!(step: flow.steps.first, pipeline: patient_pipeline2, script: "random script", status: :aborted, started_at: Time.current)
      Computation.create!(step: flow.steps.second, pipeline: patient_pipeline2, script: "random script", status: :error, started_at: Time.current)
      Computation.create!(step: flow.steps.third, pipeline: patient_pipeline2, status: :created)
      patient_pipeline2.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers.txt")
      patient_pipeline2.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "numbers2.txt")

      stage_in_all_flow = Flow.find_by(name: "Stage in all test")
      patient_pipeline3 = Pipeline.create!(mode: :manual, name: "Pipeline 3", flow: stage_in_all_flow, user: org.users.first, runnable: Patient.second)
      Computation.create!(step: stage_in_all_flow.steps.first, pipeline: patient_pipeline3, script: "random script", status: :created, started_at: Time.current)
      patient_pipeline3.inputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "input_numbers.txt")
      patient_pipeline3.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "output_numbers.txt")

      org_pipeline = Pipeline.create!(mode: :manual, name: "Org Pipeline", flow:, user: org.users.first, runnable: org)
      Computation.create!(step: flow.steps.first, pipeline: org_pipeline, status: :created)
      Computation.create!(step: flow.steps.second, pipeline: org_pipeline, status: :created)
      Computation.create!(step: flow.steps.third, pipeline: org_pipeline, status: :created)

      Computation.find_each do |computation|
        ParameterValue::ModelVersion.create(parameter: computation.step.parameter_for("tag-or-branch"), computation:)
      end
    end

    def create_artifacts(org)
      org.artifacts.attach(io: File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "artifact1.txt")
      org.artifacts.attach(io: File.open(Rails.root.join("test/fixtures/files/numbers.txt")), filename: "artifact2.txt")
    end

    def create_cohort(org)
      Cohort.create!(name: "Cohort", organization: org, patients: Patient.all)
    end
    def cpu_resource_type
      @cpu_resource_type ||= ResourceType.find_or_create_by!(name: "cpu")
    end

    def gpu_resource_type
      @gpu_resource_type ||= ResourceType.find_or_create_by!(name: "gpu")
    end

    def user
      @user ||= User.admins.first
    end
end
