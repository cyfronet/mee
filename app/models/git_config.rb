# frozen_string_literal: true

class GitConfig
  extend ActiveModel::Naming
  include ActiveModel::Model
  include ActiveModel::Attributes
  include ActiveModel::Validations

  attribute :host, :string
  attribute :download_key, :string

  validates :host, presence: true
  validates :download_key, presence: true

  validate do
    calculate_download_key_fingerprint
  rescue
    errors.add(:download_key, "is not a valid ssh key")
  end

  def download_key_file=(file)
    if file.present?
      self.download_key = file.read
    end
  end

  def download_key_fingerprint
    calculate_download_key_fingerprint
  rescue
  end

  def git_type
    self.class.git_type
  end

  def change_to_class(new_git_config_class)
    keepers = new_git_config_class.attribute_names
    attrs = GitConfig.dump(self).slice(*keepers)
    new_git_config_class.new(attrs)
  end

  def persisted?
    false
  end

  class << self
    def by_type(git_type)
      types_classes.find { |type| type.git_type == git_type }
    end

    def types
      types_classes.map(&:git_type)
    end

    def git_type
      model_name.element
    end

    def load(hsh)
      if hsh.present?
        git_config_class = by_type(hsh.delete("git_type"))
        keepers = git_config_class&.attribute_names
        keepers << :download_key_file

        git_config_class&.new(hsh.slice(*keepers))
      end
    end

    def dump(object)
      if object
        ActiveSupport::HashWithIndifferentAccess
          .new(object.attributes.merge({ git_type: object.git_type })).compact
      end
    end

    private
      def types_classes
        [GitConfig::Gitlab, GitConfig::Github, GitConfig::Native]
      end
  end

  private
    def calculate_download_key_fingerprint
      if download_key.present?
        SSHData::PrivateKey.parse(download_key).first.public_key.fingerprint(md5: true)
      end
    end
end
