# frozen_string_literal: true

class ParameterValue::Select < ParameterValue
  store_accessor :value_store, :value

  validate :value_inclusion, if: :parameter
  delegate :values, to: :parameter, allow_nil: true

  def permitted_params
    [:value]
  end

  def value
    super || parameter&.default_value
  end

  def value_inclusion
    allowed_values = values.map(&:value)
    errors.add(:value, "is not included in the list") unless [*value].all? { |val| allowed_values.include?(val) }
  end

  def blank?
    value.blank?
  end
end
