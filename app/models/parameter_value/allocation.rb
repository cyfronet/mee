# frozen_string_literal: true

class ParameterValue::Allocation < ParameterValue
  store_accessor :value_store, :value

  validates :value, inclusion: {
    in: ->(obj) { obj.parameter.matching_allocations.map(&:name) },
  }, if: :validate_allocation?

  validates :value, presence: true

  def permitted_params
    [:value]
  end

  def value
    super || parameter&.default_value
  end

  def blank?
    value.blank?
  end


  def to_s
    value
  end

  private
    def validate_allocation?
      parameter && value.present?
    end
end
