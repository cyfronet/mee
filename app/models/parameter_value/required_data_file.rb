# frozen_string_literal: true

class ParameterValue::RequiredDataFile < ParameterValue
  store_accessor :value_store, :data_file_id, :data_file_type_id

  attribute :data_file_id, :integer
  validates :data_file_id, presence: true

  def initialize(attrs)
    attrs[:data_file_type_id] = attrs["parameter"].data_file_type.id if attrs["parameter"]

    super(attrs)
  end

  def permitted_params
    [:data_file_id]
  end

  def data_files
    parameter.data_files
  end

  attr_writer :parameter
  def parameter
    @parameter ||= load_parameter
  end

  def value
    data_file&.filename&.to_s
  end

  def data_file
    ActiveStorage::Blob.find_by(id: data_file_id)
  end

  def blank?
    data_file_id.blank?
  end

  def to_s
    data_file_id
  end

  private
    def load_parameter
      if @dft = DataFileType.find_by(id: data_file_type_id)
        Parameter::RequiredDataFile.new(data_file_type: @dft, pipeline: computation.pipeline)
      end
    end
end
