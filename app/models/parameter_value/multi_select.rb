# frozen_string_literal: true

class ParameterValue::MultiSelect < ParameterValue::Select
  def permitted_params
    { value: [] }
  end

  def to_bash
    "(#{value.map { |item| "\"#{item}\"" }.join(' ')})"
  end
end
