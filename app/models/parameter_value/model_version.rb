# frozen_string_literal: true

class ParameterValue::ModelVersion < ParameterValue
  store_accessor :value_store, :version

  validates :version, inclusion: {
    in: ->(obj) { obj.parameter.versions.values.flatten },
    message: "Please select correct model version"
  }, if: :validate_version?

  validates :version, presence: true

  def value
    version
  end

  def version
    super || parameter&.default_value
  end

  def permitted_params
    [:version]
  end

  def blank?
    version.blank?
  end

  def to_s
    version
  end

  private
    def validate_version?
      parameter && version.present?
    end
end
