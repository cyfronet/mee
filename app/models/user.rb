# frozen_string_literal: true

class User < ApplicationRecord
  include User::Account
  include User::Plgrid

  include User::Organizations
  include User::Pipelines
  include User::Flows
  include User::Steps

  def credentials_valid?
    ssh_credentials.valid?
  end

  def hpc_client(site: nil)
    Rails.configuration.hpc_client.constantize.new(host: site&.host, user: self)
  end


  private
    def ssh_credentials
      SshCredentials.new(ssh_key, ssh_certificate)
    end
end
