# frozen_string_literal: true

module Liquid
  module DataRepository
    class Dataverse::Entry < Entry
      def initialize(organization, user)
        super(organization, user)
      end

      def dataset_url(persistent_id)
        "#{@url}/api/access/dataset/:persistentId/?persistentId=#{persistent_id}"
      end

      def datafile_url(persistent_id)
        "#{@url}/api/access/datafile/:persistentId/?persistentId=#{persistent_id}"
      end

      def add_datafile_url(persistent_id)
        "#{@url}/api/datasets/:persistentId/add?persistentId=#{persistent_id}"
      end
    end
  end
end
