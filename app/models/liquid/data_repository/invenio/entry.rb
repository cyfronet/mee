# frozen_string_literal: true

module Liquid
  module DataRepository
    class Invenio::Entry < Entry
      def initialize(organization, user)
        super(organization, user)
      end

      def check_access_url(record_id)
        "#{@url}/api/records/#{record_id}"
      end

      def public_dataset_url(record_id)
        "#{@url}/api/records/#{record_id}/files-archive"
      end

      def draft_dataset_url(record_id)
        "#{@url}/api/records/#{record_id}/draft/files-archive"
      end

      def public_datafile_url(record_id, filename)
        "#{@url}/api/records/#{record_id}/files/#{filename}/content"
      end

      def draft_datafile_url(record_id, filename)
        "#{@url}/api/records/#{record_id}/draft/files/#{filename}/content"
      end

      def draft_files(record_id)
        "#{@url}/api/records/#{record_id}/draft/files"
      end
    end
  end
end
