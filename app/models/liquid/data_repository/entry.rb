# frozen_string_literal: true

module Liquid
  module DataRepository
    class Entry
      include ActiveModel::Validations

      attr_reader :token
      attr_reader :url

      validates :url, presence: { message: ::I18n.t("data_repository.missing_config") }
      validates :token, presence: { message: ::I18n.t("data_repository.missing_token") }
      validate :validate_entry_usage

      def initialize(organization, user)
        @organization = organization
        @user = user
        @type, @url, @token = data_repository_config
      end

      def type
        self.class.module_parent
      end

      def membership
        @user.memberships.detect { |m| m.organization_id == @organization.id }
      end

      def data_repository_config
        config = @organization.data_repository_config
        type = config.nil? ? nil : config.type
        url = config.nil? ? nil : config.url.delete_suffix("/")

        token = membership.data_repository_token
        [type, url, token]
      end

      def validate_entry_usage
        if @type != type.to_s.demodulize.downcase
          errors.add(:entry, ::I18n.t("data_repository.bad_usage"))
        end
      end
    end
  end
end
