# frozen_string_literal: true

class Current < ActiveSupport::CurrentAttributes
  attribute :user
  attribute :organization
  attribute :membership

  def user=(user)
    super
    find_membership(user, organization)
  end

  def organization=(organization)
    super
    find_membership(user, organization)
  end

  private
    def find_membership(user, organization)
      self.membership = organization.memberships.find_by(user:) if user && organization
    end
end
