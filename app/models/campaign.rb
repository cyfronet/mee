# frozen_string_literal: true

class Campaign < ApplicationRecord
  include OrganizationUnit

  belongs_to :cohort, counter_cache: :campaigns_count, optional: true
  has_many :pipelines, dependent: :destroy

  broadcasts_refreshes

  before_create do
    self.desired_pipelines = cohort.patients.size
  end

  after_touch :update_status!

  enum :status, { initializing: 0, created: 1, running: 2,
                  finished: 3, creating_pipelines_failed: 4 }

  validates :name, presence: true,
            uniqueness: { scope: :cohort_id, case_sensitive: true },
            format: { with: /\A[a-zA-Z0-9_]+\z/,
                      message: I18n.t("campaigns.errors.name") }
  validates_each :cohort, on: :create do |record, attr, cohort|
    record.errors.add(:cohort_id, I18n.t("campaigns.errors.cohort_doesnt_exist")) unless cohort
    record.errors.add(:cohort_id, I18n.t("campaigns.errors.empty_cohort")) unless cohort&.patients.present?
  end

  def run
    running!
    pipelines.each { |pipeline| ::Pipelines::StartRunnableJob.perform_later(pipeline) }
  end

  def update_status!
    update(status: Campaign::StatusCalculator.new(self).calculate)
  end

  def add_failed
    increment!(:failed_pipelines)
    update_status!
  end
end
