# frozen_string_literal: true

module User::Account
  extend ActiveSupport::Concern

  included do
    include Gravtastic
    gravtastic default: "mm"

    include RoleModel
    roles :admin

    attribute :terms, :boolean, default: false

    validates :first_name, presence: true
    validates :last_name, presence: true
    validates :email, presence: true

    validates :terms, acceptance: { accept: true }, on: :create
  end

  class_methods do
    def admins
      where(any_role(:admin))
    end

    def any_role(*roles)
      (arel_table[:roles_mask] & mask_for(roles)).gt(0)
    end
  end

  def name
    "#{first_name} #{last_name}"
  end
end
