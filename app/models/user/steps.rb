# frozen_string_literal: true

module User::Steps
  extend ActiveSupport::Concern

  included do
    has_many :steps, dependent: :nullify
  end
end
