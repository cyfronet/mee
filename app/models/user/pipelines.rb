# frozen_string_literal: true

module User::Pipelines
  extend ActiveSupport::Concern

  included do
    has_many :pipelines
    has_many :computations, -> { unscope(:order) }, through: :pipelines
  end

  class_methods do
    def with_submitted_computations
      joins(pipelines: :computations)
        .where(computations: { status: [:queued, :running] })
        .distinct
    end
  end
end
