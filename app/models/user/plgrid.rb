# frozen_string_literal: true

module User::Plgrid
  extend ActiveSupport::Concern

  included do
    encrypts :ssh_key, :ssh_certificate
  end

  class_methods do
    def from_plgrid_omniauth(auth)
      find_or_initialize_by(plgrid_login: auth.info.nickname).tap do |user|
        set_new_user_attrs(auth, user)

        user.credentials_expired_at = nil
        user.fetch_short_lived_ssh_credentials!(auth.dig("credentials", "token"))
        user.manage_memberships(auth.dig("extra", "raw_info", "groups"))
      end
    end

    def set_new_user_attrs(auth, user)
      user.email = auth.info.email
      name_elements = auth.info.name.split(" ")
      user.first_name = name_elements[0]
      user.last_name = name_elements[1..-1].join(" ")
      user.terms = true
    end
  end

  def manage_memberships(plgrid_team_ids)
    group_org_ids = Organization.where(plgrid_team_id: plgrid_team_ids).pluck(:id)

    memberships.each do |membership|
      state = group_org_ids.include?(membership.organization_id) ? :approved : :blocked
      membership.update(state:)
    end

    group_org_ids
      .reject { |id| memberships.detect { |m| m.organization_id == id } }
      .each do |organization_id|
      memberships.build(organization_id:, state: :approved)
    end
  end

  def fetch_short_lived_ssh_credentials!(token)
    ccm = ::Plgrid::Ccm.new(token)
    ccm.fetch!

    self.ssh_key = ccm.key
    self.ssh_certificate = ccm.certificate
  end
end
