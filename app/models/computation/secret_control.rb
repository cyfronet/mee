# frozen_string_literal: true

module Computation::SecretControl
  extend ActiveSupport::Concern

  included do
    before_update :remove_secret, if: :status_changed?
  end

  def generate_secret
    self.secret = Secret.generate
  end

  def remove_secret
    self.secret = nil if completed?
  end

  def file_manipulation?(secret)
    Secret.new(self, secret).valid?
  end

  private
    class Secret
      def initialize(computation, secret)
        @computation = computation
        @secret = secret
      end

      def self.generate
        SecureRandom.base58
      end

      def valid?
        active? && equal?
      end

      private
        def active?
          %w[queued running].any?(@computation.status)
        end

        def equal?
          @computation.secret == @secret
        end
    end
end
