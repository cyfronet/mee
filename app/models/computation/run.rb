# frozen_string_literal: true

class Computation::Run
  delegate :pipeline, :user, :pipeline_step, to: :computation
  attr_reader :computation

  def initialize(computation)
    @computation = computation
  end

  def call
    prepare_computation
    start_computation
    computation.valid?
  end

    private
      delegate :tag_or_branch, :step, to: :computation

      def prepare_computation
        init_internal_errors
        generate_script_and_secret
        clear_previous_execution_data
        computation.update(status: :script_generated, started_at: Time.current)
      end

      def start_computation
        Computations::StartJob.perform_later computation if computation.valid?
      end

      def init_internal_errors
        @errors = ActiveModel::Errors.new(computation)
        computation.internal_errors = @errors
      end

      def generate_script_and_secret
        if tag_or_branch.present?
          computation.generate_secret
          computation.generate_script(@errors)
        end
      rescue ComputationError => e
        @errors.add(:script, e.message)
        @errors.add(:tag_or_branch, "choose another or fix this one")
      end

      def clear_previous_execution_data
        computation.job_id = nil
        computation.stdout_path = nil
        computation.stdout.purge
        computation.stderr_path = nil
        computation.stderr.purge
        computation.error_output = nil
      end
end
