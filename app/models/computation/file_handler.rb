# frozen_string_literal: true

module Computation::FileHandler
  extend ActiveSupport::Concern

  included do
    delegate :pick_file_by_filename, :all_filenames, to: :pipeline
  end

  def pick_file_by_type(type)
    parameter_value_for(Parameter::RequiredDataFile.key_for(type)).try(:data_file) ||
      pipeline.pick_file_by_type(type)
  end

  def add_output(file)
    # Potential fix for: https://github.com/rails/rails/issues/42941
    blob = ActiveStorage::Blob.create_and_upload!(io: file, filename: file.original_filename)
    Rails.logger.info "Blob for #{file.original_filename} created. #{blob.id}"
    if blob && ActiveStorage::Attachment.create(name: "outputs", record_type: "Pipeline", record_id: pipeline_id, blob_id: blob.id)
      Rails.logger.info "Attached #{file.original_filename}"
      pipeline.save
      pipeline.start_computations
      Rails.logger.info "Saved pipeline and started computations"
      true
    else
      Rails.logger.error "Didn't attach file #{file.original_filename}"
      false
    end
  rescue => e
    Rails.logger.error "Failed for #{file.original_filename}, error #{e}"
  end
end
