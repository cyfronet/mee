# frozen_string_literal: true

module Computation::Logs
  extend ActiveSupport::Concern

  included do
    has_one_attached :stdout, service: :local
    has_one_attached :stderr, service: :local
  end

  def fetch_logs
    hpc.sftp_start do |connection|
      stdout.attach(io: connection.get(stdout_path), filename: "slurm.out")
      stderr.attach(io: connection.get(stderr_path), filename: "slurm.err")
    end
  end

  def stdout_live
    hpc.read(stdout_path)
  end

  def stderr_live
    hpc.read(stderr_path)
  end

  private
    def hpc
      user.hpc_client(site:)
    end
end
