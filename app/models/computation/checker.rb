# frozen_string_literal: true

class Computation::Checker
  delegate :pipeline, to: :computation, private: true

  def initialize(computation)
    @computation = computation
  end

  def configurable?
    !read_only? && pipeline.data_files_present?(computation.step.required_file_types)
  end

  def runnable?
    pipeline_in_running_mode? &&
      configured? && !computation.organization.quota.exceeded? && !read_only?
  end

  def previous_accounted?
    previous_computation_dependency? ? previous_computations_completed? : true
  end

  def configured?
    computation.tag_or_branch.present? &&
    computation.inputs_present?
  end

  def read_only?
    pipeline.archived? || !previous_accounted?
  end

  private
    attr_reader :computation

    def previous_computation_dependency?
      FlowStep.find_by(flow_id: pipeline.flow_id, step_id: computation.step_id).wait_for_previous
    end

    def previous_computations_completed?
      # computations are created according to flow_step.position order (see pipelines/build.rb)
      # so computations created earlier also are before current computation in the pipeline
      pipeline.computations.where("created_at < ?", computation.created_at).all?(&:finished?)
    end

    def pipeline_in_running_mode?
      !pipeline.campaign || pipeline.campaign.running?
    end
end
