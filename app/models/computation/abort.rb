# frozen_string_literal: true

class Computation::Abort
  def initialize(computation)
    @computation = computation
    @user = @computation.user
  end

  def call
    return unless @computation.active? && credentials_valid?

    response = @user.hpc_client(site: @computation.site)
      .abort(@computation.job_id)

    @computation.update(status: :aborted, error_message: "Job aborted") if response&.success?
  end

  private
    def credentials_valid?
      @user.credentials_valid?
    end
end
