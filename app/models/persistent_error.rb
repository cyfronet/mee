# frozen_string_literal: true

class PersistentError < ApplicationRecord
  belongs_to :errorable, polymorphic: true,
    counter_cache: "persistent_errors_count",
    touch: true

  validates :key, presence: true
  validates :message, presence: true

  after_create :notify_admins

  private
    def notify_admins
      Notifier.configuration_error(errorable:, error_message: message).deliver_later
    end
end
