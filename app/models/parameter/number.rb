# frozen_string_literal: true

class Parameter::Number < Parameter
  store_accessor :config, :min, :max, :default_value

  validates :min, numericality: true, allow_blank: true

  validates :max, numericality: true,
    allow_blank: true, unless: :min_present?

  validates :max, numericality: { greater_than_or_equal_to: ->(p) { p.min } },
    allow_blank: true, if: :min_present?

  validates :default_value,
    numericality: true,
    allow_blank: true,
    if: :min_max_not_present?
  validates :default_value,
    numericality: { greater_than_or_equal_to: ->(p) { p.min } },
    allow_blank: true,
    if: :min_present?
  validates :default_value,
    numericality: { less_than_or_equal_to: ->(p) { p.max } },
    allow_blank: true,
    if: :max_present?

  private
    def min_present?
      min.present?
    end

    def max_present?
      max.present?
    end

    def min_max_not_present?
      !(min_present? || max_present?)
    end
end
