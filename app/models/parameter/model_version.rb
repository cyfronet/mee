# frozen_string_literal: true

class Parameter::ModelVersion < Parameter
  delegate :versions, to: :step
  store_accessor :config, :default_value

  def protected?
    true
  end

  def default_value
    super || step.default_branch
  end
end
