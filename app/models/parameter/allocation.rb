# frozen_string_literal: true

class Parameter::Allocation < Parameter
  delegate :matching_allocations, to: :step
  store_accessor :config, :default_value

  def protected?
    true
  end

  def default_value
    super || default_allocation_name
  end

  def default_allocation_name
    matching_allocations.first.name if matching_allocations.size == 1
  end
end
