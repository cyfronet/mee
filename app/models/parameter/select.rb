# frozen_string_literal: true

class Parameter::Select < Parameter
  store :config, accessors: [:values, :default_value], coder: Store

  validates :values,
    length: { minimum: 2, message: "Please provide at least two options" }
  validates_associated :values

  validates :default_value, inclusion: {
    in: ->(obj) { obj.values&.map(&:value) }
  }, if: :default_value_present?

  def values_attributes=(attrs)
    self.values =
      (attrs || []).map { |_, params| Parameter::Select::Value.new(params) }
  end

  def protected?
    false
  end

  def values
    super || []
  end

  private
    # !!WARNING!! This is the only way I found to pass parent object to
    # serialized elements - by overriding private method. During the update take
    # special attention to this code since private API can be changed between versions.
    def read_store_attribute(store_attribute, key)
      super(store_attribute, key).tap do |attrs|
        attrs&.each { |a| a.select = self } if key == :values
      end
    end

    def default_value_present?
      default_value.present?
    end
end
