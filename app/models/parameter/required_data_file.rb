# frozen_string_literal: true

class Parameter::RequiredDataFile
  PREREQUISITE = "prerequisite#"

  attr_reader :data_file_type, :data_files

  def initialize(data_file_type:, pipeline:)
    @data_file_type = data_file_type
    @data_files = load_data_files(pipeline)
  end

  def value_class
    ParameterValue::RequiredDataFile
  end

  def name
    data_file_type.name
  end

  def hint
  end

  def key
    self.class.key_for(data_file_type)
  end

  def enabled?
    data_files.values.flatten.many?
  end

  def load_data_files(pipeline)
    { "Pipeline inputs" => [], "Pipeline outputs" => [], "Patient inputs" => [] }.tap do |data_files|
      pipeline.files_by_type(@data_file_type).includes(:attachments).find_each do |data_file|
        data_files["Pipeline inputs"] << data_file if pipeline_input?(data_file)
        data_files["Pipeline outputs"] << data_file if pipeline_output?(data_file)
        data_files["Patient inputs"] << data_file if patient_input?(data_file)
      end
    end
  end

  def self.key_for(type)
    data_type = type.is_a?(DataFileType) ? type.data_type : type

    "#{PREREQUISITE}#{data_type}"
  end

  private
    def pipeline_input?(blob)
      blob.attachments.any? { |a| a.record_type == "Pipeline" && a.name == "inputs" }
    end

    def pipeline_output?(blob)
      blob.attachments.any? { |a| a.record_type == "Pipeline" && a.name == "outputs" }
    end

    def patient_input?(blob)
      blob.attachments.any? { |a| a.record_type == "Patient" && a.name == "inputs" }
    end
end
