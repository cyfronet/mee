# frozen_string_literal: true

class License < ApplicationRecord
  include TimeScope, OrganizationUnit

  validates :name, presence: true

  serialize :config, coder: License::Entry::Array
  validates_associated :config

  def config_attributes=(attrs)
    self.config = attrs&.map { |_, params| License::Entry.new(params) } || []
  end

  def to_script
    config.map(&:to_export).join("\n")
  end
end
