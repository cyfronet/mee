# frozen_string_literal: true

module GitAttributes
  extend ActiveSupport::Concern

  def git_config_attributes=(attrs)
    if attrs.present?
      attrs = ActiveSupport::HashWithIndifferentAccess.new(attrs)
      attrs = previous_download_key_if_blank(attrs)
      self.git_config = GitConfig.load(attrs)
    else
      self.git_config = nil
    end
  end

  private
    def previous_download_key_if_blank(attrs)
      # File fields are not loaded into forms so they are always blank when editing
      # This allows to use previously saved download_key if new one is not uploaded
      if self.git_config.present?
        if attrs["download_key_file"].nil? && self.git_config.download_key.present?
          attrs["download_key"] = self.git_config.download_key
        end
      end
      attrs
    end
end
