# frozen_string_literal: true

module TimeScope
  extend ActiveSupport::Concern

  included do
    validates :starts_at, presence: true
    validates :ends_at, presence: true
    validate :starts_at_before_ends_at

    scope :active,
          -> { where("starts_at <= :now AND ends_at >= :now", now: Date.current) }
  end

  def active?
    now = Date.current
    starts_at <= now && ends_at >= now
  end

  def future?
    now = Date.current
    starts_at >= now && ends_at >= now
  end

  private
    def starts_at_before_ends_at
      if starts_at && ends_at && starts_at > ends_at
        errors.add(:starts_at, "can't be greater than end at date")
        errors.add(:ends_at, "can't be less than start at date")
      end
    end
end
