# frozen_string_literal: true

module Plgrid
  class HpcClient
    def initialize(host:, user: Current.user)
      @host = host
      @login = user.plgrid_login
      @ssh_key = user.ssh_key
      @ssh_certificate = user.ssh_certificate
    end

    def sftp_start(&block)
      Net::SFTP.start(@host, @login,
                      key_data: [@ssh_key], keycert_data: [@ssh_certificate],
                      keys_only: true) do |sftp|
        block.call(Sftp.new(sftp, @host))
      end
    end

    def read(path)
      io = StringIO.new
      sftp_start { |c| io = c.get(path) }
      io
    end

    def submit(script, working_directory)
      req_body = {
        host: @host,
        tag: Rails.application.config.constants.dig(:rimrock, :tag),
        script:,
        working_directory:,
      }.to_json

      response = connection.post do |req|
        req.url "api/jobs"
        req.headers["Content-Type"] = "application/json"
        req.body = req_body
      end
      HpcJob.new(response:, host: @host)
    end

    def abort(job_id)
      response = connection.put do |req|
        req.url "api/jobs/#{job_id}"
        req.headers["Content-Type"] = "application/json"
        req.body = { action: :abort }.to_json
      end
      HpcJob.new(response:, host: @host)
    end

    def check_status(job_ids)
      response = connection.get("api/jobs") do |req|
        req.options.params_encoder = Faraday::FlatParamsEncoder
        req.params = { format: "short", job_id: job_ids }
      end
      HpcJob.new(response:, host: @host)
    end

    private
      class Sftp
        def initialize(net_sftp, host)
          @net_sftp = net_sftp
          @host = host
        end

        def get(path)
          StringIO.new.tap do |io|
            @net_sftp.file.open(path, "r") do |f|
              io.puts(f.read)
              io.rewind
            rescue Net::SFTP::Exception => e
              Rails.logger.warn "#{path} log cannot be found on #{@host}", e
            end
          end
        end
      end

      def connection
        @connection ||= Faraday.new(url: Rails.application.config.constants.dig(:rimrock, :url)) do |faraday|
          faraday.request :url_encoded
          faraday.adapter Faraday.default_adapter
          faraday.headers["SSH_KEY"] = Base64.encode64(@ssh_key).gsub!(/\s+/, "")
          faraday.headers["SSH_KEY_CERT"] = Base64.encode64(@ssh_certificate).gsub!(/\s+/, "")
        end
      end
  end
end
