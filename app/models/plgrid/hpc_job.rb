# frozen_string_literal: true

class Plgrid::HpcJob
  def initialize(response:, host:)
    @response_status = response.status
    @message = parse(response.body)
    @host = host
  end

  def success?
    response_status.between?(200, 299)
  end

  def error?
    response_status >= 400
  end

  def timeout?
    response_status == 422
  end

  def unauthorized?
    response_status == 403
  end

  def status
    message["status"].downcase
  end

  def job_id
    message["job_id"]
  end

  def exit_code
    message["exit_code"]
  end

  def error_output
    message["error_output"]
  end

  def error_message
    message["error_message"]
  end

  def standard_output
    message["standard_output"]
  end

  def stdout_path
    message["stdout_path"]
  end

  def stderr_path
    message["stderr_path"]
  end

  def statuses
    message.index_by { |e| e["job_id"] }
  end

  attr_reader :message, :host

  private
    attr_reader :response_status

    def parse(body)
      JSON.parse(body)
    rescue JSON::ParserError
      body
    end
end
