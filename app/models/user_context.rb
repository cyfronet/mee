# frozen_string_literal: true

class UserContext
  delegate :admin?, to: :@membership, allow_nil: true
  delegate :approved?, to: :@membership, allow_nil: true
  attr_reader :user, :organization

  def initialize(user, organization = nil, membership = nil)
    @user = user
    @organization = organization
    @membership = membership
  end
end
