# frozen_string_literal: true

class AllocationTyping < ApplicationRecord
  belongs_to :allocation
  belongs_to :resource_type

  validates :resource_type, uniqueness: { scope: :allocation }, presence: true
end
