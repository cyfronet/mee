# frozen_string_literal: true

module Organization::DataRepository
  extend ActiveSupport::Concern

  included do
    after_commit :clean_data_repository_tokens, if: :data_repository_config_previously_changed?

    serialize :data_repository_config, coder: DataRepository::Config
    validates_associated :data_repository_config
  end

  def data_repository_config_attributes=(attrs)
    if attrs.present?
      attrs = ActiveSupport::HashWithIndifferentAccess.new(attrs)
      self.data_repository_config = DataRepository::Config.load(attrs)
    else
      self.data_repository_config = nil
    end
  end

  def clean_data_repository_tokens
    memberships.update_all(data_repository_token: nil)
  end
end
