# frozen_string_literal: true


class Organization::StorageQuota
  attr_reader :organization
  def initialize(organization)
    @organization = organization
  end

  def usage
    Rails.cache.fetch(
      "#{organization.id}_storage_usage",
      expires_in: 1.minute) {
      calculate_usage.round(2)
    }
  end

  def percentage_used
    Percentage.new(usage, organization.storage_quota).to_f
  end

  def limit
    @organization.storage_quota
  end

  def exceeded?
    usage > limit
  end

  private
    def calculate_usage
      pipelines = Pipeline.where(runnable: [organization, organization.patients])
      ActiveStorage::Blob.where(
        attachments: ActiveStorage::Attachment.where(
          record: [organization, organization.patients, pipelines])
      ).sum(:byte_size) / 1024.0**3 # change to GB
    end
end
