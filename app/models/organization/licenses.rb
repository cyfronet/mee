# frozen_string_literal: true

module Organization::Licenses
  extend ActiveSupport::Concern

  included do
    has_many :licenses, dependent: :destroy
  end

  def license_for(type)
    licenses.active.find_by(name: type)
  end
end
