# frozen_string_literal: true

module Organization::InputsStub
  extend ActiveSupport::Concern

  def inputs_blobs
    ActiveStorage::Blob.none
  end
end
