# frozen_string_literal: true

module Organization::Memberships
  extend ActiveSupport::Concern

  included do
    has_many :memberships, dependent: :destroy
    has_many :users, through: :memberships
    has_many :approved_users,
             -> { where(memberships: { state: :approved }) },
             through: :memberships,
             source: "user"
    has_many :admins,
             -> { where(Membership.any_role(:admin)).where(memberships: { state: :approved }) },
             through: :memberships,
             source: "user"
  end
end
