# frozen_string_literal: true

class DataRepository::Config
  extend ActiveModel::Naming
  include ActiveModel::Model
  include ActiveModel::Attributes
  include ActiveModel::Validations

  attribute :url, :string

  validates :url, presence: true, format: URI.regexp(%w[http https])

  def ==(other)
    self.class == other.class &&
      type == other.type &&
      url == other.url
  end

  def type
    self.class.type
  end

  def persisted?
    false
  end

  class << self
    def by_type(type)
      types_classes.find { _1.type == type }
    end

    def types
      types_classes.map(&:type)
    end

    def type
      model_name.element
    end

    def load(hsh)
      if hsh.present?
        config_class = by_type(hsh.delete("type"))
        keepers = config_class&.attribute_names
        config_class&.new(hsh.slice(*keepers))
      end
    end

    def dump(object)
      # debugger
      if object
        ActiveSupport::HashWithIndifferentAccess
          .new(object.attributes.merge({ type: object.type })).compact
      end
    end

    private
      def types_classes
        [ DataRepository::Dataverse, DataRepository::Zenodo, DataRepository::Invenio  ]
      end
  end
end
