# frozen_string_literal: true

class Pipeline < ApplicationRecord
  include Broadcaster
  include DataFiles

  enum :mode, [:automatic, :manual]
  enum :status, { waiting: 0, running: 1, success: 2, error: 3 }

  has_rich_text :notes
  belongs_to :runnable, polymorphic: true

  belongs_to :user
  belongs_to :flow

  belongs_to :campaign, optional: true, touch: true

  has_many :computations,
           # computations are created according to flow_step.position order (see pipelines/build.rb)
           # so this scope returns computations in order steps were put in the flow
           -> { order(:created_at) },
           dependent: :destroy

  validate :set_iid, on: :create
  validates :iid, presence: true, numericality: true
  validates :name, presence: true
  validates :mode, presence: true
  validates_associated :computations, on: :create

  scope :automatic, -> { where(mode: :automatic) }
  scope :latest, ->(nr = 3) { reorder(created_at: :desc).limit(nr) }

  delegate :steps, :organization, to: :flow

  after_destroy { |pipe| pipe.flow.destroy unless pipe.flow.kept? || pipe.flow.used? }
  after_destroy { |pipe| pipe.campaign&.decrement!(:desired_pipelines) }

  after_touch do
    update_status!
  end


  def update_status!
    update(status: Pipeline::StatusCalculator.new(self).calculate)
  end

  def owner_name
    user&.name || "(deleted user)"
  end

  def archived?
    flow.discarded?
  end

  def start_computations
    Pipelines::StartRunnableJob.perform_later(self) unless campaign
  end

  private
    def set_iid
      self.iid = runnable.pipelines.maximum(:iid).to_i + 1 if iid.blank?
    end
end
