# frozen_string_literal: true

class Site < ApplicationRecord
  with_options presence: true do
    validates :name, uniqueness: true
    validates :host
    validates :host_key
  end
end
