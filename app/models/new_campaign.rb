# frozen_string_literal: true

class NewCampaign
  delegate_missing_to :campaign

  attr_reader :pipeline_template, :campaign

  def initialize(campaign_params: nil, pipeline_params:, step_params: nil, user: Current.user)
    @campaign = Campaign.new(campaign_params)
    @pipeline_params = pipeline_params.merge(mode: "automatic")
    @step_params = step_params
    @user = user
    if @pipeline_params
      @pipeline_template = ::Pipelines::Build.new(@user, Current.organization,
      @pipeline_params.merge(name: "temmplate", iid: rand(10e5..10e7).to_i), @step_params).call
    end
  end

  def save
    if valid?
      super
      Campaigns::CreatePipelines.new(campaign, @user, @pipeline_params, @step_params).call
      true
    end
  end

  def valid?
    campaign_valid = @campaign.valid?
    pipeline_template_valid = @pipeline_template.valid?

    campaign_valid && pipeline_template_valid
  end
end
