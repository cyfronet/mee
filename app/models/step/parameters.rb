# frozen_string_literal: true

module Step::Parameters
  TAG_OR_BRANCH = "tag-or-branch"
  ALLOCATION = "allocation"

  extend ActiveSupport::Concern

  included do
    has_many :parameters, dependent: :destroy, autosave: true
    validates_associated :parameters
    accepts_nested_attributes_for :parameters, allow_destroy: true

    before_validation :ensure_model_version_defined
    before_validation :ensure_allocation_defined
  end

  def parameters_attributes=(attrs)
    attrs = ActiveSupport::HashWithIndifferentAccess.new(attrs).filter_map do |_, v|
      parameter_class = parameter_class(v)
      if parameter_class
        v[:type] = parameter_class
        v
      end
    end

    super(attrs)
  end

  def parameter_for(key)
    parameters.find { |p| p.key == key }
  end

  def parameter_class(attrs)
    attrs[:id] && parameter_by_id(attrs[:id])&.class ||
      Parameter.by_type(attrs[:type])
  end

  private
    def parameter_by_id(id)
      parameters.find { |p| p.id.to_s == id.to_s }
    end

    def ensure_model_version_defined
      parameters.find { |p| p.key == TAG_OR_BRANCH } ||
        parameters.build(key: TAG_OR_BRANCH,
                         name: "Model version",
                         type: Parameter::ModelVersion)
    end

    def ensure_allocation_defined
      parameters.find { |p| p.key == ALLOCATION } ||
          parameters.build(key: ALLOCATION,
                           name: "Allocation",
                           type: Parameter::Allocation)
    end
end
