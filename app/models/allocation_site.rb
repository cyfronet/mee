# frozen_string_literal: true

class AllocationSite < ApplicationRecord
  belongs_to :allocation
  belongs_to :site
end
