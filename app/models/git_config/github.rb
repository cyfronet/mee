# frozen_string_literal: true

class GitConfig::Github < GitConfig
  attribute :private_token, :string

  validates :private_token, presence: true, github_token: true

  def git_repository(repository_path:)
    GitRepository::GithubClient.new(host:,
                                    path: repository_path,
                                    download_key:,
                                    private_token:)
  end
end
