# frozen_string_literal: true

class GitConfig::Native < GitConfig
  def git_repository(repository_path:)
    GitRepository::NativeClient.new(host:,
                                    path: repository_path,
                                    download_key:)
  end
end
