# frozen_string_literal: true

class DataFileType < ApplicationRecord
  include OrganizationUnit

  validates :data_type, presence: true
  validates :pattern, presence: true
  validate :valid_pattern

  enum :viewer, { text: "text", graphics: "graphics", html: "html" }

  belongs_to :user
  has_many :prerequisites, dependent: :destroy
  has_many :steps, through: :prerequisites, source: :step

  def match?(file_name)
    Regexp.new(pattern).match?(file_name)
  end

  def =~(file_name)
    match?(file_name)
  end

  def pattern_preview
    Regexp.new(pattern).inspect
  end

  private
    def valid_pattern
      Regexp.new(pattern) if pattern.present?
    rescue RegexpError => e
      errors.add(:pattern, e.message)
    end
end
