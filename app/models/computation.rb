# frozen_string_literal: true

class Computation < ApplicationRecord
  include ParameterValues
  include SecretControl
  include FileHandler
  include Broadcaster
  include Logs

  attr_accessor :internal_errors

  belongs_to :pipeline, touch: true
  belongs_to :step

  enum :status, { created: 0, script_generated: 1,
                  queued: 2, running: 3,
                  finished: 4, aborted: 5, error: 6 }

  validate do
    errors.merge!(internal_errors) if internal_errors
  end
  validates :script, presence: { message: "Validation error, some errors are occurring, check the configuration" }, unless: :created?

  scope :active, -> { where(status: %w[script_generated queued running]) }
  scope :submitted, -> { where(status: %w[queued running]) }
  scope :created, -> { where(status: "created") }
  scope :not_finished, -> { where(status: %w[created script_generated queued running]) }
  scope :for_patient_status, ->(status) { where(pipeline_step: status) }

  delegate :mode, :manual?, :automatic?, :user, :user_id, to: :pipeline
  delegate :name, :site, :organization, to: :step
  delegate :repository, :file, :host, :download_key, to: :step
  delegate :configurable?, :configured?, :runnable?, :previous_accounted?, to: :checker

  before_update :remove_secret, if: :status_changed?
  before_destroy :abort!, if: :active?

  def initialize(attrs = {})
    if attrs
      step = attrs[:step]
      attrs[:pipeline_step] = step&.name
    end

    super(attrs)
  end

  def active?
    %w[script_generated queued running].include? status
  end

  def completed?
    %w[error finished aborted].include? status
  end

  def inputs_present?
    pipeline.data_files_present?(step.required_file_types)
  end

  def run
    manual? ? run_now : run_later
  end

  def run_now
    Computation::Run.new(self).call
  end

  def run_later
    Pipelines::StartRunnableJob.perform_later(pipeline)
  end

  def abort!
    Computation::Abort.new(self).call
  end

  def site_host
    site.host
  end

  def tag_or_branch
    parameter_value_for(Step::Parameters::TAG_OR_BRANCH)&.value
  end

  def read_only?
    pipeline.archived? || !previous_accounted?
  end

  def generate_script(errors)
    template, self.revision = step.template_and_revision_for(tag_or_branch)
    self.script = ScriptGenerator.new(self, template, errors).call
  end

  private
    def checker
      @checker ||= Computation::Checker.new(self)
    end
end
