# frozen_string_literal: true

class Membership < ApplicationRecord
  include OrganizationUnit, RoleModel

  roles [:admin]

  enum :state, [:new_account, :approved, :blocked]

  delegate :name, :email, to: :user

  belongs_to :user

  validates :user, presence: true, uniqueness: { scope: :organization }
  validates :organization, presence: true

  scope :approved, -> { where(state: :approved) }
  scope :new_accounts, -> { where(state: :new_account) }
  scope :blocked, -> { where(state: :blocked) }
  scope :admins, -> { approved.where(any_role(:admin)) }

  def self.any_role(*roles)
    (arel_table[:roles_mask] & mask_for(roles)).gt(0)
  end
end
