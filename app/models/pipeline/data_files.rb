# frozen_string_literal: true

module Pipeline::DataFiles
  extend ActiveSupport::Concern

  included do
    include Fileable::FileHandler

    has_many_attached :inputs
    has_many_attached :outputs
  end

  def pick_file_by_type(type)
    if dft = organization.data_file_types.find_by(data_type: type)
      sorted_data_files_blobs.find_by("filename ~ ?", dft.pattern)
    end
  end

  def files_by_type(data_file_type)
    sorted_data_files_blobs.where("filename ~ ?", data_file_type.pattern)
  end

  def pick_file_by_filename(filename, type: nil)
    if type
      data_files_blobs(type:).find_by(filename:)
    else
      sorted_data_files_blobs.find_by(filename:)
    end
  end

  def data_files_blobs(type: nil)
    record, name = if type
      record_type, name = type.split("_")
      record = record_type == "pipeline" ? self : runnable
      [record, name]
    else
      [[self, runnable], ["inputs", "outputs"] ]
    end
    ActiveStorage::Blob.joins(:attachments)
      .where(attachments: { record:, name: })
  end

  def data_files_present?(data_file_types)
    data_files = data_files_blobs
    data_file_types.map do |type|
      data_files.any? { |df| type =~ df.filename.to_s }
    end.all?
  end

  def all_filenames(type:)
    data_files_blobs(type:).pluck(:filename)
  end

  private
    def sorted_data_files_blobs
      # For now simple order is used to guarante following order:
      #   1. pipeline output
      #   2. pipeline input
      #   3. patient input
      data_files_blobs
        .order("attachments.record_type": :desc, "attachments.name": :desc, created_at: :asc)
    end
end
