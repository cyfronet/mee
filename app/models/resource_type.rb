# frozen_string_literal: true

class ResourceType < ApplicationRecord
  has_many :steps, dependent: :restrict_with_error
  has_many :allocation_typings, dependent: :destroy
  has_many :allocations, through: :allocation_typings

  validates :name, presence: true, uniqueness: { case_sensitive: false }
end
