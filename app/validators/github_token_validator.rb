# frozen_string_literal: true

class GithubTokenValidator < ActiveModel::EachValidator
  TEST_REPOSITORY = "cyfronet/mee-demo-steps"

  def validate_each(record, attribute, value)
    if host(record).present? && private_token(record).present?
      begin
        ::Octokit::Client.new(access_token: private_token(record))
          .branches(repository(record))
      rescue StandardError
        record.errors.add(attribute, "is invalid or token privileges are too low")
      end
    end
  end

  private
    def host(record)
      record.host || record.try(:organization_git_config)&.host
    end

    def private_token(record)
      record.private_token || record.try(:organization_git_config)&.private_token
    end

    def repository(record)
      record.try(:repository) || TEST_REPOSITORY
    end
end
