# frozen_string_literal: true

module AdminHelper
  def organization_valid?
    Current.organization.persistent_errors_count.zero?
  end

  def steps_valid?
    Current.organization.steps_persistent_errors_count.zero?
  end

  def step_valid?(step)
    step.persistent_errors_count.zero?
  end
end
