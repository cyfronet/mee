# frozen_string_literal: true

module TimeScopesHelper
  def time_scope_button_class(time_scope)
    if time_scope.active?
      "bg-success"
    else
      time_scope.future? ? "bg-info" : "bg-secondary"
    end
  end
  def time_scope_button_text(time_scope)
    if time_scope.active?
      "active"
    else
      time_scope.future? ? "future" : "outdated"
    end
  end
end
