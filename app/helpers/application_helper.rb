# frozen_string_literal: true

module ApplicationHelper
  include Pagy::Frontend

  def generate_id
    SecureRandom.alphanumeric(4, chars: [*"a".."z"] + [*"A".."Z"])
  end
end
