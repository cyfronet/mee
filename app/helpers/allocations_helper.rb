# frozen_string_literal: true

module AllocationsHelper
  def resource_types_names(allocation)
    allocation.resource_types.map { |a| a[:name] }.join(" ")
  end
end
