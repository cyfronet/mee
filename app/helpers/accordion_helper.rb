# frozen_string_literal: true

module AccordionHelper
  def accordion_classes_state(filtered)
    {
      button_state: filtered ? "" : "collapsed",
      accordion_state: filtered ? "show" : ""
    }
  end
end
