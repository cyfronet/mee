# frozen_string_literal: true

module DataFileHelper
  def data_file_types_str(data_type)
    types = data_type.types
    types.size.positive? ? types.map(&:name).join(", ") : t("storage.file.unspecified_type")
  end
end
