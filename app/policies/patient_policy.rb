# frozen_string_literal: true

class PatientPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end

  def index?
    true
  end

  def show?
    in_organization?
  end

  def new?
    true
  end

  def create?
    organization.active? &&
    in_organization?
  end

  def edit?
    organization.active? &&
    in_organization?
  end

  def update?
    false
  end

  def destroy?
    organization.active? &&
    in_organization?
  end

  def permitted_attributes
    [:case_number]
  end
end
