# frozen_string_literal: true

class FlowPolicy < ApplicationPolicy
  def index?
    user.organizations.include?(organization)
  end

  def show?
    in_organization?
  end

  def new?
    in_organization?
  end

  def create?
    organization.active? &&
    in_organization?
  end

  def edit?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def update?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def destroy?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def permitted_attributes
    [:name, :description, flow_steps_attributes: [:step_id, :position, :_destroy, :id, :wait_for_previous]]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:).kept
    end
  end

  private
    def owner?
      record.user == user
    end
end
