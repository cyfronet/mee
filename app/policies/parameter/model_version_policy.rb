# frozen_string_literal: true

class Parameter::ModelVersionPolicy < ApplicationPolicy
  def permitted_attributes
    [:id, :name, :hint, :default_value]
  end
end
