# frozen_string_literal: true

class Parameter::MultiSelectPolicy < ApplicationPolicy
  def permitted_attributes
    [:type, :id, :name, :key, :hint,
     :_destroy,
     default_value: [],
     values_attributes: [:name, :value]]
  end
end
