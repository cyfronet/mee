# frozen_string_literal: true

class Parameter::NumberPolicy < ApplicationPolicy
  def permitted_attributes
    [:type, :id, :name, :key, :hint, :_destroy, :min, :max, :default_value]
  end
end
