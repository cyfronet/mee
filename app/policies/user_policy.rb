# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      if admin?
        scope.all
      else
        scope.where(id: user&.id)
      end
    end
  end

  def index?
    admin?
  end

  def destroy?
    admin?
  end

  def update?
    admin?
  end

  def manage_users?
    admin?
  end
end
