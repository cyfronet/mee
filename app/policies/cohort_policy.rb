# frozen_string_literal: true

class CohortPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end

  def index?
    true
  end

  def new?
    organization.active?
  end

  def create?
    organization.active? &&
    in_organization?
  end

  def show?
    in_organization?
  end

  def destroy?
    organization.active? &&
    in_organization?
  end

  def update?
    organization.active? &&
    in_organization?
  end

  def permitted_attributes
    [:name]
  end
end
