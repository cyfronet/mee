# frozen_string_literal: true

class GitConfig::GithubPolicy < ApplicationPolicy
  def permitted_attributes
    [:git_type, :host, :download_key_file, :private_token]
  end
end
