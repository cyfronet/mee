# frozen_string_literal: true

class ArtifactPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    organization.active? &&
    !Current.organization.quota.exceeded?
  end

  def create?
    organization.active? &&
    in_organization?
  end

  def edit?
    organization.active? &&
    in_organization?
  end

  def update?
    organization.active? &&
    in_organization?
  end

  def destroy?
    organization.active? &&
    in_organization?
  end
end
