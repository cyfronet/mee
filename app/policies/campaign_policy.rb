# frozen_string_literal: true

class CampaignPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      Campaign.where(organization:)
    end
  end

  def index?
    true
  end

  def new?
    organization.active?
  end

  def create?
    organization.active? &&
    in_organization? && organization_flow?
  end

  def run?
    organization.active? &&
    in_organization?
  end

  def permitted_attributes
    [:name, :cohort_id, pipeline: [:flow_id, :parameters_values]]
  end

  def show?
    in_organization?
  end

  def destroy?
    organization.active? &&
    admin?
  end
end
