# frozen_string_literal: true

class HelpItemPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      HelpItem.all.select do |item|
        item.enabled_for?(organization)
      end
    end
  end

  def index?
    true
  end

  def show?
    record.enabled_for?(organization)
  end
end
