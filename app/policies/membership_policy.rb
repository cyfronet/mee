# frozen_string_literal: true

class MembershipPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      if admin?
        scope.where(organization:)
      else
        scope.where(id: user&.id, organization:)
      end
    end
  end

  def index?
    admin?
  end

  def destroy?
    organization.active? &&
    admin?
  end

  def update?
    organization.active? &&
    admin?
  end

  def update_roles?
    admin?
  end

  def manage_users?
    admin?
  end

  def permitted_attributes
    [:state, roles: []]
  end
end
