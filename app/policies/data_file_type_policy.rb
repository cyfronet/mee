# frozen_string_literal: true

class DataFileTypePolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    true
  end

  def create?
    organization.active? &&
    in_organization?
  end

  def edit?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def update?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def destroy?
    organization.active? &&
    (admin? || owner?) && in_organization?
  end

  def permitted_attributes
    [:data_type, :name, :pattern, :viewer]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end

  private
    def owner?
      record.user == user
    end
end
