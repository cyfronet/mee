# frozen_string_literal: true

class ParameterPolicy < ApplicationPolicy
  def show?
    user.organizations.include?(organization)
  end
end
