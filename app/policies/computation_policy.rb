# frozen_string_literal: true

class ComputationPolicy < ApplicationPolicy
  def permitted_attributes
    [parameter_values_attributes: parameter_attributes]
  end

  def show?
    in_organization?
  end

  def update?
    organization.active? &&
    !organization.quota.exceeded? &&
      owned? && !record.active? && configurable? &&
      record.step.persistent_errors_count.zero?
  end

  def runnable?
    organization.active? &&
    !organization.quota.exceeded? &&
      user.credentials_valid? && record.runnable?
  end

  def abort?
    owned? && record.active? && record.manual?
  end

  private
    def parameter_attributes
      record.form_parameters_values.to_h do |pv|
        [pv.key, pv.permitted_params]
      end
    end

    def configurable?
      !record.pipeline.archived? && configurable_in_mode?
    end

    def configurable_in_mode?
      if record.automatic?
        record.step.parameters.size != record.parameter_values.size
      else
        record.runnable?
      end
    end

    def owned?
      record.user_id == user.id && record.organization == organization
    end

    def in_organization?
      record.step&.organization_id == organization.id
    end
end
