# frozen_string_literal: true

class ComputationChannel < ApplicationCable::Channel
  def subscribed
    stream_for computation if computation
  end

  private
    def computation
      Computation.find(params[:id])
    end
end
