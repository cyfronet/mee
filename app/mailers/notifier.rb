# frozen_string_literal: true

class Notifier < ApplicationMailer
  def account_approved(user:, organization:)
    @organization_name = organization.name
    @login_url = root_url(subdomain: organization.slug)

    mail(to: user.email, subject: I18n.t("emails.account_approved.subject",
                                         organization: @organization_name))
  end

  def credentials_expired(user)
    mail(to: user.email, subject: I18n.t("emails.credentials_expired.subject"))
  end

  def configuration_error(errorable:, error_message:)
    admins_emails = errorable.organization.admins.pluck(:email)
    @errorable_name = errorable.name
    @organization_name = errorable.organization.name
    @error_message = error_message
    mail(to: admins_emails, subject: I18n.t("emails.configuration_error.subject", organization_name: @organization_name))
  end

  def allocation_expired(organization:, latest_allocation_expiration:)
    @name = organization.name
    @latest_allocation_expiration = latest_allocation_expiration
    admins_emails = organization.admins.pluck(:email)
    mail(to: admins_emails, subject: I18n.t("emails.allocation_will_expire.subject", organization: @name))
  end
end
