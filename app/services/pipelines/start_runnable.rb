# frozen_string_literal: true

module Pipelines
  class StartRunnable
    def initialize(pipeline)
      @pipeline = pipeline
      @user = pipeline.user
    end

    def call
      internal_call if @pipeline.automatic? && @user.credentials_valid? && @pipeline.organization.active?
    end

    private
      def internal_call
        @pipeline.computations.created.each { |c| c.run_now if c.runnable? }
      end
  end
end
