# frozen_string_literal: true

class Pipelines::Build
  def initialize(user, runnable, runnable_params, parameters_values_params, campaign: nil)
    @user = user
    @runnable = runnable
    @runnable_params = runnable_params || {}
    @parameters_values_params = parameters_values_params || {}
    @campaign = campaign
  end

  def call
    params = @runnable_params.merge(user: @user, runnable: @runnable, campaign: @campaign)
    params[:mode] ||= :automatic

    pipeline = Pipeline.new(params)

    if pipeline.flow
      # rubocop:disable Rails/FindEach
      # `find_each` don't respect ordering
      pipeline.steps.includes(:parameters).each do |step|
        pipeline.computations.build(
          step:,
          pipeline:,
          parameter_values_attributes: step_params(step.slug)
        )
      end
      # rubocop:enable Rails/FindEach
    end

    pipeline
  end

  private
    def step_params(step_name)
      @parameters_values_params.fetch(step_name) { {} }.to_h
    end
end
