# frozen_string_literal: true

require "zip"

class Patients::Uploader < ApplicationJob
  class UploadError < StandardError; end
  attr_reader :uploader, :patients

  def initialize(uploader)
    @uploader = uploader
  end

  def call
    tmp_file = download_patient_file
    ::Zip::File.open(tmp_file.path) do |zip_file|
      check_for_nested_directories(zip_file)
      @patients = create_patients(zip_file)
      zip_file.select { _1.file_type_is?(:file) }.each { add_file(_1) }
    end

    tmp_file.unlink
    uploader.finished!
  rescue UploadError => e
    uploader.error_message = e.message
    uploader.failed!
  end

  private
    def download_patient_file
      patient_file = Tempfile.new("patient_file.zip")
      file_content = uploader.patient_archive.download.mb_chars
      patient_file.write(file_content)
      patient_file.close
      patient_file
    end

    def check_for_nested_directories(zip_file)
      if zip_file.find { |entry| entry.ftype == :directory && entry.name.split("/").count > 1 }
        raise UploadError, I18n.t("patients.upload.errors.nested_directories")
      end
    end

    def create_patients(file)
      cohorts = [uploader.cohort].compact
      case_numbers = extract_case_numbers(file)
      patients_data = case_numbers.map do |case_number|
        { case_number: case_number.mb_chars.to_s, organization: uploader.organization, cohorts: }
      end

      patients = Patient.create(patients_data)
      patients_hash = {}
      patients.map { |patient| patients_hash[patient.case_number] = patient }
      patients_hash
    end

    def extract_case_numbers(zip_file)
      case_numbers = zip_file.filter_map { |patient_dir|
        File.basename(patient_dir.name) if patient_dir.ftype == :directory
      }
      check_for_duplicates(case_numbers)
    end

    def check_for_duplicates(case_numbers)
      duplicates = uploader.organization.patients.where(case_number: case_numbers).pluck(:case_number)
      if duplicates.any?
        raise UploadError, I18n.t("patients.upload.errors.duplicated_case_numbers", numbers: duplicates.join(", "))
      end
      case_numbers
    end

    def add_file(entry)
      file_data = entry.get_input_stream.read
      filename = entry.name.split("/").last
      case_number = entry.name.split("/").second_to_last
      patients[case_number].inputs.attach(io: StringIO.new(file_data), filename:)
    rescue
      raise UploadError, I18n.t("patients.upload.errors.file_error", filename:, case_number:)
    end
end
