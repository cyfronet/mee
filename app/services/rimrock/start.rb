# frozen_string_literal: true

module Rimrock
  class Start
    def initialize(computation, options = {})
      @computation = computation
      @user = @computation.user
    end

    def call
      @computation.with_lock do
        return if already_started?

        hpc_job = @user.hpc_client(site: @computation.site)
        .submit(@computation.script, @computation.working_directory)
        case
        when hpc_job.success? then success(hpc_job)
        when hpc_job.unauthorized? then unauthorized
        else failure(hpc_job)
        end
      end
    end

    private
      def already_started?
        %w[queued running].include? @computation.status
      end

      def success(hpc_job)
        @computation.update(
          job_id: hpc_job.job_id,
          job_host: hpc_job.host,
          status: hpc_job.status,
          stdout_path: hpc_job.stdout_path,
          stderr_path: hpc_job.stderr_path
        )
      end

      def unauthorized
        @computation.update(
          status: "error",
          error_message: "403 - Unauthorized"
        )
      end

      def failure(hpc_job)
        @computation.update(
          job_host: hpc_job.host,
          status: hpc_job.status,
          exit_code: hpc_job.exit_code,
          standard_output: hpc_job.standard_output,
          error_output: hpc_job.error_output,
          error_message: hpc_job.error_message
        )
      end
  end
end
