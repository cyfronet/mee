# frozen_string_literal: true

module Rimrock
  class Update
    JOBS_BATCH_SIZE = 100

    def initialize(user, host, options = {})
      @user = user
      @host = host
      @on_finish_callback = options[:on_finish_callback]
    end

    def call
      active_computations.find_in_batches(batch_size: JOBS_BATCH_SIZE) do |computations|
        job_ids = computations.map(&:job_id)
        hpc_job = @user.hpc_client.check_status(job_ids)
        case
        when hpc_job.success? then success(computations, hpc_job)
        when hpc_job.timeout? then error(hpc_job, :timeout)
        else error(hpc_job, :internal)
        end
      end
    end

    private
      def success(computations, hpc_job)
        computations.each do |computation|
          update_computation(computation, hpc_job.statuses[computation.job_id])
        end
      end

      def update_computation(computation, new_status)
        if new_status
          updated_status = new_status["status"].downcase
          computation.update(status: updated_status)
          if computation.completed?
            computation.fetch_logs
            Pipelines::RemoveDuplicateOutputsJob.perform_later(computation.pipeline)
          end
          on_finish_callback(computation) if computation.status == "finished"
        else
          computation.update(status: "error", error_message: "Job cannot be found")
        end
      end

      def error(hpc_job, error_type)
        Rails.logger.tagged(self.class.name) do
          Rails.logger.warn(
            I18n.t("rimrock.update.#{error_type}", user: @user&.name, details: hpc_job.message)
          )
        end
      end

      def active_computations
        @active_computations ||= @user.computations.submitted.where(job_host: @host)
      end

      def on_finish_callback(computation)
        @on_finish_callback&.new(computation)&.call
      end
  end
end
