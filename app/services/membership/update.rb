# frozen_string_literal: true

class Membership::Update < Membership::Base
  def initialize(current_user, membership, attributes)
    @current_user = current_user
    @membership = membership
    @attributes = attributes
  end

  def call
    if blocking?
      return :self if self?
    elsif removing_admin_role? && last_admin?
      return :last_admin
    end

    update!
  end

  private
    def update!
      if @membership.update(@attributes)
        :ok
      else
        :error
      end
    end

    def blocking?
      @attributes[:state] == "blocked"
    end

    def removing_admin_role?
      @membership.admin? && (@attributes[:roles] || []).exclude?("admin")
    end

    def last_admin?
      Membership.admins
        .where(organization: @membership.organization)
        .where.not(user: @current_user).count.zero?
    end
end
