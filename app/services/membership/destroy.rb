# frozen_string_literal: true

class Membership::Destroy < Membership::Base
  def call
    if self?
      :self
    elsif perform!
      :ok
    else
      :error
    end
  end

  private
    def perform!
      Computation.joins(:pipeline)
      .where("pipelines.user_id = ?", @membership.user_id)
      .find_each(&:abort!)
      @membership.destroy
    end
end
