# frozen_string_literal: true

class Step::ValidateRepository
  def initialize(step)
    @step = step
  end

  def call
    if repo
      clear_persistent_errors
      check_repository
    end
  end

  private
    def clear_persistent_errors
      @step.persistent_errors.where(key: "repository").destroy_all
    end

    def check_repository
      @step.validate

      error_message = []
      error_message << I18n.t("errors.git_config.ssh_key") unless repo.key_valid?
      error_message << I18n.t("errors.git_config.private_token") if private_token_error?

      if error_message.any?
        error_message = ([I18n.t("errors.git_config.repository")] + error_message).join(" or ")
        @step.persistent_errors.create(key: "repository", message: error_message)
      end
    end

    def repo
      @repo ||= @step&.git_repository
    end

    def private_token_error?
      @step.calculated_git_config
        .errors[:private_token].present?
    end
end
