# frozen_string_literal: true

class Files::Filter
  def initialize(query_params, files, organization)
    @query_params = query_params
    @files = files
    @organization = organization
  end

  def call
    return files if query_params.nil? || files.nil?
    apply_filters
  end

  private
    attr_reader :query_params, :organization, :files

    def apply_filters
      @files = filter_by_name(query_params[:name]) if query_params[:name].present?
      @files = filter_by_name(data_file_pattern) if query_params[:file_type].present?
      @files
    end

    def filter_by_name(param)
      files.joins(:blob).where("active_storage_blobs.filename ~ ?", param)
    end


    private
      def data_file_pattern
        organization.data_file_types.find_by(data_type: query_params[:file_type])&.pattern
      end
end
