# frozen_string_literal: true

require "liquid"

class ScriptGenerator
  attr_reader :computation

  delegate :pipeline, :revision, to: :computation
  delegate :runnable, :user, :mode, to: :pipeline
  delegate :email, to: :user
  delegate :slug, :organization, to: :runnable

  def initialize(computation, template, errors = ActiveModel::Errors.new(computation))
    @computation = computation
    @template = template
    @errors = errors
  end

  def call
    if @template
      parsed_template = Liquid::Template.parse(@template)

      parsed_template
        .render({ "email" => lambda { email },
                  "case_number" => lambda { (runnable.try(:case_number)) },
                  "revision" => revision,
                  "grant_id" => lambda { allocation_id }, "allocation_id" => lambda { allocation_id },
                  "mode" => lambda { mode },
                  "pipeline_identifier" => lambda { pipeline_identifier },
                  "pipeline_name" => lambda { pipeline.name },
                  "campaign_name" => lambda { pipeline.campaign&.name },
                  "cohort_name" => lambda { pipeline.campaign&.cohort&.name },
                  "step_name" => lambda { @computation.name }
                },
                registers: { pipeline:, computation:, organization:, errors: @errors })
        .gsub(/\r\n?/, "\n")
    end
  rescue Liquid::SyntaxError => e
    @errors.add(:script, e.message)
  end

  def allocation_id
    @allocation_or_error ||=
      computation.parameter_value_for(Step::Parameters::ALLOCATION)&.value ||
        @errors.add(:script, "active allocation cannot be found")
  end

  def pipeline_identifier
    "#{slug}-#{pipeline.iid}"
  end
end
