# frozen_string_literal: true

class AllocationsValidationsJob < ApplicationJob
  def perform
    Organization.find_each do |organization|
      Organization::ValidateAllocationJob.perform_later(organization)
    end
  end
end
