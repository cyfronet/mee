# frozen_string_literal: true

class OrganizationsJob < ApplicationJob
  def perform
    Organization.active.find_each do |organization|
      organization.validate_later
    end
  end
end
