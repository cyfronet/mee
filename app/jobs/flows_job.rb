# frozen_string_literal: true

class FlowsJob < ApplicationJob
  def perform
    Flow.cleanup
  end
end
