# frozen_string_literal: true

module Pipelines
  class RemoveDuplicateOutputsJob < ApplicationJob
    queue_as :computation

    def perform(pipeline)
      ::Pipelines::RemoveDuplicateOutputs.new(pipeline).call
    end
  end
end
