# frozen_string_literal: true

module Computations
  class SiteUpdateJob < ApplicationJob
    queue_as :computation

    def perform(user, host)
      GuardedExecutor.new(user).call do
        Rimrock::Update.new(user, host,
                            on_finish_callback: PipelineUpdater).call
      end
    end
  end
end
