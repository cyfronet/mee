# frozen_string_literal: true

module Computations
  class StartJob < ApplicationJob
    queue_as :computation

    def perform(computation)
      GuardedExecutor.new(computation.user).call { start(computation) }
    end

    private
      def start(computation)
        Rimrock::Start.new(computation).call
      rescue StandardError => e
        Rails.logger.error(e)
        computation.update(status: :error,
                           error_message: e.message)
      end
  end
end
