# frozen_string_literal: true

module Computations
  class UpdateJob < ApplicationJob
    queue_as :computation

    def perform(user)
      user.computations.submitted.distinct.pluck(:job_host).each do |host|
        Computations::SiteUpdateJob.perform_later(user, host)
      end
    end
  end
end
