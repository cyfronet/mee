# frozen_string_literal: true

class Organization::ValidateAllocationJob < ApplicationJob
  def perform(organization)
    @organization = organization

    if allocation_expires_soon?
      Notifier.allocation_expired(organization:, latest_allocation_expiration:).deliver_later
    end
  end

  private
    def allocation_expires_soon?
      (Date.today..(Date.today + time_window)).cover?(latest_allocation_expiration)
    end

    def latest_allocation_expiration
      @latest_allocation_expiration ||= @organization.allocations.order(ends_at: :desc).pluck(:ends_at).first
    end

    def time_window
      Mee::Application.config.checks.before_allocation_expire
    end
end
