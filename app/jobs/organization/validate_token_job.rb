# frozen_string_literal: true

class Organization::ValidateTokenJob < ApplicationJob
  def perform(organization)
    if organization.last_persistent_error_for(child: "git_config", key: "private_token").nil?
      organization.validate

      if organization.git_config.errors[:private_token].present?
        organization.persistent_errors.create(child: "git_config", key: "private_token", message: I18n.t("errors.git_config.private_token"))
      end
    end
  end
end
