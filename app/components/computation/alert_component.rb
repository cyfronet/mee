# frozen_string_literal: true

class Computation::AlertComponent < ViewComponent::Base
  include ComputationsHelper

  def initialize(computation:)
    @computation = computation
  end

  def alert_class
    if computation.configurable?
      alert_computation_class(computation)
    else
      "alert-warning"
    end
  end

  def message
    if computation.pipeline.archived?
      I18n.t("steps.readonly", step: computation.name.downcase)
    elsif !computation.inputs_present?
      missing_input
    elsif !computation.previous_accounted?
      I18n.t("computations.wait_for_previous")
    elsif computation.configurable?
      I18n.t("computation.#{computation.status}", step: computation.name)
    else
      I18n.t("computations.quota_exceeded")
    end
  end

  attr_reader :computation

  def missing_input
    content_tag(:span) do
      concat(I18n.t("steps.missing_input", step: computation.name.downcase))
      if computation.step.required_file_types.present?
        concat("The following files should be available:")
        concat(input_files_list)
      end
    end
  end

  def input_files_list
    content_tag(:ul) do
      computation.step.required_file_types.map do |required_dft|
        content_tag(:li) do
          concat(required_dft.name)
          concat(": ")
          concat(required_dft.pattern)
        end
      end.join.html_safe
    end
  end
end
