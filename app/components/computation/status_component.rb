# frozen_string_literal: true

class Computation::StatusComponent < ViewComponent::Base
  with_collection_parameter :computation

  def initialize(computation:)
    @computation = computation
  end

  erb_template <<~ERB
    <span id="<%= dom_id(@computation, "status_component") %>">
        <% if archived? %>
          <span class="fa-layers fa-fw"">
            <i class="<%= icon_class %>" title="<%= tooltip %>"></i>
            <span class="fa-layers-counter status-with-warning">!</span>
          </span>
        <% else %>
          <i class="<%= icon_class %>" title="<%= tooltip %>"></i>
        <% end %>
    </span>
  ERB

  private
    def icon_class
      if need_configuration?(@computation)
        "fas fa-wrench"
      elsif !@computation.configurable?
        "far fa-circle"
      elsif @computation.active?
        "fas fa-circle-notch fa-spin"
      elsif @computation.status == "finished"
        "far fa-check-circle"
      elsif @computation.status == "error"
        "far fa-times-circle"
      else
        "fas fa-circle"
      end
    end

    def need_configuration?(computation)
      @computation.pipeline.automatic? && @computation.tag_or_branch.blank?
    end

    def tooltip
      if @computation.configurable?
        I18n.t("steps.#{@computation.status}", step: @computation.step.name)
      elsif archived?
        I18n.t("steps.readonly", step: @computation.step.name)
      else
        I18n.t("steps.missing_input", step: @computation.step.name)
      end
    end

    def archived?
      @computation.pipeline.archived?
    end
end
