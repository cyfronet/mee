# frozen_string_literal: true

class Computation::DetailsComponent < ViewComponent::Base
  include ComputationsHelper
  STATUS_MAP = {
    created: { type: "primary" },
    script_generated: { type: "primary" },
    runnable: { type: "primary" },
    queued: { type: "info" },
    running: { type: "warning" },
    error: { type: "danger" },
    finished: { type: "success" },
    aborted: { type: "warning" }
  }.freeze

  def initialize(computation:)
    @computation = computation
  end

  def start_time
    @computation.started_at ? l(@computation.started_at, format: :short) : "-"
  end

  def site
    @computation.site.name.capitalize
  end

  def step_link
    link_to @computation.step.name, @computation.step
  end

  def source_link
    if @computation.revision
      link_to @computation.revision,
              "https://#{host}/#{repo}/tree/#{@computation.revision}"
    end
  end

  def execution_time
    case @computation.status
    when "created", "script_generated", "queued"
      "-"
    when "running"
      Time.at(Time.now - @computation.started_at).utc.strftime("%Hh %Mm %Ss")
    else
      # TODO: FIXME If possible, use finish time from the computing job
      Time.at(@computation.updated_at - @computation.started_at).utc.strftime("%Hh %Mm %Ss")
    end
  end

  def stdout_link
    link_to_if @computation.stdout_path && !@computation.queued?,
      t("computation.stdout_path"), computation_stdout_path(@computation),
      target: "_blank"
  end

  def stderr_link
    link_to_if @computation.stderr_path && !@computation.queued?,
      t("computation.stderr_path"), computation_stderr_path(@computation),
      target: "_blank"
  end

  def computation_status
    status = runnable? ? "runnable" : @computation.status
    label_class = STATUS_MAP[status.to_sym][:type] || "default"
    content_tag :div, [t(".#{status}"), @computation.error_message].reject(&:blank?).join(" - "),
                class: "badge bg-#{label_class} text-break",
                title: @computation.error_message
  end

  def error_output
    content_tag :div, @computation.error_output if @computation.error_output
  end

  private
    def repo
      @computation.step.repository
    end

    def host
      @computation.step.host
    end

    def runnable?
      @computation.status == "created" && @computation.runnable?
    end
end
