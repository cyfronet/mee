# frozen_string_literal: true

class Step::GitComponent < ViewComponent::Base
  def initialize(step:)
    @step = step
  end

  attr_reader :step
end
