# frozen_string_literal: true

class ProgressBar < ViewComponent::Base
  attr_reader :width, :color, :value_now, :min, :max

  def initialize(width:, color:, value_now:, min: 0, max: 100)
    @width = width
    @color = color
    @value_now = value_now
    @min = min
    @max = max
  end
end
