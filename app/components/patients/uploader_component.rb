# frozen_string_literal: true

class Patients::UploaderComponent < ViewComponent::Base
  attr_reader :uploader, :cohort
  def initialize(uploader:, cohort: nil)
    @uploader = uploader
    @cohort = cohort
  end

  def latest_status
    text = I18n.t("patients.uploader.latest_status", status: uploader&.status)
    text.concat error_message if uploader&.failed?
    content_tag :div, text,
    class: status_class
  end

  def status_class
    case uploader&.status
    when "processing"
      "alert alert-info"
    when "finished"
      "alert alert-success"
    when "failed"
      "alert alert-error"
    end
  end

  def error_message
    I18n.t("patients.uploader.error", error_message: uploader&.error_message) if uploader.error_message
  end

  def title
    I18n.t("patients.uploader.title")
  end

  def url
    upload_patients_path
  end
end
