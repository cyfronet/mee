# frozen_string_literal: true

class Cohorts::SummaryComponent < ViewComponent::Base
  with_collection_parameter :cohort

  delegate :destroy?, to: :@policy

  def initialize(cohort:, pundit_user:)
    super
    @cohort = cohort
    @policy = Pundit.policy!(pundit_user, @cohort)
  end

  def campaigns_size
    @cohort.campaigns.size
  end

  def patients_size
    @cohort.patients.size
  end
end
