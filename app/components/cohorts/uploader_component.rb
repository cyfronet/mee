# frozen_string_literal: true

class Cohorts::UploaderComponent < Patients::UploaderComponent
  def title
    I18n.t("cohorts.uploader.title")
  end

  def url
    upload_patients_path(cohort_id: cohort.id)
  end
end
