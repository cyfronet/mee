# frozen_string_literal: true

class Progress < ViewComponent::Base
  attr_reader :title, :bars

  def initialize(title:, bars:)
    @title = title
    @bars = bars
  end

  def add_progress_bar(width:, color:, value_now:)
    bars << ProgressBar.new(width:, color:, value_now:, min:, max:)
  end
end
