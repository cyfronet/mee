# frozen_string_literal: true

class DropzoneFileInput < SimpleForm::Inputs::FileInput
  def dropzone_input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options,
                                                 wrapper_options)

    @builder.file_field(attribute_name, merged_input_options)
  end

  def input_html_options
    multiple = super[:data].try(:[], :multiple)
    data = (super[:data] || {}).merge({ dropzone_target: "input", max_files_upload_count_value: Current.organization.max_files_upload_count })
    super.merge({ direct_upload: true, multiple:, include_hidden: false, data: })
  end

  def dropzone_placeholder(wrapper_options = nil)
    template.content_tag(:div, class: "dropzone-msg dz-message needsclick text-gray-600 m-0") do
      image + title + description
    end
  end

  private
    def image
      template.content_tag(:span, class: "flex justify-center mb-2") do
        options[:image] || template.image_tag("dropzone_placeholder.svg")
      end
    end

    def title
      template.content_tag(:h5, class: "dropzone-msg-title text-sm") do
        options[:title] || I18n.t("dropzone.inputs.title")
      end
    end

    def description
      template.content_tag(:span, class: "dropzone-msg-desc text-sm") do
        options[:description] || I18n.t("dropzone.inputs.description")
      end
    end
end
