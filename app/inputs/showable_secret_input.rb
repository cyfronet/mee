# frozen_string_literal: false

class ShowableSecretInput < SimpleForm::Inputs::PasswordInput
  def input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

    template.content_tag(:div, class: "input-group", "data-controller": "secret") do
      @builder.text_field(attribute_name,
                              merged_input_options.merge("data-secret-target": "input", type: "password")) +
      template.content_tag(:button, class: "btn btn-outline-secondary m-0", "data-action": "secret#toogle") do
        template.content_tag(:i, "", class: "fas fa-eye", "data-secret-target": "icon")
      end
    end
  end
end
