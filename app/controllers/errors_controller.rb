# frozen_string_literal: true

class ErrorsController < ApplicationController
  allow_unauthenticated_access
  layout "errors"

  def not_found; end

  def unprocessable; end

  def internal_server_error; end
end
