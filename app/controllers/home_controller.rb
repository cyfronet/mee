# frozen_string_literal: true

class HomeController < RootApplicationController
  before_action :load_organizations
  include Rails.application.routes.url_helpers

  def index
  end

  def not_found
    flash.now["error"] = I18n.t("organization.not_found")

    render :index
  end

  def not_authorized
    flash.now["error"] = I18n.t("organization.not_authorized")

    render :index
  end

  private
    def load_organizations
      @user_organizations = Current.user&.organizations
      @organizations = Organization.without @user_organizations
    end

    def script_name(organization_id)
      "/#{OrganizationSlug.encode(organization_id)}"
    end
end
