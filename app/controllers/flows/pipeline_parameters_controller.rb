# frozen_string_literal: true

class Flows::PipelineParametersController < ApplicationController
  layout false

  def show
    policy_scope(Flow).find_by!(id: params[:flow_id])
    @pipeline = ::Pipelines::Build.new(Current.user, @runnable,
                                       { flow_id: params[:flow_id],
                                         mode: params[:mode] }, {}).call
    authorize(@pipeline, :new?)
  end
end
