# frozen_string_literal: true

class AdminApplicationController < ActionController::Base
  include Authentication
  include Sentryable
  include ErrorRescues

  before_action do
    raise Pundit::NotAuthorizedError unless Current.user.admin?
  end
end
