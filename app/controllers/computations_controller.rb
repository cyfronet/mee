# frozen_string_literal: true

class ComputationsController < ApplicationController
  before_action :find_and_authorize

  def show
    load_computations

    if request.xhr?
      render partial: "computations/show", layout: false,
              locals: {
                pipeline: @pipeline, computation: @computation,
                computations: @computations
              }
    end
  end

  def update
    @computation.assign_attributes(permitted_attributes(@computation))
    @computation.error_message = nil if @computation.completed?
    if policy(ActiveStorage::Attachment).create? && @computation.save && @computation.run
      redirect_to computation_path(@computation),
      notice: I18n.t("computations.update.started_#{@computation.mode}")
    else
      @computation.status = @computation.status_was
      load_computations
      render "computations/actions/show", status: :unprocessable_entity
    end
  end

  private
    def load_computations
      @computations = @pipeline.computations.
                      includes(:pipeline, parameter_values: :parameter, step: [
                        :prerequisites, :required_file_types])
    end

    def find_and_authorize
      @computation = Computation.includes(pipeline: :runnable).find(params[:id])
      @pipeline = @computation.pipeline
      @runnable = @pipeline.runnable

      authorize(@computation, :show?)
    end
end
