# frozen_string_literal: true

class Admin::LicensesController < ApplicationController
  before_action :load_and_authorize, only: [:edit, :update, :destroy]

  def index
    authorize(License)
    @pagy, @licenses = pagy(policy_scope(License))
  end

  def new
    @license = License.new
    authorize(@license)
  end

  def create
    @license = License.new(permitted_attributes(License))
    authorize(@license)

    if @license.save
      redirect_to(admin_licenses_path)
    else
      render(:new, status: :unprocessable_entity)
    end
  end

  def edit
  end

  def update
    attrs = permitted_attributes(@license)
    attrs["config_attributes"] ||= []

    if @license.update(attrs)
      redirect_to(admin_licenses_path)
    else
      render(:edit, status: :bad_request)
    end
  end

  def destroy
    @license.destroy
    redirect_to(admin_licenses_path)
  end

  private
    def load_and_authorize
      @license = Current.organization.licenses.find(params[:id])
      authorize(@license)
    end
end
