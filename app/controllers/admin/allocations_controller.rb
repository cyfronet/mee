# frozen_string_literal: true

class Admin::AllocationsController < ApplicationController
  before_action :load_and_authorize, only: [:edit, :update, :destroy]

  def index
    authorize(Allocation)
    @pagy, @allocations = pagy(policy_scope(Allocation).includes(:resource_types))
  end

  def new
    @allocation = Allocation.new
    authorize(@allocation)
  end

  def create
    @allocation = Allocation.new(permitted_attributes(Allocation))
    authorize(@allocation)

    if @allocation.save
      redirect_to(admin_allocations_path)
    else
      render(:new, status: :unprocessable_entity)
    end
  end

  def edit
  end

  def update
    if @allocation.update(permitted_attributes(@allocation))
      redirect_to(admin_allocations_path)
    else
      render(:edit, status: :unprocessable_entity)
    end
  end

  def destroy
    @allocation.destroy
    redirect_to(admin_allocations_path)
  end

  private
    def load_and_authorize
      @allocation = Current.organization.allocations.find(params[:id])
      authorize(@allocation)
    end
end
