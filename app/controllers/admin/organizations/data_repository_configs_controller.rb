# frozen_string_literal: true

module Admin
  class Organizations::DataRepositoryConfigsController < ApplicationController
    def show
      if enabled?
        render partial: "data_repository_config/form",
               layout: false,
               locals: {
                 module_type: "organization",
                 data_repository_config: DataRepository::Dataverse.new,
                 data_repository_config_url: admin_organization_data_repository_config_url(organization_id: Current.organization.id || "_new"),
                 data_repository_types: DataRepository::Config.types
               }
      else
        render partial: "data_repository_config/default"
      end
    end

    private
      def enabled?
        ActiveModel::Type::Boolean.new.cast(params[:enable])
      end
  end
end
