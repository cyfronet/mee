# frozen_string_literal: true

class Cohorts::CampaignsController < ApplicationController
  def index
    @cohort = Cohort.includes(:campaigns).find(params[:cohort_id])
    authorize(@cohort)
  end
end
