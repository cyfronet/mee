# frozen_string_literal: true

class CampaignsController < ApplicationController
  include PipelineParams
  before_action :find_campaign, only: [:destroy, :show]

  def index
    @pagy, @campaigns = pagy(policy_scope(Campaign))
  end

  def show
    @pipelines = @campaign.pipelines
      .includes(:runnable, computations: [step: [:flows], parameter_values: []])
      .sort_by(&:name)
  end

  def new
    initialize_new
  end

  def create
    @campaign = NewCampaign.new(
      campaign_params: permitted_attributes(Campaign),
      pipeline_params: permitted_attributes(Pipeline),
      step_params: permitted_step_attributes(params)
    )

    if @campaign.save
      redirect_to campaign_path(id: @campaign.id), notice: I18n.t("campaigns.create.success", name: @campaign.name)
    else
      initialize_new(permitted_step_attributes(params), permitted_attributes(Pipeline)[:flow_id])
      render(:new, status: :unprocessable_entity)
    end
  end

  def destroy
    if @campaign.destroy
      flash[:notice] = I18n.t("campaigns.delete.success", name: @campaign.name)
    else
      flash[:alert] = I18n.t("campaigns.delete.failure", name: @campaign.name)
    end
    respond_to do |format|
      format.html { redirect_to campaigns_path }
      format.turbo_stream
    end
  end

  private
    def find_campaign
      @campaign = Campaign.includes(:cohort).find(params[:id])
      authorize @campaign
    end

    def initialize_new(parameters_values_params = {}, flow_id = policy_scope(Flow).first&.id)
      @cohorts = policy_scope(Cohort)
      @flows = policy_scope(Flow)
      @campaign ||= NewCampaign.new(pipeline_params: { flow_id: })
    end
end
