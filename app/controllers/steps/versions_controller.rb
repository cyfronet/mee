# frozen_string_literal: true

class Steps::VersionsController < ApplicationController
  before_action :find_and_authorize

  def index
    versions = @step.versions(force_reload: force_reload?)

    render partial: "options", layout: false, locals: { versions: }
  end

  private
    def find_and_authorize
      @step = Current.organization.steps.friendly.find(params[:step_id])
      authorize(@step, :versions?)
    end

    def force_reload?
      params[:force_reload] == "true"
    end
end
