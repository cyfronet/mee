# frozen_string_literal: true

class Computations::ArtifactsController < Computations::ApiController
  def show
    if artifact = Current.organization.artifacts_blobs.find_by(filename: params[:name])
      redirect_to url_for(artifact)
    else
      render json: {
        error: "No artifact with given name found",
        status: 404
      }, status: :not_found
    end
  end

  def create
    if params[:file].nil?
      render json: { error: "No file provided", status: 400 }, status: :bad_request
      return
    end
    if Artifact.upload(files: [params[:file]])
      render json: { message: "Upload successful", status: 200 }, status: :created
    else
      render json: { error: "Upload failed", status: 500 }, status: :internal_server_error
    end
  end
end
