# frozen_string_literal: true

class Computations::StderrsController < ApplicationController
  def show
    @computation = Computation.find(params[:computation_id])
    authorize(@computation, :show?)

    if @computation.stderr.attached?
      redirect_to rails_blob_path(@computation.stderr)
    else
      render plain: @computation.stderr_live.string
    end
  end
end
