# frozen_string_literal: true

class Computations::ApiController < ActionController::API
  before_action do
    load_organization!

    @computation = Computation.find_by!(id: params[:computation_id])
    render json: {
      error: "Unauthorized", status: 401
    }, status: :unauthorized unless file_manipulation?
  rescue ActiveRecord::RecordNotFound
    render json: { error: "Computation or organization not found", status: 404 }, status: :not_found
  end

  private
    def load_organization!
      Current.organization = Organization.find_by!(id: request.env["mee.organization_id"])
    end

    def file_manipulation?
      @computation.file_manipulation?(params[:secret])
    end

    def user
      @computation.user
    end
end
