# frozen_string_literal: true

class Computations::FilesController < Computations::ApiController
  def show_by_type
    serve_file(@computation.pick_file_by_type(params[:type]))
  end

  def show_by_filename
    serve_file(@computation.pick_file_by_filename(params[:filename], type: params[:type]))
  end

  def all_filenames
    unless ["pipeline_inputs", "pipeline_outputs", "patient_inputs"].include? params[:type]
      render json: { error: "Invalid type", status: 400 }, status: :bad_request
      return
    end

    render json: { files: @computation.all_filenames(type: params[:type]) }
  end

  def create
    if params[:file].nil?
      render json: { error: "No file provided", status: 400 }, status: :bad_request
      return
    end

    if @computation.add_output(params[:file])
      render json: { message: "Upload successful", status: 200 }, status: :created
    else
      render json: { error: "Upload failed", status: 500 }, status: :internal_server_error
    end
  end

  def serve_file(file)
    if file
      redirect_to url_for(file)
    else
      render json: {
        error: "No file found",
        status: 404
      }, status: :not_found
    end
  end
end
