# frozen_string_literal: true

class Organizations::GitConfigsController < RootApplicationController
  include GitConfigs

  private
    def module_type
      "organization"
    end
end
