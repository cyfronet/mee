# frozen_string_literal: true

class FileBaseController < ApplicationController
  private
    def filter_files(files, organization)
      Files::Filter.new(query_params, files, organization).call
    end

    def query_params
      params.slice(:name, :file_type)
    end
end
