# frozen_string_literal: true

class RootApplicationController < ActionController::Base
  include Authentication
  include Pundit::Authorization
  include Sentryable
  include ErrorRescues

  allow_unauthenticated_access

  layout "root"

  def pundit_user
    Current.user
  end
end
