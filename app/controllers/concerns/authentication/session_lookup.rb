
# frozen_string_literal: true

module Authentication::SessionLookup
  def find_user_by_cookie
    User.find_by(id: cookies.signed[:user_id])
  end
end
