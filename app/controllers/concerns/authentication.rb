# frozen_string_literal: true

module Authentication
  extend ActiveSupport::Concern
  include SessionLookup

  included do
    before_action :restore_authentication
    before_action :require_authentication
    helper_method :signed_in?
  end

  class_methods do
    def allow_unauthenticated_access(**options)
      skip_before_action :require_authentication, **options
    end
  end

  private
    def signed_in?
      Current.user.present?
    end

    def require_authentication
      signed_in? || request_authentication
    end

    def restore_authentication
      user = find_user_by_cookie
      if user && user.credentials_valid?
        authenticated_as(user)
      end
    end

    def request_authentication
      cookies[:return_to_after_authenticating] = request.url

      redirect_to root_path
    end


    def post_authenticating_url
      cookies.delete(:return_to_after_authenticating) || root_url
    end

    def authenticated_as(user)
      Current.user = user
      cookies.signed.permanent[:user_id] = { value: user.id, httpsonly: true, same_site: :lax }
    end

    def reset_authentication
      cookies.delete(:user_id)
    end
end
