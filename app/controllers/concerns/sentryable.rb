# frozen_string_literal: true

module Sentryable
  extend ActiveSupport::Concern

  included do
    before_action :set_sentry_context, if: :sentry_enabled?
  end

  def external_error_log(message)
    Sentry.capture_message(message) if sentry_enabled?
  end

  private
    def set_sentry_context
      if Current.user
        Sentry.set_user(id: Current.user.id,
                        email: Current.user.email,
                        username: Current.user.name)
      end
    end

    def sentry_enabled?
      Rails.env.production?
    end
end
