# frozen_string_literal: true

class HelpController < ApplicationController
  def index
    authorize(HelpItem)
  end

  def show
    help_item = HelpItem.for(clean_path_info(path_params[:category]),
                             path_params[:file])
    authorize(help_item)

    @title = help_item.title
    @markdown = File.read(help_item.path)
  rescue ArgumentError, Errno::ENOENT
    render "errors/not_found.html.haml", layout: "errors", status: :not_found
  end

  private
    def path_params
      params.require(:category)
      params.require(:file)

      params
    end

    PATH_SEPS = Regexp.union(*[::File::SEPARATOR, ::File::ALT_SEPARATOR].compact)

    # Taken from ActionDispatch::FileHandler
    # Cleans up the path, to prevent directory traversal outside the doc folder.
    def clean_path_info(path_info)
      parts = path_info.split(PATH_SEPS).reject { |p| p.empty? || p == "." }

      clean = []

      # Walk over each part of the path
      parts.each do |part|
        # Turn `one/two/../` into `one` or add simple folder names to the clean path.
        part == ".." ? clean.pop : clean << part
      end

      # If the path was an absolute path (i.e. `/` or `/one/two`),
      # add `/` to the front of the clean path.
      clean.unshift "/" if parts.empty? || parts.first.empty?

      # Join all the clean path parts by the path separator.
      ::File.join(*clean)
    end
end
