# frozen_string_literal: true

class Pipelines::OutputsController < FileBaseController
  before_action :find_and_authorize

  def index
    @files_exist = @pipeline.outputs.any?
    @files = filter_files(@pipeline.outputs, @pipeline.organization)
  end

  private
    def find_and_authorize
      @pipeline = Pipeline.find(params[:pipeline_id])
      authorize(@pipeline, :show?)
    end
end
