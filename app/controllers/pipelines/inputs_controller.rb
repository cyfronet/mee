# frozen_string_literal: true

class Pipelines::InputsController < FileBaseController
  before_action :find_and_authorize

  def index
    @files_exist = @pipeline.inputs.any?
    @files = filter_files(@pipeline.inputs, @pipeline.organization)
  end

  def create
    @pipeline.add_input(params[:pipeline][:files])
    redirect_back fallback_location: root_path, notice: t(".success")
  rescue StandardError
    redirect_back fallback_location: root_path, alert: t(".failure")
  end

  private
    def find_and_authorize
      @pipeline = Pipeline.find(params[:pipeline_id])
      authorize(@pipeline, :show?)
    end
end
