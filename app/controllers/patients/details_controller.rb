# frozen_string_literal: true

class Patients::DetailsController < ApplicationController
  before_action :find_and_authorize

  def show
    if request.xhr?
      @details = Patients::Details.new(@patient.slug, Current.user).call
      render partial: "patients/details", layout: false,
             locals: { patient: @patient, details: @details }
    end
  end

  private
    def find_and_authorize
      @patient = policy_scope(Patient).find_by!(slug: params[:patient_id])
      authorize(@patient)
    end
end
