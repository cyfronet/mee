# frozen_string_literal: true

class Patients::UploadsController < ApplicationController
  def create
    Patient::Uploader.create(organization:, cohort:, status: :processing, patient_archive: params[:patient_uploader][:file])
    redirect_back(fallback_location: root_path)
  end

  private
    def organization
      Current.organization
    end

    def cohort
      Cohort.find(params[:cohort_id]) if params[:cohort_id]
    end
end
