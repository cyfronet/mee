# frozen_string_literal: true

class Patients::FilesController < FileBaseController
  def index
    @patient = Patient.find(params[:patient_id])
    authorize(@patient)
    @files_exist = @patient.inputs.any?
    @files = filter_files(@patient.inputs, @patient.organization)
  end

  def create
    patient = Patient.find_by(slug: params[:patient_id])
    patient.add_input(params[:patient][:files])
    redirect_back fallback_location: root_path, notice: t(".success")
  rescue StandardError
    redirect_back fallback_location: root_path, alert: t(".failure")
  end
end
