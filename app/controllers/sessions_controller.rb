# frozen_string_literal: true

class SessionsController < ActionController::Base
  include Authentication, Sentryable

  skip_before_action :verify_authenticity_token, only: :create
  allow_unauthenticated_access only: :create

  rescue_from Plgrid::Ccm::FetchError do
    external_error_log("Unable to fetch short ssh key for #{auth.info["nickname"]}")

    redirect_to root_path, alert: <<~MSG
      Failed to retrieve the short-lived SSH key.
      Our support team has been notified and is actively working to resolve the issue.
    MSG
  end

  def create
    user = User.from_plgrid_omniauth(auth).tap { |u| u.save! }

    authenticated_as(user)
    start_computations(user)

    redirect_to post_authenticating_url
  end

  def destroy
    reset_authentication

    redirect_to root_path
  end

  private
    def auth
      request.env["omniauth.auth"]
    end

    def start_computations(user)
      Pipeline.automatic.where(user:).
        find_each { |p| Pipelines::StartRunnableJob.perform_later(p) }
    end
end
