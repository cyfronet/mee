# frozen_string_literal: true

require_relative "boot"

require "rails"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_view/railtie"
require "action_mailer/railtie"
require "active_job/railtie"
require "action_cable/engine"
# require "action_mailbox/engine"
require "action_text/engine"
require "rails/test_unit/railtie"

require File.expand_path("lib/organization_slug")

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mee
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 8.0

    config.autoload_lib(ignore: %w(assets tasks templates))

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.mission_control.jobs.base_controller_class = "AdminApplicationController"
    config.mission_control.jobs.http_basic_auth_enabled = false

    config.active_record.encryption.support_unencrypted_data = true

    # Custom error pages
    config.exceptions_app = routes

    config.constants = config_for(:application)

    config.checks = Struct.new(:before_allocation_expire).new(48.hours)

    config.i18n.load_path += Dir[Rails.root.join("config", "locales", "**", "*.{rb,yml}")]

    config.cache_store = :redis_cache_store, {
      url: config.constants["redis_url"],
      namespace: "cache:mee",
      expires_in: 90.minutes
    }

    config.generators do |g|
      g.test_framework :test_unit
      g.factory_bot false
    end

    config.middleware.use OrganizationSlug::Extractor
    config.middleware.use OrganizationSlug::LimitSessionToAccountSlugPath

    config.hpc_client = "::Plgrid::HpcClient"
  end
end
