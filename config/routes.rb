# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  constraints(-> { _1.subdomain.present? }) do
    match "(*path)", via: :all, to: "subdomains#redirect", format: false
  end

  constraints(-> { _1.env["mee.organization_id"] }) do
    root to: "welcome#index"

    resource :membership, only: [:show, :create, :update]

    ## User profile section routes
    resource :profile, only: :show

    resources :patients, except: [:edit, :update], constraints: { id: /.+/ } do
      resources :pipelines, only: [:index, :new, :create]
      scope module: :patients do
        resources :files, only: [:index]
        resource :details, only: :show
        post "inputs", to: "files#create", as: :inputs
      end
    end

    resources :comparisons, only: [:index]

    resources :pipelines do
      scope module: :pipelines do
        resources :inputs, only: [:index, :create]
        resources :outputs, only: [:index]
      end
    end

    resources :computations, only: [:show, :update] do
      scope module: :computations do
        resource :actions, only: :show
        resource :abort, only: [:create]
        resource :stdout, only: :show
        resource :stderr, only: :show
        get "files/:secret/type/:type", to: "files#show_by_type", as: :type_inputs
        get "files/:secret/filename/:filename", constraints: { filename: /.*/ }, to: "files#show_by_filename", as: :filename_inputs
        post "files/:secret", to: "files#create", as: :outputs
        get "files/:secret/all_filenames", to: "files#all_filenames", as: :all_filenames
        resource :artifacts, only: :show, path: "artifacts/:secret/:name", constraints: { name: /.*/ }
        resource :artifacts, only: :create, path: "artifacts/:secret", as: :upload_artifact
      end
    end

    get "steps/*step_id/versions", to: "steps/versions#index", as: :step_versions

    resources :artifacts, only: :index
    post "artifacts_upload", to: "artifacts#upload", as: :upload_artifact

    scope module: :organizations do
      resources :artifacts, only: [:edit, :update]
      get "files/:file_id/artifacts/new", to: "artifacts#new", as: :new_artifact_from_file
      post "files/:file_id/artifacts", to: "artifacts#create", as: :create_artifact_from_file
    end

    # Help
    get "help" => "help#index"
    get "help/:category/:file" => "help#show",
        as: :help_page,
        constraints: { category: /.*/, file: %r{[^/.]+} }

    resources :parameters, only: :show

    resources :flows do
      scope module: :flows do
        resource :pipeline_parameters, only: :show
      end
    end
    resources :steps do
      scope module: :steps do
        resources :git_configs, only: [:index, :show]
      end
    end

    resources :cohorts do
      scope module: :cohorts do
        resources :patients, only: [:destroy, :create, :index]
        resources :campaigns, only: [:index]
      end
    end

    post "patients/upload", to: "patients/uploads#create", as: :upload_patients

    resources :campaigns, except: [:edit, :update] do
      scope module: :campaigns do
        resource :run, only: :create
      end
    end

    resources :data_file_types, except: :show

    namespace :admin do
      resources :users
      resource :organization, only: [:show, :update, :destroy] do
        scope module: :organizations do
          resources :git_configs, only: :show
          resource :data_repository_config, only: :show
        end
      end
      resources :allocations, except: :show
      resources :licenses, except: :show
    end

    delete "files/*id",
           to: "files#destroy",
           constraints: { id: /.*/ },
           as: :file
  end

  constraints(-> { !_1.env["mee.organization_id"] }) do
    root to: "home#index", as: :home_root
    resources :home, only: [:index]
    resources :organizations, only: [:new, :create]
    namespace :organizations do
      resources :git_configs, only: :show
    end

    get "organization_not_found", to: "home#not_found", as: :organization_not_found
    get "organization_not_authorized", to: "home#not_authorized", as: :organization_not_authorized

    mount MissionControl::Jobs::Engine, at: "/admin/jobs"
  end

  constraints(-> { _1.subdomain.blank? }) do
    delete "logout", to: "sessions#destroy", as: "logout"
    match "auth/:provider/callback", to: "sessions#create", via: [:get, :post]
    get "auth/failure", to: redirect("/")
  end

  match "/404", to: "errors#not_found", via: :all
  match "/422", to: "errors#unprocessable", via: :all
  match "/500", to: "errors#internal_server_error", via: :all
end
# rubocop:enable Metrics/BlockLength
