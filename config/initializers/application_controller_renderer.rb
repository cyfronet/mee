# frozen_string_literal: true

ActiveSupport::Reloader.to_prepare do
  ApplicationController.renderer.defaults.merge!(
    http_host: ENV["HOST"],
    https: true
  )
end
