# frozen_string_literal: true

# Automatically enable YJIT as of Ruby 3.3, as it bring very
# sizeable performance improvements.

if defined? RubyVM::YJIT.enable
  Rails.application.config.after_initialize do
    RubyVM::YJIT.enable
  end
end
