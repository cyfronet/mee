# MEE - EurValve portal [![build status](https://gitlab.com/cyfronet/mee/badges/master/build.svg)](https://gitlab.com/cyfronet/mee/commits/master)

## Project description

MEE is a portal framework for patient cohort analysis. The aim of MEE is to provide a one-stop environment for
large-scale studies which involve multpile runs of simulation pipelines upon a range of input cases (i.e. individual
patients). Such analyses permit computational scientists to manage representative patient models, schedule HPC
simulations, download results and manage experimental pipelines.

MEE provides:

  * A consistent, Web-based GUI
  * HPC access automation, including staging of input data and retrieval of results from HPC storage
  * Creation and management of customizable execution environments for multiple organizations
  * A uniform security model, permitting authentication and authorization when accessing any of the above

MEE is intended for computational scientists and medical IT professionals.

## Development

MEE is build as a classic Ruby on Rails application. As a result it can be
started in development environment simply by invoking `./bin/dev` command. MEE
computation is starting job on the HPC cluster, which is consuming and
publishing files stored in MEE active storage. Since the application started in
the development environment binds to the `localhost` a specific mechanism
(tounneling by using `ngrok`) is needed.

### ngrok

If you use `stage_in` or `stage_out` commands in computations, you will need a
way to contact `localhost` from the cluster. To achieve it, we're using ngrok.
It enables you to get `localhost` out in the world.

First you have to [install and setup ngrok](https://ngrok.com/download)

Then, open terminal and run this command
```
ngrok http 3000
```

We automatically discover ngrok URL and extract host from it. If you want to
override you can set `HOST` environment variable:

```bash
export HOST=mee.public.host
```
In case you need the url ngrok set up for you, it is in the terminal where you ran `ngrok` command.
Ngrok limits the amount of requests in the free account, so do not run it unless you need it (the limit resets at the beginning of the month)

### Running

To start only web application run:
```
bin/dev
```

We are also using [solid_queue](https://github.com/rails/solid_queue) to execute
and schedule delayed jobs. Rails server and solid_queue are executed as a
separate processes. We are managing these processes by using
[overmind](https://github.com/DarthSim/overmind) (more advanced, with tmux
support) or [foreman](https://github.com/ddollar/foreman).

### Sample data

To load sample data for demo organizations you need to get an SSH key file for
the project's pipelines from one of the team members and set it's location in
`PIPELINE_SSH_KEY` environment variable. You also need to create a private
GitLab API access token and write it into `GITLAB_API_PRIVATE_TOKEN` environment
variable.

For example:
```
export PIPELINE_SSH_KEY="./config/mee_ssh_key"
export GITLAB_API_PRIVATE_TOKEN="FAqDZyC_ap2gVyBLTL72"
```

Then run following commands:
```
./bin/rails eurvalve:setup
./bin/rails primage:setup
./bin/rails gemini:setup
./bin/rails sano:setup
```


## Dependencies

asdf-vm can be used to install dependencies below ([installation guide](https://asdf-vm.com/#/core-manage-asdf-vm?id=install-asdf-vm))

  * MRI 2.6.x (`asdf plugin add ruby`)
  * NodeJS (`asdf plugin add nodejs`)

Then run `asdf install`

  * libvips (`sudo apt-get install libvips`)
  * PostgreSQL (`sudo apt-get install postgresql`)
  * PostgreSQL citext extension (`sudo apt-get install postgresql-contrib`)
  * PostgreSQL libpq-dev (`sudo apt-get install libpq-dev`)
  * Redis (`sudo apt-get install redis-server`)
  * Yarn ([installation guide](https://classic.yarnpkg.com/en/docs/install#debian-stable))

## DBMS Settings

You need to create user/role for your account in PostgreSQL. You can do it
using the 'createuser' command line tool. Then, make sure to alter this user
for rights to create databases.

## Installation

Run this command. It will, by default, create databases `mee_development` and `mee_test`.
```
bin/setup
```
If it does not complete successfully, do the next step.

### (optional) Manual activation of the citext extension

Skip this step if the previous one completed successfully.

As the PostgreSQL superuser, run the `CREATE EXTENSION IF NOT EXISTS citext
   WITH SCHEMA public;` on all databases (dev, test, ...) to activate the
   extension. So login to the `mee_development` database as the superuser (it is
   usually called 'postgres') and issue the CREATE EXTENSION command above. Then
   switch to `mee_test` and do the same.

When done, issue the `bin/setup` command again as a regular system user.
This time it should have no problem completing.

## Configuration

You need to:
* copy config/puma.rb.example into config/puma.rb
  and edit as required (env, location, socket/tcp). You probably want to have `application_path` set to `"."` and use tcp port 3000.
* create required directories defined in the config in tmp (such as pids). This step is not necessary if you configure `application path` as `"."` because required directories already exist in the project's repository.

## ENV variables

We use ENV variables to keep secrets safe. To customize the application
you can set the following ENV variables:

  * `REDIS_URL` (optional) - redis database url
  * `SENTRY_DN` - if present MEE will sent error and performance information to
    Sentry
  * `SENTRY_ENVIRONMENT` - if present overrides sentry environment (default set
    to `Rails.env`)
  * `USER_OVERRIDE` (only for demo organization generation) - override system
    user (see `DemoOrganization#path` for details).
  * `ADMIN_PLGRID_LOGIN` (only in development environment) - if defined
    administrator with specified PLGrid login will be created.
  * `HOST` (optional in local env) - enables to run computations in dev environment using ngrok (see ngrok section)
  * `GITLAB_API_PRIVATE_TOKEN` and `GITHUB_API_PRIVATE_TOKEN` - tokens used to seed demo organization. See [Setting up demo organization repo access](#setting-up-demo-organization-repo-access) to know how to create them.

## Active Storage S3
On the production environment we are using S3 to store patients/pipelines inputs
and outputs. What is more we are using Dropzone with direct upload to upload
patient or pipeline inputs. The direct S3 upload requires special cors
configuration:
  1. configure `s3cmd` (`s3cmd --configure`) for you S3 account
  2. create `cors.xml` with following content (replace `__DOMAIN__` with the MEE
     host name):
    ```xml
      <CORSConfiguration>
        <CORSRule>
          <AllowedOrigin>https://__DOMAIN__</AllowedOrigin>
          <AllowedMethod>PUT</AllowedMethod>
          <AllowedHeader>*</AllowedHeader>
          <ExposeHeader>Origin</ExposeHeader>
          <ExposeHeader>Content-Type</ExposeHeader>
          <ExposeHeader>Content-MD5</ExposeHeader>
          <ExposeHeader>Content-Disposition</ExposeHeader>
          <MaxAgeSeconds>3600</MaxAgeSeconds>
        </CORSRule>
      </CORSConfiguration>
    ```
    3. set CORS: `s3cmd setcors cors.xml s3://__BUCKET_NAME__`
    4. check CORS: `s3cmd info s3://__BUCKET_NAME__`

## Setting up demo organization repo access
To set up demo organization, you will need deploy key and ssh key.
- Deploy key is created inside the repo. [Github documentation](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/managing-deploy-keys) [Gitlab deploy key documentation](https://docs.gitlab.com/ee/user/project/deploy_keys/).
- SSH key is created inside user profile [Github SSH key documentation](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) [Gitlab SSH key documentation](https://docs.gitlab.com/ee/user/ssh.html)

SSH key is used in git client configuration form in private token field. Deploy key is uploaded as in 'Download key file' field.

When setting git client configuration up, remember to choose appropriate client type.

Both demo projects (`cyfronet/mee-demo-steps`) are available publicly, but you need to contact developers for the deploy key.

## Testing

System tests require Chrome headless installed. Please take a look at:
https://developers.google.com/web/updates/2017/04/headless-chrome for manual. To
install chrome on your debian based machine use following snippet:

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
```

To execute all tests run:

```
./bin/rails test:all
```
## Internationalization by [i18n-task](https://github.com/glebm/i18n-tasks) tool

In tests automatically runs health (missing and unused) of translations using.

You can find configuration file at __*config/i18n-tasks.yml*__

To check the state of translations, just run
```rb
i18n-tasks health
```
Remove translation from *.yml* file by command
```rb
i18n-tasks rm key-name
# or all
i18n-tasks unused -f yaml | i18n-tasks data-remove
```
Add missing translations to en.yml file
```rb
i18n-tasks add-missing -v 'TRME %{value}' en
```

### Config ignoring

In file *config/i18n-tasks.yml*, at section named: **ignore_unused** are some paths e.g.
```rb
- "admin.licenses.empty.*"
```
Tool don't recognize where translations are used. So we can configure to ignore this "unused" path as is line up.


## Using bullet to increase application performance
[Bullet](https://github.com/flyerhzm/bullet) gem is enabled in _development_ and _test_ environments.
While running application in development or running tests _bullet_ logs warnings to _log/bullet.log_ file.

## Contributing

1. Fork the project
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new pull request
6. When feature is ready add reviewers and wait for feedback (at least one
   approve should be given and all review comments should be resolved)
