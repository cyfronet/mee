# frozen_string_literal: true

require "test_helper"

class NotifierTest < ActionMailer::TestCase
  test "account approved: notify user" do
    email = Notifier.account_approved(user: users("user"),
                                      organization: organizations("main")).deliver_now

    assert_match "has been approved", email.body.encoded
    assert_equal [users("user").email], email.to
  end

  test "credentials expired: notify user" do
    email = Notifier.credentials_expired(users("user")).deliver_now

    assert_match "short lived PLGrid credentials has expired", email.body.encoded
    assert_equal [users("user").email], email.to
  end

  test "allocation will expire: notify admin users" do
    assert_emails organizations("main").admins.count do
      @email = Notifier.allocation_expired(organization: organizations("main"),
                                           latest_allocation_expiration: Date.today).deliver_now
    end

    assert_match "you will run out of allocations", @email.body.encoded
    assert_equal organizations("main").admins.pluck(:email), @email.to
  end

  test "configuration error: notify admin users" do
    assert_emails organizations("main").admins.count do
      @email = Notifier.configuration_error(
        errorable: organizations("main"),
        error_message: "configuration error"
      ).deliver_now
    end

    assert_match "configuration error", @email.body.encoded
    assert_equal organizations("main").admins.pluck(:email), @email.to
  end
end
