# frozen_string_literal: true

require "test_helper"

OmniAuth.config.test_mode = true

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium,
            using: ENV["SHOW_BROWSER"].present? ? :chrome : :headless_chrome,
            screen_size: [1400, 1400] do |opts|
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-dev-shm-usage")
  end

  def login_as(user, teams: organizations("main").plgrid_team_id, omniauth: false)
    omniauth ? login_via_omniauth(user, teams:) : login_via_cookie(user, teams:)
  end

  def login_via_omniauth(user, teams:)
    stub_plgrid_oauth(user, teams)

    visit "#{root_path}auth/plgrid"
  end

  def login_via_cookie(user, teams:)
    visit "/up" # fast to load page

    cookie_jar = ActionDispatch::TestRequest.create.cookie_jar
    cookie_jar.signed[:user_id] = user.id
    page.driver.browser.manage.add_cookie(name: "user_id", value: cookie_jar[:user_id])

    user.manage_memberships([teams].flatten)

    visit root_path
  end

  Capybara.app_host = "http://mee.lvh.me"
end

Capybara.configure do |config|
  config.always_include_port = true
  config.default_max_wait_time = 3
  config.server = :puma, { Silent: true }
end
