# frozen_string_literal: true

class Plgrid::HpcClientMock
  def initialize(host:, user:)
    @host = host
    @login = user.plgrid_login
  end

  def sftp_start(&block)
    block.call(SftpMock.new(@host, @login))
  end

  def read(path)
    SftpMock.new(@host, @login).get(path)
  end

  def submit(script, working_directory)
  end

  def check_status(job_ids)
  end

  def abort(job_id)
  end

  class << self
    def stub_abort(job_id, status: 200, body: "OK")
      response = ResponseMock.new(status, body)
      Plgrid::HpcClientMock.any_instance.stubs(:abort)
      .with(job_id)
      .returns(Plgrid::HpcJob.new(response:, host: "mocked_host"))
    end

    def stub_check_status(job_ids, status: 200, body: "OK")
      response = ResponseMock.new(status, body)
      Plgrid::HpcClientMock.any_instance.stubs(:check_status)
      .with(job_ids)
      .returns(Plgrid::HpcJob.new(response:, host: "mocked_host"))
    end

    def stub_submit(script, working_directory, status: 201, body: "OK")
      response = ResponseMock.new(status, body)
      Plgrid::HpcClientMock.any_instance.stubs(:submit)
      .with(script, working_directory)
      .returns(Plgrid::HpcJob.new(response:, host: "mocked_host"))
    end
  end

  private
    class SftpMock
      def initialize(host, login)
        @host = host
        @login = login
      end

      def get(path)
        StringIO.new(Plgrid::HpcMock.instance.read_file(@host, @login, path) || "")
      end
    end

    class ResponseMock
      def initialize(status, body)
        @status = status
        @body = body
      end

      attr_reader :status, :body
    end
end
