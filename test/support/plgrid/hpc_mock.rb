# frozen_string_literal: true

class Plgrid::HpcMock
  include Singleton

  def initialize
    reset!
  end

  class << self
    def stub_file(host, user, path, payload)
      instance.stub_file(host, user, path, payload)
    end

    def reset!
      instance.reset!
    end

    def stub_stdout(computation, payload)
      stub_file(computation.site.host,
                computation.user.plgrid_login,
                computation.stdout_path,
                payload)
    end

    def stub_stderr(computation, payload)
      stub_file(computation.site.host,
                computation.user.plgrid_login,
                computation.stderr_path,
                payload)
    end
  end

  def reset!
    @files = {}
  end

  def stub_file(host, user, path, payload)
    @files[file_key(host, user, path)] = payload
  end

  def read_file(host, user, path)
    @files[file_key(host, user, path)]
  end

  private
    def file_key(host, user, path)
      "#{user}@#{host}:#{path}"
    end
end
