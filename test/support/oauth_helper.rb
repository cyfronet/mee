# frozen_string_literal: true

module OauthHelper
  def stub_oauth(provider, options = {})
    OmniAuth.config.add_mock(
      provider,
      {
        info: {
          nickname: nil, name: nil,
          first_name: nil, last_name: nil,
          email: nil
        },
        extra: {},
        credentials: {},
        provider:,
        uid: "123"
      }.deep_merge(options)
    )
  end

  def stub_plgrid_oauth(user, teams = "")
    stub_oauth(
      :plgrid,
      {
        credentials: { token: "valid-token" },
        extra: {
          raw_info: {
            groups: teams
          }
        },
        info: {
          name: "#{user.first_name} #{user.last_name}",
          nickname: user.plgrid_login,
          email: user.email,
        }
      }
    )
  end
end
