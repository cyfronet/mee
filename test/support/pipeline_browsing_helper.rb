# frozen_string_literal: true

module PipelineBrowsingHelper
  def mock_rimrock_computation_ready_to_run
    Computation.any_instance.stubs(:runnable?).returns(true)
    Pipeline.any_instance.stubs(:data_files_present?).returns(true)
  end

  def computation_run_text(computation)
    I18n.t("computations.start_#{computation.mode}", step: computation.name)
  end
end
