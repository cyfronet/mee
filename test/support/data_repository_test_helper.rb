# frozen_string_literal: true

module DataRepositoryTestHelper
  def data_repository_test_setup
    computation = build(:computation)
    url = "https://data_repository.local"
    class_name = self.class.name.demodulize
    class_name.slice!("Test")

    config = "DataRepository::#{class_name.classify}".constantize.new(url:)
    computation.organization.stubs(:data_repository_config).returns(config)

    membership = memberships("user")

    [ computation, url, membership.data_repository_token ]
  end
end
