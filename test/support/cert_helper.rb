# frozen_string_literal: true

module CertHelper
  def private_key
    SSHKey.generate.private_key
  end
end
