# frozen_string_literal: true

module PunditHelper
  def assert_permit(user, record, action)
    assert permit(user, record, action),
           "User #{user.inspect} should be permitted to #{action} #{record}, but isn't permitted"
  end

  def assert_no_permit(user, record, action)
    assert_not permit(user, record, action),
               "User #{user.inspect} should NOT be permitted to #{action} #{record}, but is permitted"
  end

  def permit(user, record, action)
    policy(user, record).public_send("#{action}?")
  end

  def policy_scope(user, scope)
    "#{policy_class_name}::Scope".constantize.new(user, scope).resolve
  end

  def policy(user, record)
    policy_class_name.constantize.new(user, record)
  end

  def pundit_user
    UserContext.new(users("user"), organizations("main"), memberships("user"))
  end

  def other_organization_pundit_user
    UserContext.new(users("other"), organizations("other"), memberships("user"))
  end

  def stranger_pundit_user
    UserContext.new(users("stranger"), organizations("main"), memberships("stranger"))
  end

  def admin_pundit_user
    UserContext.new(users("admin"), organizations("main"), memberships("admin"))
  end

  private
    def policy_class_name
      self.class.to_s.gsub(/Test/, "")
    end
end
