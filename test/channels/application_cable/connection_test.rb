# frozen_string_literal: true

require "test_helper"

module ApplicationCable
  class ConnectionTest < ActionCable::Connection::TestCase
    test "connects with cookies" do
      cookies.signed[:user_id] = users(:user).id

      connect

      assert_equal users(:user), connection.current_user
    end

    test "rejects connection without cookies" do
      assert_reject_connection { connect }
    end
  end
end
