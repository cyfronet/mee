# frozen_string_literal: true

require "application_system_test_case"

class RootSystemTest < ApplicationSystemTestCase
  setup do
    in_root!
  end

  test "allows to logout" do
    user = users("user")

    login_as(user)

    visit root_path
    click_on user.name
    click_on "Logout"

    assert_text "Login"
  end
end
