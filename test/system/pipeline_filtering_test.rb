# frozen_string_literal: true

require "minitest/autorun"
require "application_system_test_case"

class PipelineFilteringTest < ApplicationSystemTestCase
  setup do
    @pipeline1 = create(:pipeline, user: users("user"))
    @pipeline2 = create(:pipeline, user: users("admin"), runnable: @pipeline1.runnable)
  end

  test "hides unchecked pipeline names" do
    in_organization! organizations("main")
    login_as(users("user"))

    visit patient_pipelines_path(@pipeline1.runnable)
    fill_in "pipeline-name-filter", with: @pipeline1.name
    within "div.pipeline-filters" do
      click_on "Filter"
    end

    assert_no_content @pipeline2.name
    assert_content @pipeline1.name
  end

  test "hides unchecked pipelines by owner" do
    in_organization! organizations("main")
    login_as(users("user"))

    visit patient_pipelines_path(@pipeline1.runnable)
    select @pipeline1.owner_name, from: "pipeline-owner-filter"
    within "div.pipeline-filters" do
      click_on "Filter"
    end

    assert_no_content @pipeline2.name
    assert_content @pipeline1.name
  end

  test "hides unchecked pipelines by pipeline status" do
    in_organization! organizations("main")
    login_as(users("user"))

    @pipeline1.computations << create(:computation, status: "running")

    visit patient_pipelines_path(@pipeline1.runnable)
    select "waiting", from: "pipeline-status-filter"
    within("div.pipeline-filters") do
      click_on "Filter"
    end

    assert_no_content @pipeline1.name
    assert_content @pipeline2.name
  end
end
