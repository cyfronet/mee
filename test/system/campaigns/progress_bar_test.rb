# frozen_string_literal: true

require "application_system_test_case"

class Campaigns::ProgressBarTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("admin")
    @campaign = create(:campaign)
  end

  test "doesn't show progress bar for initializing campaign" do
    @campaign.update(status: :initializing)
    visit campaign_path(@campaign)

    assert_no_text "Campaign computation progress"
    within("div#progress_bar_campaign_card_#{@campaign.id}") do
      assert_no_selector ".progress"
    end
  end

  test "doesn't show progress bar for creating pipelines failed campaign" do
    @campaign.update(status: :creating_pipelines_failed)
    visit campaign_path(@campaign)

    assert_no_text "Campaign computation progress"
    within("div#progress_bar_campaign_card_#{@campaign.id}") do
      assert_no_selector ".progress"
    end
  end

  test "shows progress bar for running campaign" do
    @campaign.update(status: :running)
    visit campaign_path(@campaign)

    assert_text "Campaign computation progress"
    within("div#progress_bar_campaign_card_#{@campaign.id}") do
      assert_selector ".progress"
    end
  end

  test "shows progress bar for finished campaign" do
    @campaign.update(status: :finished)
    visit campaign_path(@campaign)

    assert_text "Campaign computation progress"
    within("div#progress_bar_campaign_card_#{@campaign.id}") do
      assert_selector ".progress"
    end
  end
end
