# frozen_string_literal: true

require "application_system_test_case"
require "minitest/autorun"

class Campaigns::RunTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  setup do
    @cohort = create(:cohort, patients: create_list(:patient, 3))
    @campaign = create(:campaign, cohort: @cohort, status: 1, pipelines:
      build_list(:pipeline, 3))

    in_organization! organizations("main")
    login_as users("admin")
  end

  test "can see computation statuses" do
    @campaign.pipelines.each do |pipeline|
      create(:computation, status: "queued", pipeline:,
             parameter_values: [
               ParameterValue::ModelVersion.new(
                 parameter: parameters("first_tag_or_branch"),
                 version: "master"
               )
             ])
    end
    visit campaign_path(@campaign)
    assert_css ".fa-spin", count: 3
  end

  test "updates status as running" do
    visit campaign_path(@campaign)
    perform_enqueued_jobs do
      click_on I18n.t("campaigns.view.run.created")
    end
    assert_text I18n.t("campaigns.view.run.running")
  end
end
