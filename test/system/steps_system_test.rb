# frozen_string_literal: true

require "application_system_test_case"

class StepsSystemTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "Organization member can create new step" do
    visit steps_path

    click_on "New pipeline step"

    fill_in "Name", with: "My new step"
    fill_in "Repository", with: "cyfronet/test"
    fill_in "File", with: "run.sh.liquid"
    select "cpu", from: "Resource type"
    select "Ares", from: "Site"

    assert_difference "Step.count" do
      click_on "Create new pipeline step"
      assert_text "My new step"
      assert_text "Step was created successfully"
    end
  end

  test "Owner can edit step" do
    step = steps("first")

    visit edit_step_path(step)

    fill_in "Name", with: "updated name", match: :first
    version_parent_scope = find_field("Key", with: "tag-or-branch", disabled: :all).find(:xpath, "../../..")
    version_parent_scope.fill_in "Name", with: "Model version updated"

    allocation_parent_scope = find_field("Key", with: "allocation", disabled: :all).find(:xpath, "../../..")
    allocation_parent_scope.fill_in "Name", with: "Allocation updated"

    select "gpu", from: "Resource type"
    select "Athena", from: "Site"
    click_on "Save changes"

    assert_text "updated name"
    assert_text "Step was updated successfully"
    step.reload

    assert_equal "updated name", step.name
  end

  test "Should delete step when no depending flows" do
    step = create(:step)

    visit steps_path

    within :xpath, "//form[@action='#{step_path step}']" do
      click_on "Remove this pipeline step"
    end
    accept_modal do
      click_button "Delete"
    end

    assert_no_text step.name
    assert_text "Step was removed successfully"
  end
end
