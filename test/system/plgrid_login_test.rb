# frozen_string_literal: true

require "minitest/autorun"
require "application_system_test_case"

class PlgridLoginTest < ApplicationSystemTestCase
  test "new user is granted access when in organizations team" do
    in_organization! organizations("main")
    login_as(users("user"), omniauth: true)

    assert_content "#{users("user").first_name} #{users("user").last_name}"
  end

  test "new user is not granted access when not in organizations team" do
    in_organization! organizations("main")
    login_as(users("user"), teams: "", omniauth: true)

    assert_content "no access"
  end

  test "denies access to user when no longer in organizations team" do
    plgrid_user = users("user")
    in_organization! organizations("main")
    login_as(plgrid_user, omniauth: true)

    assert_equal "approved", plgrid_user.memberships.first.state
    find("a[class='nav-link dropdown-toggle user-profile']").click
    click_on("Logout")

    login_as(plgrid_user, teams: "", omniauth: true)
    assert_equal "blocked", plgrid_user.memberships.first.state
  end

  test "grants access to user that was added to team in the organization" do
    plgrid_user = users("user")
    in_organization! organizations("main")
    login_as(plgrid_user, teams: "", omniauth: true)
    assert_equal "blocked", plgrid_user.memberships.first.state

    click_on("Logout")

    login_as(plgrid_user, omniauth: true)
    assert_equal "approved", plgrid_user.memberships.first.state
  end

  test "after plgrid login proxy expired notification date is reseted" do
    john = create(:user, credentials_expired_at: Time.zone.now)
    in_organization! organizations("main")

    login_as(john, omniauth: true)
    john.reload

    assert_nil john.credentials_expired_at
  end
end
