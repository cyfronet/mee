# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::ManualComputationsTest < ApplicationSystemTestCase
  include GitlabHelper
  include PipelineBrowsingHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @patient = create(:patient, case_number: "1234")
    @pipeline = create(:pipeline, runnable: @patient, name: "p1", mode: :manual)
    @step = steps(:first)
    @computation = create(:computation, pipeline: @pipeline, step: @step)
    stub_repo_versions(@step, { branches: ["master"], tags: ["t1"] })
  end

  test "unable to start computation when version is not selected" do
    mock_rimrock_computation_ready_to_run
    visit computation_path(@computation)
    click_button computation_run_text(@computation)

    assert_text "can't be blank"
  end

  test "abort only shows when computation is running" do
    mock_rimrock_computation_ready_to_run
    visit computation_path(@computation)

    assert_no_link I18n.t("computations.show.abort")
  end

  test "computation alert is displayed when no required input data" do
    step = create(:step, required_file_types: [create(:data_file_type)])
    create(:flow_step, flow_id: @pipeline.flow_id, step_id: step.id)
    missing_resources_computation = create(:computation, pipeline: @pipeline, step:)

    visit computation_path(missing_resources_computation)
    msg = I18n.t("steps.missing_input", step: missing_resources_computation.name)

    assert_text msg.squish
    missing_resources_computation.step.required_file_types.each do |rft|
      assert_text rft.name
    end
  end

  test "displays computation stdout and stderr" do
    @computation.update(started_at: Time.current,
                                stdout_path: "stdout.pl",
                                stderr_path: "stderr.pl")

    visit computation_path(@computation)
    assert_link "stdout", href: computation_stdout_path(@computation)
    assert_link "stderr", href: computation_stderr_path(@computation)
  end
end
