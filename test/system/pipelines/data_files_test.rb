# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::DataFilesTest < ApplicationSystemTestCase
  include GitlabHelper
  include PipelineBrowsingHelper
  include ActiveJob::TestHelper
  include ActionDispatch::TestProcess::FixtureFile

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @patient = create(:patient, case_number: "1234")
    @pipeline = create(:pipeline, runnable: @patient, name: "p1")
    @dft = create(:data_file_type,
      name: "TestDataFileType",
      viewer: :text,
      data_type: "test_data_file_type", pattern: "^test_data_file.*\.txt$")
    @step = create(:step, required_file_types: [@dft])
    create(:flow_step, flow: @pipeline.flow, step: @step)
    @computation = create(:computation, pipeline: @pipeline, step: @step)
    mock_rimrock_computation_ready_to_run
  end

  test "allows to choose Data File to use when there is more than 1" do
    stub_repo_versions(@step, { branches: ["master"], tags: ["t1"] })
    stub_get_repo_file(@step, "master", Base64.encode64("script"))
    allocation = create(:allocation)
    Computations::StartJob.expects(:perform_later)

    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "test_data_file1.txt")
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "test_data_file2.txt")

    visit computation_path(@computation)

    assert_text @dft.name
    assert_text @pipeline.inputs_blobs.first.filename
    assert_text @pipeline.inputs_blobs.second.filename

    select("master")
    select(allocation.name)
    select(@pipeline.inputs_blobs.second.filename.to_s)
    perform_enqueued_jobs do
      click_button computation_run_text(@computation)
      assert_text :all, "is being submitted"
    end

    @computation.reload
    assert_equal @pipeline.inputs_blobs.second, @computation.pick_file_by_type(@dft.data_type)
  end

  test "does not allow to choose Data File when there is only 1" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "test_data_file1.txt")

    visit computation_path(@computation)

    within(:xpath, ".//form[@action='#{computation_path(@computation)}']") do
      assert_no_text @dft.name
      assert_no_text @pipeline.inputs_blobs.first.filename
    end
  end
end
