# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::ComputationsTest < ApplicationSystemTestCase
  include GitlabHelper
  include PipelineBrowsingHelper
  include ActiveJob::TestHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @step = create(:step)
    create(:flow_step, step: @step)
    @computation = create(:computation, step: @step)
  end

  test "shows valid computation data" do
    @computation.update(parameter_values: [build(:model_version_parameter_value, parameter: parameters("first_tag_or_branch"))])
    visit computation_path(@computation)

    assert_text @computation.site.name.capitalize
    assert_text @computation.revision
    assert_text @computation.step.name
    assert_text I18n.t("computation.details_component.runnable")
  end

  test "show started computation source link for started step" do
    @computation.update(revision: "my-revision", started_at: Time.zone.now)

    visit computation_path(@computation)

    assert_link "my-revision", href: "https://gitlab.com/#{@step.repository}/tree/my-revision"
    assert_text I18n.l(@computation.started_at, format: :short)
  end

  test "computation source link is not shown when no revision" do
    visit computation_path(@computation)
    assert_no_link href: "https://gitlab.com/#{@step.repository}/tree"
  end

  test "alert is shown with discarded flow" do
    @computation.pipeline.flow.discard!

    visit computation_path(@computation)

    assert_text I18n.t("steps.readonly", step: @computation.name.downcase)
  end
end
