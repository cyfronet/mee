# frozen_string_literal: true

require "application_system_test_case"

class FlowsTest < ApplicationSystemTestCase
  include ChoiceJSHelper
  setup do
    in_organization! organizations("main")
    login_as users("user")
  end

  test "Show only organization flows" do
    other_org_flow = create(:flow, organization: organizations("other"))

    visit flows_path

    assert_text flows("first").name
    assert_no_text other_org_flow.name
  end

  test "User can create new flow" do
    visit flows_path

    click_on "New pipeline flow"

    fill_in "Name", with: "Newly created flow"
    choicejs_select steps("first").name, from: "Flow steps"
    assert_difference "Flow.count", 1 do
      click_on "Create new pipeline flow"
      assert_text "Newly created flow"
    end
  end

  test "User can edit owned existing flow" do
    flow = flows("first")
    visit edit_flow_path(flow)

    fill_in "Name", with: "updated name"
    click_on "Save changes"
    assert_content "updated name"
    assert_equal "updated name", flow.reload.name
  end

  test "User can discard flow" do
    flow = flows("first")
    create(:pipeline, flow:)
    visit flows_path

    assert_no_difference "Flow.count" do
      within :xpath, "//form[@action='#{flow_path flow}']" do
        click_on "Remove this pipeline flow"
      end
      accept_modal do
        click_button "Delete"
      end
    end

    assert_no_content flow.name
    flow.reload
    assert_not_nil flow.discarded_at
  end

  test "User can delete flow when no pipelines depend on it" do
    flow = flows("first")
    visit flows_path

    assert_changes "Flow.count", -1 do
      within :xpath, "//form[@action='#{flow_path flow}']" do
        click_on "Remove this pipeline flow"
      end
      accept_modal do
        click_button "Delete"
      end
      assert_no_text flow.name
    end
  end

  test "User can delete step when it is discarded and only used by this flow" do
    step = create(:step, discarded_at: Time.current)
    flow = create(:flow, flow_steps_attributes: [step:, position: 1])
    visit flows_path

    assert_difference %w[Flow.count Step.count], -1 do
      within :xpath, "//form[@action='#{flow_path flow}']" do
        click_on "Remove this pipeline flow"
      end
      accept_modal do
        click_button "Delete"
      end
      assert_no_text flow.name
    end
  end
end
