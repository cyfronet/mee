# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"

class Patients::IndexTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  test "allows to add a new patient" do
    create(:patient)
    visit patients_path

    assert_text I18n.t("patients.index.add")
    click_link I18n.t("patients.index.add")

    assert_current_path new_patient_path
  end

  test "lets navigate to a given patient case" do
    patient = create(:patient)
    visit patients_path

    assert_text patient.case_number
    click_link patient.case_number

    assert_current_path patient_path(patient)
  end

  test "deletes patient" do
    patient = create(:patient)
    visit patients_path

    assert_button I18n.t("patients.summary_component.destroy")
    assert_difference "Patient.count", -1 do
      click_button I18n.t("patients.summary_component.destroy")
      click_button "Delete", match: :first
      assert_text I18n.t("patients.destroy.success", case_number: patient.case_number)
    end
  end
end
