# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"

class Patients::ShowTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("user")
    @patient = create(:patient)
  end

  test "deletes patient" do
    visit patient_path(@patient)

    assert_button I18n.t("patients.show.remove")
    assert_difference "Patient.count", -1 do
      click_button I18n.t("patients.show.remove")
      click_button "Delete", match: :first
      assert_text  I18n.t("patients.destroy.success", case_number: @patient.case_number)
    end
  end

  test "remove button as owner" do
    pipeline = create(:pipeline, runnable: @patient, name: "p1")

    visit patient_path(@patient)

    assert_difference "Pipeline.count", -1 do
      click_button I18n.t("pipelines.show.delete")
      click_button "Delete", match: :first
      assert_text I18n.t("pipelines.destroy.success", name: pipeline.name)
    end
  end

  test "doesn't show remove button for non-owner" do
    create(:pipeline, runnable: @patient, user: users("admin"))

    visit patient_path(@patient)

    assert_no_link I18n.t("pipelines.show.remove")
  end

  test "add pipeline button" do
    visit patient_path(@patient)

    assert_link I18n.t("runnables.show.new")
    click_link I18n.t("runnables.show.new")

    assert_current_path new_patient_pipeline_path(@patient)
  end

  test "lets navigate to a given pipeline" do
    pipeline = create(:pipeline, runnable: @patient)
    visit patient_path(@patient)

    assert_text pipeline.name
    click_link pipeline.name

    assert_current_path pipeline_path(pipeline)
  end
end
