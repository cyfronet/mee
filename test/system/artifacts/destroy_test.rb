# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
module Artifacts
  class DestroyTest < ApplicationSystemTestCase
    include ActionDispatch::TestProcess::FixtureFile

    def setup
      @organization = organizations("main")
      in_organization! @organization
      login_as users("admin")
      @organization.artifacts.attach(io: fixture_file_upload("id_rsa"), filename: "output")
      @artifact = @organization.artifacts.first
      visit artifacts_path
    end

    test "it destroys artifact" do
      click_button I18n.t("artifacts.artifact.destroy")
      click_button "Delete", match: :first

      assert_text "File deleted"
      assert_no_text @artifact.filename
    end

    test "with failing storage shows failure error" do
      ActiveStorage::Attachment.any_instance.stubs(:purge_later).returns(false)

      click_button I18n.t("artifacts.artifact.destroy")
      click_button "Delete", match: :first

      assert_text "Failed to delete file"
    end
  end
end
