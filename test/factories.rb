# frozen_string_literal: true

# This file will be renamed to `factories.rb` after we will eliminate factory
# bot. Right now we cannot use this name, because factory bot tries to load it
# by using naming convention also in development mode.
class ActiveSupport::TestCase
  include FixtureFactory::Registry
  include FixtureFactory::Methods

  define_factories do
    factory :user do |n|
      {
        email: "johndoe#{n}@email.pl",
        first_name: "John#{n}",
        last_name: "Doe#{n}",
        plgrid_login: "plgjohndoe#{n}",
        terms: true
      }
    end

    factory :patient do |n|
      {
        case_number: "Case #{n}",
        organization: organizations("main"),
      }
    end

    factory :pipeline do |n|
      {
        name: "pipeline_#{n}",
        flow: flows("first"),
        user: users("user"),
        runnable: build(:patient)
      }
    end

    factory :cohort do |n|
      {
        name: "cohort_#{n}",
        organization: organizations("main"),
        patients: build_list(:patient, 1)
      }
    end

    factory :patient_uploader, class: -> { Patient::Uploader } do |n|
      {
        organization: organizations("main"),
        status: :processing,
      }
    end

    factory :campaign, class: -> { Campaign } do |n|
      {
        name: "campaign_#{n}",
        organization: organizations("main"),
        cohort: build(:cohort)
      }
    end

    factory :computation do |n|
      {
        step: steps("first"),
        script: "SCRIPT",
        revision: "1234",
        pipeline: build(:pipeline),
        stdout_path: "stdout_#{n}",
        stderr_path: "stderr_#{n}"
      }
    end

    factory :step do |n|
      {
        name: "step #{n}",
        repository: "cyfronet/step-repo#{n}",
        file: "script#{n}.sh.liquid",
        resource_type: resource_types("cpu"),
        organization: organizations("main"),
        user: users("user"),
        site: sites("ares")
      }
    end

    factory :flow, like: :first do |n|
      {
        name: "flow #{n}",
        flow_steps_attributes: [step: steps("first"), position: 1]
      }
    end

    factory :flow_step do |n|
      {
        flow_id: flows("first").id,
        step_id: steps("first").id,
        position: n
      }
    end
    factory :data_file_type do |n|
      {
        name: "Dft#{n}",
        data_type: "dft#{n}",
        pattern: "^dft.#{n}$",
        organization: organizations("main"),
        user: users("admin")
      }
    end

    factory :data_file do |n|
      {
        name: "data_file_#{n}.txt",
        data_type: data_file_types("image"),
        fileable: build(:patient)
      }
    end

    factory :license, like: :ansys do |n|
      {
        name: "license #{n}"
      }
    end

    factory :allocation, like: :cpu do |n|
      {
        name: "allocation #{n}",
        resource_types: [resource_types("cpu")],
        sites: [sites("ares")]
      }
    end

    factory :expired_allocation, like: :cpu, via: :allocations, class: -> { Allocation } do |n|
      {
        name: "expired allocation #{n}",
        ends_at: Date.yesterday,
        resource_types: [resource_types("cpu")],
        sites: [sites("ares")]
      }
    end

    factory :future_allocation, like: :cpu, via: :allocations, class: -> { Allocation } do |n|
      {
        name: "future allocation #{n}",
        starts_at: Date.tomorrow,
        resource_types: [resource_types("cpu")],
        sites: [sites("ares")]
      }
    end

    factory :git_config, class: -> { GitConfig::Native } do
      {
        host: "gitlab.com",
        download_key: SSHKey.generate.private_key
      }
    end

    factory :gitlab_git_config, parent: :git_config, class: -> { GitConfig::Gitlab } do |n|
      {
        private_token: "token#{n}"
      }
    end

    factory :model_version_parameter, like: :first_tag_or_branch, via: :parameters, class: -> { Parameter::ModelVersion }

    factory :parameter do |n|
      {
        key: "parameter#{n}",
        name: "Parameter name #{n}",
        step: steps("first")
      }
    end
    factory :allocation_parameter, parent: :parameter, class: -> { Parameter::Allocation }
    factory :number_parameter, parent: :parameter, class: -> { Parameter::Number }
    factory :string_parameter, parent: :parameter, class: -> { Parameter::String }
    factory :select_parameter, parent: :parameter, class: -> { Parameter::Select } do
      {
        values: [
          Parameter::Select::Value.new(name: "A", value: "a"),
          Parameter::Select::Value.new(name: "B", value: "b")
        ]
      }
    end

    factory :parameter_value do |n|
      {
        computation: build(:computation)
      }
    end
    factory :allocation_parameter_value, parent: :parameter_value, class: -> { ParameterValue::Allocation } do
      {
        parameter: build(:allocation_parameter)
      }
    end
    factory :model_version_parameter_value, parent: :parameter_value, class: -> { ParameterValue::ModelVersion } do
      {
        parameter: build(:model_version_parameter)
      }
    end
    factory :number_parameter_value, parent: :parameter_value, class: -> { ParameterValue::Number } do
      {
        parameter: build(:number_parameter)
      }
    end
    factory :select_parameter_value, parent: :parameter_value, class: -> { ParameterValue::Select } do
      {
        parameter: build(:select_parameter)
      }
    end
    factory :string_parameter_value, parent: :parameter_value, class: -> { ParameterValue::String } do
      {
        parameter: build(:string_parameter)
      }
    end
  end
end
