# frozen_string_literal: true

require "test_helper"

class TimeScopesHelperTest < ActionView::TestCase
  test "#time_scope_button_class" do
    assert_equal "bg-secondary", time_scope_button_class(build(:expired_allocation))
    assert_equal "bg-success", time_scope_button_class(build(:allocation))
    assert_equal "bg-info", time_scope_button_class(build(:future_allocation))
  end

  test "#time_scope_button_text" do
    assert_equal "outdated", time_scope_button_text(build(:expired_allocation))
    assert_equal "active", time_scope_button_text(build(:allocation))
    assert_equal "future", time_scope_button_text(build(:future_allocation))
  end
end
