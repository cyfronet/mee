# frozen_string_literal: true

require "test_helper"

class Pipelines::BuildTest < ActiveSupport::TestCase
  include GitlabHelper

  setup do
    @flow = flows("first")
    @user = users("user")
  end

  test "pass step version into rimrock based computations" do
    step = steps("first")
    stub_repo_versions(step, { branches: [], tags: ["rimrock-step-v1"] })

    patient = build(:patient, case_number: "Patient-1", slug: "patient 1")
    params_config = { step.slug => {
      "tag-or-branch" => { "version" => "rimrock-step-v1" }
    } }

    pipeline = Pipelines::Build.new(@user, patient,
      { name: "test", flow_id: @flow.id, mode: "manual" }, params_config).call

    computation = pipeline.computations.find { |c| c.step == step }

    assert_equal "rimrock-step-v1", computation.tag_or_branch
  end

  test "creates computations for all pipeline steps" do
    pipeline = Pipelines::Build.new(@user,
      build(:patient, case_number: "Patient 2", slug: "patient-2"),
      { name: "test", flow_id: @flow.id, mode: "manual" }, {}).call

    assert_equal @flow.steps.sort, pipeline.computations.map(&:step).sort
  end

  test "creates the computations in correct order" do
    fs1, fs2 = @flow.flow_steps

    fs1.update(position: 3)
    fs2.update(position: 1)
    @flow.reload

    pipeline = Pipelines::Build.new(@user,
      build(:patient, case_number: "Patient 2", slug: "patient-2"),
      { name: "test", flow_id: @flow.id, mode: "manual" }, {}).call

    assert_equal fs2.step, pipeline.computations.first.step
    assert_equal fs1.step, pipeline.computations.second.step
  end
end
