# frozen_string_literal: true

require "test_helper"

class Files::FilterTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile

  def setup
    @pipeline = create(:pipeline)
    @file1 = @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "random.zip")
    @file2 = @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "file.zip")
  end

  test "filter by name" do
    files = Files::Filter.new({ name: "random", file_type: "" }, @pipeline.outputs, @pipeline.organization).call
    assert_equal(1, files.count)
    assert_includes(files, @pipeline.outputs.first)
  end

  test "filter by type" do
    files = Files::Filter.new({ name: "", file_type: data_file_types("image").data_type }, @pipeline.outputs, @pipeline.organization).call
    assert_equal(1, files.count)
    assert_includes(files, @pipeline.outputs.second)
  end
end
