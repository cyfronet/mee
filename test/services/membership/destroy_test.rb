# frozen_string_literal: true

require "test_helper"

class Membership::DestroyTest < ActiveSupport::TestCase
  test "removes user membership" do
    assert_changes "Membership.count", -1 do
      result = Membership::Destroy.new(users("admin"), memberships("user")).call
      assert_equal :ok, result
    end
  end

  test "abort all active user computation" do
    computation = create(:computation, started_at: Time.current, status: :running)
    Plgrid::HpcClientMock.stub_abort(computation.job_id)

    Membership::Destroy.new(users("admin"), memberships("user")).call

    assert_equal "aborted", computation.reload.status
  end

  test "is fobidden to remove self" do
    assert_no_changes "Membership.count" do
      result = Membership::Destroy.new(users("admin"), memberships("admin")).call
      assert_equal :self, result
    end
  end
end
