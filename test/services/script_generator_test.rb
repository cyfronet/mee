# frozen_string_literal: true

require "test_helper"

class ScriptGeneratorTest < ActiveSupport::TestCase
  include GitlabHelper
  include ActionDispatch::TestProcess::FixtureFile
  include LinkHelper

  test "add error when active allocation cannot be found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    travel_to(1.year.from_now) do
      ScriptGenerator.new(computation, "{{ allocation_id }}", errors).call
    end


    assert_includes errors[:script], "active allocation cannot be found"
  end

  test "adds error when input file is not found" do
    computation = build(:computation)
    create(:data_file_type, data_type: "provenance")
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% stage_in provenance %}", errors).call

    assert_includes errors[:script],
                   "cannot find provenance data file in patient or pipeline directories"
  end

  test "inserts upload file for file type" do
    patient = build(:patient)
    pipeline = create(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:, status: :running, secret: SecureRandom.base58)
    dft = create(:data_file_type,
                  name: "TestDataFileType",
                  viewer: :text,
                  data_type: "test_data_file_type", pattern: "^test_data_file.*\.txt$")
    computation.step.required_file_types << dft
    pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "test_data_file1.txt")

    script = ScriptGenerator.new(
      computation,
      "{% stage_in #{dft.data_type} %}"
    ).call
    assert_includes script, computation_type_inputs_url(computation, secret: computation.secret,
      type: dft.data_type, script_name: script_name(computation), protocol: :https)
  end

  test "inserts upload file for filename" do
    patient = build(:patient)
    pipeline = create(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:, status: :running, secret: SecureRandom.base58)
    pipeline.inputs.attach(io: fixture_file_upload("id_rsa"),
      filename: "test_data_file1.txt")

    script = ScriptGenerator.new(
      computation,
      "{% stage_in_by_filename test_data_file1.txt %}"
    ).call
    assert_includes script, computation_filename_inputs_url(computation, secret: computation.secret,
      filename: "test_data_file1.txt", script_name: script_name(computation), protocol: :https)
  end

  test "inserts upload file for stage in all" do
    patient = build(:patient)
    pipeline = create(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:, status: :running, secret: SecureRandom.base58)
    pipeline.inputs.attach(io: fixture_file_upload("id_rsa"),
      filename: "test_data_file1.txt")

    script = ScriptGenerator.new(
      computation,
      "{% stage_in_all pipeline_inputs %}"
    ).call
    assert_includes script, computation_all_filenames_url(computation, secret: computation.secret, type: "pipeline_inputs", script_name: script_name(computation), protocol: :https)
    assert_includes script, computation_filename_inputs_url(computation, secret: computation.secret, filename: "$filename", type: "pipeline_inputs", script_name: script_name(computation), protocol: :https)
  end

  test "inserts download file curl" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out foo.txt %}").call

    assert_includes script, computation_outputs_url(computation, secret: computation.secret,
      script_name: script_name(computation), protocol: :https)
  end

  test "inserts download artifact curl" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_in_artifact $filename %}").call

    assert_includes script, "https://mee.lvh.me:3000/1059638630/computations/#{computation.id}/artifacts/#{computation.secret}/$filename"
  end

  test "inserts upload artifact curl new" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out_artifact filename} %}").call

    assert_includes script, computation_upload_artifact_url(computation, secret: computation.secret, script_name: script_name(computation), protocol: :https)
  end

  test "raises error for wrong" do
    computation = create(:computation, secret: SecureRandom.base58)
    errors = computation.errors
    ScriptGenerator.new(computation, "{% wrong_tag foo.txt %}", errors).call

    assert_includes errors[:script], "Liquid syntax error: Unknown tag 'wrong_tag'"
  end

  test "inserts repository sha to clone" do
    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                 "{{ revision }}").call

    assert_includes script, "rev"
  end

  test "inserts clone repo command" do
    organization = organizations("main")
    computation = build(:computation, revision: "rev")
    script = ScriptGenerator.new(computation, "{% clone_repo %}").call

    assert_includes script, "export SSH_DOWNLOAD_KEY=\"#{organization.git_config.download_key}"
    assert_includes script, "git clone git@#{organization.git_config.host}:#{computation.repository}"
    assert_match(/cd.*repo.*/, script)
    assert_includes script, "git reset --hard rev"
  end

  test "inserts license_for ansys script" do
    create(:license,
          name: "ansys",
          config: [License::Entry.new(key: "ansysli_servers",
                                      value: "ansys-servers"),
                    License::Entry.new(key: "ansyslmd_license_file",
                                      value: "ansys-license-file")
                    ])

    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                "{% license_for ansys %}").call

    assert_includes script, "export ANSYSLI_SERVERS=ansys-servers"
    assert_includes script, "export ANSYSLMD_LICENSE_FILE=ansys-license-file"
  end

  test "adds error when requested license is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% license_for ansys %}", errors).call

    assert_includes errors[:script], "cannot find requested ansys license"
  end


  test "inserts value_of tag-or-branch and allocation" do
    allocation_id = allocations("cpu").name
    step = steps("first")
    stub_repo_versions(step,
                      { branches: ["main"], tags: ["t1"] })

    computation = create(:computation,
                         step:,
                          parameter_values_attributes: {
                            "tag-or-branch" => { "version" => "main" },
                            "allocation" => { "value" => allocation_id }
                          })

    script = ScriptGenerator.new(computation,
      "{% value_of tag-or-branch %} {% value_of allocation %}").call

    assert_equal "main #{allocation_id}", script
  end

  test "inserts value_of for multi select" do
    step = steps("multi_select")
    stub_repo_versions(step,
                      { branches: ["main"], tags: ["t1"] })


    computation = create(:computation,
                         step:,
                          parameter_values_attributes: {
                            "tag-or-branch" => { "version" => "main" },
                            "multi" => { "value" => ["1", "2"] },
                            "allocation" => { "value" => allocations("cpu").name }
                          })

    script = ScriptGenerator.new(computation,
                                "{% value_of multi %}").call

    assert_includes script, '("1" "2")'
  end

  test "adds error when requested value is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% value_of tag-or-branch %}", errors).call

    assert_includes errors[:script], "parameter value tag-or-branch was not found"
  end

  test "inserts pipeline identifier" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ pipeline_identifier }}").call

    assert_equal "case-312-#{pipeline.iid}", script
  end

  test "inserts pipeline name" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ pipeline_name }}").call

    assert_equal "#{pipeline.name}", script
  end

  test "inserts step name" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ step_name }}").call

    assert_equal "#{computation.name}", script
  end

  test "inserts patient case_number" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ case_number }}").call

    assert_equal "Case 312", script
  end

  test "inserts user email" do
    user = users("user")
    pipeline = build(:pipeline, user:)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ email }}").call

    assert_equal script, user.email
  end

  test "inserts pipeline mode" do
    pipeline = build(:pipeline, mode: "automatic")
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ mode }}").call

    assert_equal "automatic", script
  end
end
