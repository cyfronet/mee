# frozen_string_literal: true

require "test_helper"

class Rimrock::UpdateTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  setup do
    @user = users("user")
  end

  test "do nothing when user does not have active jobs" do
    create(:computation, status: "finished", job_host: "host")

    # if it makes a request, it would throw an error of not having stubbed request
    Rimrock::Update.new(@user, "host").call
  end

  test "asks about jobs when user has active computations" do
    create(:computation, started_at: Time.current, status: "finished", job_host: "host")
    create(:computation, started_at: Time.current, status: "queued", job_id: "other_job", job_host: "otherhost")
    c1 = create(:computation, started_at: Time.current, status: "queued", job_id: "job1", job_host: "host")
    c2 = create(:computation, started_at: Time.current, status: "queued", job_id: "job2", job_host: "host")

    Plgrid::HpcClientMock.stub_check_status(["job1", "job2"],
     body: '[{"job_id": "job1", "status": "FINISHED"}, {"job_id": "job2", "status": "RUNNING"}]')

    Rimrock::Update.new(@user, "host").call
    c1.reload
    c2.reload

    assert_equal "finished", c1.status
    assert_equal "running", c2.status
  end

  test "logs when error updating computations" do
    pipeline = create(:pipeline, user: @user)
    create(:computation, status: "queued", job_id: "job1", job_host: "host", pipeline:)

    Plgrid::HpcClientMock.stub_check_status(["job1"], status: 500, body: "error details")

    Rails.logger.expects(:warn).
      with(I18n.t("rimrock.update.internal",
                  user: @user.name, details: "error details"))

    Rimrock::Update.new(@user, "host").call
  end

  test "triggers callback after computation is finished and queues delete duplicate job" do
    c1 = create(:computation, started_at: Time.current, status: "queued", job_id: "job1", job_host: "host")

    Plgrid::HpcClientMock.stub_check_status(["job1"], body: '[{"job_id": "job1", "status": "FINISHED"}]')

    Plgrid::HpcMock.stub_stdout(c1, "stdout log")
    Plgrid::HpcMock.stub_stderr(c1, "stderr log")

    callback_instance = mock
    callback_instance.expects(:call)
    callback = stub(new: callback_instance)

    assert_enqueued_jobs(1, only: Pipelines::RemoveDuplicateOutputsJob) do
      Rimrock::Update.new(@user, "host", on_finish_callback: callback).call
    end

    assert_equal "stdout log", c1.stdout_blob.download
    assert_equal "stderr log", c1.stderr_blob.download
  end

  test "update in baches" do
    c1 = create(:computation, started_at: Time.current, status: "queued", job_id: "job1", job_host: "host")
    c2 = create(:computation, started_at: Time.current, status: "queued", job_id: "job2", job_host: "host")

    stub_const(Rimrock::Update, :JOBS_BATCH_SIZE, 1) do
      Plgrid::HpcClientMock.stub_check_status(["job1"], body: '[{"job_id": "job1", "status": "RUNNING"}]')
      Plgrid::HpcClientMock.stub_check_status(["job2"], body: '[{"job_id": "job2", "status": "RUNNING"}]')

      Rimrock::Update.new(@user, "host", on_finish_callback: nil).call

      assert_equal "running", c1.reload.status
      assert_equal "running", c2.reload.status
    end
  end
end
