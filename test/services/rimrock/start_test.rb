# frozen_string_literal: true

require "test_helper"

class Rimrock::StartTest < ActiveSupport::TestCase
  test "starts computation" do
    computation = create(:computation)
    Plgrid::HpcClientMock.stub_submit(
      computation.script, computation.working_directory,
      body: '{"job_id":"id", "stdout_path":"out", ' \
      '"stderr_path":"err", "status":"QUEUED"}'
    )

    Rimrock::Start.new(computation).call

    assert_equal "out", computation.stdout_path
    assert_equal "err", computation.stderr_path
    assert_equal "queued", computation.status
  end

  test "fails to start computation" do
    computation = create(:computation, started_at: Time.current)
    Plgrid::HpcClientMock.stub_submit(
      computation.script, computation.working_directory, status: 422,
      body: '{"status":"error", "exit_code": -1, ' \
        '"standard_output":"stdout", "error_output":"stderr", ' \
        '"error_message": "error_msg"}'
    )

    Rimrock::Start.new(computation).call

    assert_equal "error", computation.status
    assert_equal(-1, computation.exit_code)
    assert_equal "stdout", computation.standard_output
    assert_equal "stderr", computation.error_output
    assert_equal "error_msg", computation.error_message
  end

  test "authorization fail" do
    computation = create(:computation, started_at: Time.current)
    Plgrid::HpcClientMock.stub_submit(
      computation.script, computation.working_directory, status: 403,
      body: '{"status":"error", "exit_code": -1, ' \
        '"standard_output":"stdout", "error_output":"stderr", ' \
        '"error_message": "error_msg"}'
    )

    Rimrock::Start.new(computation).call

    assert_equal "error", computation.status
    assert_equal "403 - Unauthorized", computation.error_message
  end

  test "cannot start already started computation" do
    computation = create(:computation, status: "running")
    Plgrid::HpcClientMock.any_instance.expects(:submit).never

    Rimrock::Start.new(computation).call
  end
end
