# frozen_string_literal: true

require "test_helper"

class ScriptGenerator::DataRepositoryTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include LinkHelper
  include DataRepositoryTestHelper

  test "adds error when external repository config is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    organizations("main").update(data_repository_config: nil)

    ScriptGenerator.new(computation, "{% dataverse_file_stage_out hello.txt dataset_pid %}", errors).call
    assert_includes errors[:script],
                    "This step requires external data repository configuration. Please ask organization's administrator for its support."
  end

  test "adds error when external repository token is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    memberships("user").update(data_repository_token: nil)

    ScriptGenerator.new(computation, "{% dataverse_file_stage_out hello.txt dataset_pid %}", errors).call
    assert_includes errors[:script],
                    "This step requires external data repository configuration. Data repository token is missing in your profile!"
  end

  test "adds error on bad data repository usage" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% zenodo_file_stage_in record_id filename %}", errors).call
    assert_includes errors[:script],
                    "This step is using liquid tags for external data repository that is not configured for this organization."
  end
end
