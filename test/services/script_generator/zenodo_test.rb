# frozen_string_literal: true

require "test_helper"

class ScriptGenerator::ZenodoTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include LinkHelper
  include DataRepositoryTestHelper

  test "inserts upload file to zenodo deposit curl" do
    computation, zenodo_url, token = data_repository_test_setup

    upload_script = ScriptGenerator.new(
      computation,
      "{% zenodo_file_stage_out hello.txt record_id %}"
    ).call

    url = "#{zenodo_url}/api/deposit/depositions/record_id/files"
    filename = "$(basename #{url})"

    assert_includes upload_script, "curl -w \"%{http_code}\" -o #{filename} -H \"Authorization: Bearer #{token}\" -X POST -F file=@hello.txt #{url}"
  end

  test "inserts download dataset from zenodo curl" do
    computation, zenodo_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% zenodo_dataset_stage_in record_id %}"
    ).call

    check_access_url = "#{zenodo_url}/api/records/record_id"
    filename = "record_id"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o /dev/null -H \"Authorization: Bearer #{token}\" #{check_access_url}"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/files-archive", "#{zenodo_url}/api/records/record_id/draft/files-archive"
    assert_includes download_script, "curl -L -w \"%{http_code}\" -o #{filename} -H \"Authorization: Bearer #{token}\" $URL"
  end

  test "inserts download dataset as dataset.zip from zenodo curl" do
    computation, zenodo_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% zenodo_dataset_stage_in record_id dataset.zip %}"
    ).call

    check_access_url = "#{zenodo_url}/api/records/record_id"
    filename = "dataset.zip"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o /dev/null -H \"Authorization: Bearer #{token}\" #{check_access_url}"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/files-archive"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/draft/files-archive"
    assert_includes download_script, "curl -L -w \"%{http_code}\" -o #{filename} -H \"Authorization: Bearer #{token}\" $URL"
  end

  test "inserts download file from zenodo curl" do
    computation, zenodo_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% zenodo_file_stage_in record_id filename %}"
    ).call

    check_access_url = "#{zenodo_url}/api/records/record_id"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o /dev/null -H \"Authorization: Bearer #{token}\" #{check_access_url}"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/files/filename/content"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/draft/files/filename/content"
    assert_includes download_script, "curl -L -o filename -J -w \"%{http_code}\" -H \"Authorization: Bearer #{token}\" $URL"
  end

  test "inserts download file as filename from zenodo curl" do
    computation, zenodo_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% zenodo_file_stage_in record_id filename target %}"
    ).call

    check_access_url = "#{zenodo_url}/api/records/record_id"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o /dev/null -H \"Authorization: Bearer #{token}\" #{check_access_url}"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/files/filename/content"
    assert_includes download_script, "#{zenodo_url}/api/records/record_id/draft/files/filename/content"
    assert_includes download_script, "curl -L -o target -J -w \"%{http_code}\" -H \"Authorization: Bearer #{token}\" $URL"
  end
end
