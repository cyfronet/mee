# frozen_string_literal: true

require "test_helper"

class ScriptGenerator::DataverseTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include LinkHelper
  include DataRepositoryTestHelper

  test "inserts upload file to dataverse dataset curl" do
    computation, dataverse_url, token = data_repository_test_setup

    upload_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_out hello.txt dataset_pid %}"
    ).call

    url = "#{dataverse_url}/api/datasets/:persistentId/add?persistentId=dataset_pid"
    filename = "$(basename #{url})"

    assert_includes upload_script, "curl -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" -X POST -F file=@hello.txt #{url}"
  end

  test "inserts upload file with metadata to dataverse dataset curl" do
    computation, dataverse_url, token = data_repository_test_setup

    upload_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_out hello.txt dataset_pid {\"directoryLabel\":\"dir1/subdir\"} %}"
    ).call

    url = "#{dataverse_url}/api/datasets/:persistentId/add?persistentId=dataset_pid"
    filename = "$(basename #{url})"

    assert_includes upload_script, "curl -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" -X POST -F file=@hello.txt -F jsonData='{\"directoryLabel\":\"dir1/subdir\"} ' #{url}"
  end

  test "inserts download dataset from dataverse curl" do
    computation, dataverse_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_dataset_stage_in dataset_pid %}"
    ).call

    url = "#{dataverse_url}/api/access/dataset/:persistentId/?persistentId=dataset_pid"
    filename = "$(basename #{url})"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download dataset as dataset.zip from dataverse curl" do
    computation, dataverse_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_dataset_stage_in dataset_pid dataset %}"
    ).call

    url = "#{dataverse_url}/api/access/dataset/:persistentId/?persistentId=dataset_pid"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o dataset -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download file from dataverse curl" do
    computation, dataverse_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_in file_pid %}"
    ).call

    url = "#{dataverse_url}/api/access/datafile/:persistentId/?persistentId=file_pid"

    assert_includes download_script, "curl -L -O -J -w \"%{http_code}\" -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download file as filename from dataverse curl" do
    computation, dataverse_url, token = data_repository_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_in file_pid filename %}"
    ).call

    url = "#{dataverse_url}/api/access/datafile/:persistentId/?persistentId=file_pid"

    assert_includes download_script, "curl -L -o filename -J -w \"%{http_code}\" -H \"X-Dataverse-key:#{token}\" #{url}"
  end
end
