# frozen_string_literal: true

require "test_helper"

class Step::ValidateRepositoryTest < ActiveSupport::TestCase
  include GitlabHelper

  test "creates persistent error when token is wrong" do
    step = steps("first")
    errors = { private_token: "invalid" }
    GitConfig::Gitlab.any_instance.stubs(:errors).returns(errors)

    assert_difference "step.persistent_errors.count" do
      assert_difference "step.organization.reload.steps_persistent_errors_count" do
        Step::ValidateRepository.new(step).call
      end
    end
  end

  test "creates persistent error when ssh key is wrong" do
    step = steps("first")
    GitRepository::GitlabClient.any_instance.stubs(:key_valid?).returns(false)

    assert_difference "step.persistent_errors.count" do
      assert_difference "step.reload.persistent_errors_count" do
        assert_difference "step.organization.reload.steps_persistent_errors_count" do
          Step::ValidateRepository.new(step).call
        end
      end
    end
  end
end
