# frozen_string_literal: true

require "test_helper"

class ComputationPolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "can start manual computation only when required inputs present" do
    pipeline = create(:pipeline, mode: :manual)
    computation = create(:computation,
      step: steps("second"), pipeline:,
      parameter_values: [
        build(:model_version_parameter_value, parameter: parameters("second_tag_or_branch"))
      ]
    )

    assert_no_permit(pundit_user, computation, :update)

    pipeline.inputs.attach(io: StringIO.new("image"), filename: "imaging_123.zip")
    assert_permit(pundit_user, computation, :update)
  end

  test "cannot see computation from other organization" do
    computation = create(:computation)
    assert_no_permit(other_organization_pundit_user, computation, :show)
  end
end
