# frozen_string_literal: true

require "test_helper"

class DataFileTypePolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "User can manage own organization data file types" do
    dft = data_file_types("image")

    assert_permit(admin_pundit_user, dft, :edit)
    assert_permit(admin_pundit_user, dft, :create)
    assert_permit(admin_pundit_user, dft, :update)
    assert_permit(admin_pundit_user, dft, :destroy)

    assert_permit(pundit_user, dft, :edit)
    assert_permit(pundit_user, dft, :create)
    assert_permit(pundit_user, dft, :update)
    assert_permit(pundit_user, dft, :destroy)
  end

  test "User cannot manage other user data file types" do
    dft = create(:data_file_type, user: users("admin"))

    assert_no_permit(pundit_user, dft, :edit)
    assert_no_permit(pundit_user, dft, :update)
    assert_no_permit(pundit_user, dft, :destroy)
  end

  test "Admin cannot manage other organization data file types" do
    dft = create(:data_file_type, organization: organizations("other"), user: users("other"))

    assert_no_permit(admin_pundit_user, dft, :edit)
    assert_no_permit(admin_pundit_user, dft, :create)
    assert_no_permit(admin_pundit_user, dft, :update)
    assert_no_permit(admin_pundit_user, dft, :destroy)
  end

  test "Admin can see only organization data file types" do
    create(:data_file_type, organization: organizations("other"))

    assert_equal data_file_types("image", "segmentation").sort,
                 DataFileTypePolicy::Scope.new(admin_pundit_user, DataFileType).resolve
  end
end
