# frozen_string_literal: true

require "test_helper"

class CampaignsTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "#index with no campaigns shows empty text" do
    get campaigns_path

    assert_includes response.body, I18n.t("campaigns.empty.title")
  end

  test "#index Organization member doesn't see campaigns from other organization" do
    campaign = create(:campaign, organization: organizations("main"))
    other_campaign = create(:campaign, organization: organizations("other"))

    get campaigns_path

    assert_includes response.body, campaign.name
    assert_includes response.body, campaign_path(campaign)

    assert_not_includes response.body, campaign_path(other_campaign)
  end

  test "#destroy Organization admin can remove a campaign" do
    campaign = create(:campaign)

    assert_changes "Campaign.count", -1 do
      delete campaign_path(campaign)
    end
  end

  test "#destroy Organization member cannot remove a campaign" do
    login_as(users("user"))
    campaign = create(:campaign)

    assert_no_changes "Campaign.count" do
      delete campaign_path(campaign)
    end
  end
end
