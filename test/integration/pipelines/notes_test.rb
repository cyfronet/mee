# frozen_string_literal: true

require "test_helper"

class Pipelines::NotesTest < ActionDispatch::IntegrationTest
  def setup
    @organization = organizations("main")
    in_organization! @organization
    @pipeline = create(:pipeline, runnable: @organization,
                user: users("admin"), notes: "some notes")
  end

  test "owner sees and updates notes" do
    login_as users("admin")
    get pipeline_path(@pipeline)

    assert_includes response.body, "some notes"

    patch pipeline_path(@pipeline), params: {
        pipeline: {
          notes: "updated content"
        }
    }

    get pipeline_path(@pipeline)
    assert_includes response.body, "updated content"

    assert_equal "updated content", @pipeline.reload.notes.to_plain_text
  end

  test "non-owner doesn't see notes" do
    login_as users("user")

    get pipeline_path(@pipeline)
    assert_not_includes response.body, "some notes"
  end
end
