# frozen_string_literal: true

require "test_helper"

class Admin::AllocationTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "are limited to organization allocations" do
    other_org_allocation = create(:allocation, organization: organizations("other"))

    get admin_allocations_path

    assert_includes response.body, allocations("cpu").name
    assert_includes response.body, allocations("gpu").name
    assert_not_includes response.body, other_org_allocation.name
  end

  test "admin can create new allocation" do
    assert_changes "Allocation.count", 1 do
      post admin_allocations_path, params: {
        allocation: {
          name: "test-allocation",
          starts_at: "02/02/2010",
          ends_at: "02/02/2020",
          resource_type_ids: [resource_types("cpu").id],
          site_ids: [sites("ares").id]
        }
      }
    end
  end

  test "admin can edit existing allocation" do
    allocation = allocations("cpu")

    patch admin_allocation_path(allocation), params: {
      allocation: {
        name: "updated-name",
      }
    }

    assert_equal "updated-name", allocation.reload.name
  end
end
