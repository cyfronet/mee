# frozen_string_literal: true

require "test_helper"

class Organization::AddPipelineTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  def setup
    @organization = organizations("main")
    in_organization! @organization
    login_as users("user")
  end

  test "adds manual pipeline" do
    assert_difference "Pipeline.count" do
      assert_no_enqueued_jobs only: Pipelines::StartRunnableJob do
        post pipelines_path,
          params: { pipeline: {
            name: "my new manual pipeline",
            mode: "manual",
            flow_id: flows(:first).id
          } }
      end
    end

    assert_response :redirect

    pipeline = Pipeline.last
    assert_equal @organization, pipeline.runnable
    assert_equal "my new manual pipeline", pipeline.name
    assert_predicate pipeline, :manual?
  end

  test "adds automatic pipeline" do
    assert_difference "Pipeline.count" do
      assert_enqueued_jobs 1, only: Pipelines::StartRunnableJob do
        post pipelines_path,
          params: { pipeline: {
            name: "my new automatic pipeline",
            mode: "automatic",
            flow_id: flows(:first).id
          } }
      end
    end

    pipeline = Pipeline.last
    assert_equal @organization, pipeline.runnable
    assert_equal "my new automatic pipeline", pipeline.name
    assert_predicate pipeline, :automatic?
  end

  test "is unable to create pipeline without name" do
    post pipelines_path,
      params: { pipeline: {
        mode: "automatic",
        flow_id: flows(:first).id
      } }

    assert_response :unprocessable_entity
  end
end
