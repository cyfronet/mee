# frozen_string_literal: true

require "test_helper"

class Computations::AbortComputationTest < ActionDispatch::IntegrationTest
  include GitlabHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @step = create(:step)
    create(:flow_step, step: @step)
    pipeline = create(:pipeline, mode: :manual)
    @computation = create(:computation, step: @step, status: :script_generated, pipeline:, started_at: Time.current)
  end

  test "user can abort running computation" do
    Plgrid::HpcClientMock.stub_abort(@computation.job_id)
    post computation_abort_path(@computation)
    assert_predicate @computation.reload, :aborted?
    assert_response :found
    follow_redirect!
    assert_includes(response.body, "#{@computation.name} was aborted by the user")
  end
end
