# frozen_string_literal: true

require "test_helper"

class Computations::ArtifactsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = organizations("main")
    in_organization! @organization
    @computation = create(:computation, secret: SecureRandom.base58, status: :running)
  end

  test "show returns 404 if file is not found" do
    get computation_artifacts_path(@computation, secret: @computation.secret, name: "does not exist")
    assert_response :not_found
    assert_match "No artifact with given name found", @response.body
  end

  test "show serves file from s3" do
    @organization.artifacts.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    get computation_artifacts_path(@computation, secret: @computation.secret, name: "artifact.txt")
    assert_redirected_to @organization.artifacts_blobs.first
  end

  test "create returns 400 if no file is provided" do
    post computation_upload_artifact_path(@computation, secret: @computation.secret)

    assert_response :bad_request
    assert_match "No file provided", @response.body
  end

  test "create returns 201 and creates file" do
    assert_difference "@organization.artifacts.count" do
      post computation_upload_artifact_path(@computation, secret: @computation.secret), params: { file: fixture_file_upload("file.zip") }
    end
    assert_response :created
    assert_match "Upload successful", @response.body
  end

  test "create returns 500 for upload failure" do
    Artifact.stubs(:upload).returns(false)

    assert_no_difference "@organization.artifacts.count" do
      post computation_upload_artifact_path(@computation, secret: @computation.secret), params: { file: fixture_file_upload("file.zip") }
    end
    assert_match "Upload failed", @response.body
    assert_response :internal_server_error
  end
end
