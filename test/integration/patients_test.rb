# frozen_string_literal: true

require "test_helper"

class PatientsTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  test "#index shows notification when no patients are available" do
    get patients_path

    assert_includes response.body, I18n.t("patients.empty.title")
    assert_includes response.body, I18n.t("patients.empty.new")
  end

  test "#show shows pipelines list" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient, name: "p1")

    get patient_path(patient)

    assert_includes response.body, pipeline.flow.name
    assert_includes response.body, pipeline.user.name
    assert_includes response.body, pipeline_path(pipeline)
  end

  test "#show edit pipeline button for owner" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient, name: "p1")

    get patient_path(patient)

    assert_includes response.body, edit_pipeline_path(pipeline)
  end

  test "#show no edit pipeline button for non-owner" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient, name: "p1", user: users("admin"))

    get patient_path(patient)

    assert_not_includes response.body, edit_pipeline_path(pipeline)
  end

  test "doesn't show compare button when one pipeline exists" do
    patient = create(:patient)
    create(:pipeline, runnable: patient, name: "p1")

    get patient_path(patient)

    assert_not_includes response.body, I18n.t("runnables.pipelines.tab_compare.compare")
  end

  test "shows compare button when more than one pipeline exists" do
    patient = create(:patient)
    create(:pipeline, runnable: patient)
    create(:pipeline, runnable: patient)

    get patient_path(patient)

    assert_includes response.body, I18n.t("runnables.pipelines.tab_compare.compare")
  end
end
