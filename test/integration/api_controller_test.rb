# frozen_string_literal: true

require "test_helper"

class FakesController < Computations::ApiController
  include ActionDispatch::Assertions::RoutingAssertions
  def show
    render nothing: true
  end
end

class FakeControllerTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    @computation = create(:computation, started_at: Time.current, secret: SecureRandom.base58, status: :running)
    Rails.application.routes.draw do
      resources :computations do
        resource :fake, only: :show, path: "fake/:secret"
      end
    end
  end

  test "show returns 401 when secret is wrong" do
    get computation_fake_path(@computation, secret: "wrong secret")
    assert_response :unauthorized
  end

  test "show returns 401 when secret is outdated" do
    secret = @computation.secret
    @computation.update(secret: nil)
    get computation_fake_path(@computation, secret:)
    assert_response :unauthorized
    assert_match "Unauthorized", @response.body
  end

  test "show returns 401 when computation is finished" do
    @computation = create(:computation, started_at: Time.current, secret: SecureRandom.base58, status: :finished)
    get computation_fake_path(@computation, secret: @computation.secret)
    assert_response :unauthorized
    assert_match "Unauthorized", @response.body
  end

  test "show returns 404 when no organization is found" do
    in_root!
    get computation_fake_path(@computation, secret: @computation.secret)
    assert_response :not_found
    assert_match "Computation or organization not found", @response.body
  end

  test "show returns 404 when no computation is found" do
    get computation_fake_path("wrong_id", secret: SecureRandom.base58)
    assert_response :not_found
    assert_match "Computation or organization not found", @response.body
  end

  test "show returns 200 when happy path" do
    get computation_fake_path(@computation, secret: @computation.secret)
    assert_response :success
  end

  def teardown
    Rails.application.reload_routes!
  end
end
