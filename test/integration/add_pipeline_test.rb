# frozen_string_literal: true

require "test_helper"

class Patients::AddPipelineTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @patient = create(:patient)
  end

  test "adds manual pipeline" do
    assert_difference "Pipeline.count" do
      assert_no_enqueued_jobs only: Pipelines::StartRunnableJob do
        post pipelines_path,
          params: {
            patient_id: @patient.slug,
            pipeline: {
            name: "my new manual pipeline",
            mode: "manual",
            flow_id: flows(:first).id
          } }
      end
    end

    assert_response :redirect
    follow_redirect!
    pipeline = Pipeline.last
    assert_redirected_to computation_path(pipeline.computations.first)
    assert_equal @patient, pipeline.runnable
    assert_equal "my new manual pipeline", pipeline.name
    assert_predicate pipeline, :manual?
  end

  test "adds automatic pipeline" do
    assert_difference "Pipeline.count" do
      assert_enqueued_jobs 1, only: Pipelines::StartRunnableJob do
        post pipelines_path,
          params: {
            patient_id: @patient.slug,
            pipeline: {
            name: "my new automatic pipeline",
            mode: "automatic",
            flow_id: flows(:first).id
          } }
      end
    end

    assert_response :redirect
    follow_redirect!
    pipeline = Pipeline.last
    assert_redirected_to computation_path(pipeline.computations.first)
    assert_equal @patient, pipeline.runnable
    assert_equal "my new automatic pipeline", pipeline.name
    assert_predicate pipeline, :automatic?
  end

  test "is unable to create pipeline without name" do
    post pipelines_path,
      params: {
        patient_id: @patient.slug,
        pipeline: {
        mode: "automatic",
        flow_id: flows(:first).id
      } }

    assert_response :unprocessable_entity
  end
end
