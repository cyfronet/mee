# frozen_string_literal: true

require "test_helper"

class Organizations::ArtifactsControllerTest < ActionDispatch::IntegrationTest
  include ActionCable::TestHelper
  def setup
    @organization = organizations("main")
    in_organization! @organization
    login_as users("admin")
    @pipeline = create(:pipeline)
  end

  test "new returns 200" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    get new_artifact_from_file_path(file_id: @pipeline.inputs.first.id)
    assert_response :ok
  end

  test "create returns 400 if no name is provided" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    post create_artifact_from_file_path(
      file_id: @pipeline.inputs.first.id,
      artifact: {
        filename: nil,
      }
    ), as: :turbo_stream

    assert_response :unprocessable_entity
  end

  test "create returns 200 as turbo response and creates file" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    assert_difference "@organization.artifacts.count" do
      post create_artifact_from_file_path(
        file_id: @pipeline.inputs.first.id,
        artifact: {
          filename: "test",
        }
      ), as: :turbo_stream
    end

    assert_turbo_stream status: :created, action: :prepend, target: "flash"
    assert_turbo_stream status: :created, action: :update, target: dom_id(@pipeline.inputs.first, "actions")
  end

  test "create returns 422 for save failure" do
    Artifact.any_instance.stubs(:save).returns(false)

    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    assert_no_difference "@organization.artifacts.count" do
      post create_artifact_from_file_path(
        file_id: @pipeline.inputs.first.id,
        artifact: {
          filename: "test",
        }
      ), as: :turbo_stream
    end
    assert_response :unprocessable_entity
  end

  test "update returns 200 for update failure" do
    Artifact.any_instance.stubs(:update).returns(false)

    @organization.artifacts.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    assert_no_changes "@organization.artifacts.first.filename" do
      patch artifact_path(
        id: @organization.artifacts.first.id,
        artifact: {
          filename: "test",
        }
      ), as: :turbo_stream
    end
    assert_response :unprocessable_entity
  end

  test "update returns 422 for update failure" do
    @organization.artifacts.attach(io: fixture_file_upload("id_rsa"), filename: "artifact.txt")

    assert_no_changes "@organization.artifacts.first.filename" do
      patch artifact_path(
        id: @organization.artifacts.first.id,
        artifact: {
          filename: "test",
        }
      ), as: :turbo_stream
    end

    assert_response :ok
    assert_turbo_stream action: :replace, target: @organization.artifacts.first
    assert_turbo_stream action: :prepend, target: "flash"
  end
end
