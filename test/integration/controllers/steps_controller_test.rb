# frozen_string_literal: true

require "test_helper"

class StepsControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "I can remove step" do
    step = create(:step)

    assert_difference "Step.count", -1 do
      delete step_path(step)
      assert_turbo_stream_broadcasts step
    end

    assert_redirected_to steps_path
  end

  test "I cannot remove step from other organization" do
    other_step = create(:step, organization: organizations("other"))

    assert_no_difference "Step.count" do
      delete step_path(other_step)
    end
  end

  test "Uses old download key if not present" do
    step = create(:step, git_config: build(:gitlab_git_config))
    patch step_path(step), params: { "step" => { "name" => "name", "repository" => "cyfronet/repo", "file" => "file_name",
                                             "git_config_attributes" =>
                                               { "git_type" => "gitlab", "host" => "gitlab.com", "private_token" => "token1" },
                                             "resource_type_id" => "#{ResourceType.first.id}", "site_id" => "#{Site.first.id}",
                                             "required_file_type_ids" => [], "parameters_attributes" => {},  },
                                    "commit" => "Save changes", "id" => "#{step.slug}" }

    assert_redirected_to step_path(step)
    assert_equal Step.find_by(id: step.id).git_config.download_key, step.git_config.download_key
  end
end
