# frozen_string_literal: true

require "test_helper"

class DataRepositoryControllerTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper
  test "removes data repository tokens when config is updated" do
    in_organization! organizations("main")
    login_as users("admin")

    post admin_organization_data_repository_config_url,
          params: {
            url: "https://changed_url"
          }

    assert_nil memberships("admin").data_repository_token
  end

  test "hides access token setup if data repository config is disabled" do
    in_organization! organizations("other")
    login_as users("stranger")

    get profile_path
    assert_no_match "Personal access token", @response.body
  end
end
