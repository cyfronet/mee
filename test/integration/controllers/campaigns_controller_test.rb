# frozen_string_literal: true

require "test_helper"

class CampaignsControllerTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  def setup
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "POST /campaigns creates a campaign" do
    cohort = create :cohort
    flow = create :flow

    perform_enqueued_jobs do
      post campaigns_path, params: { campaign: { name: "Name", "cohort_id": "#{cohort.id}" },
                                     pipeline: { flow_id: flow.id, parameters_values: { "#{flow.steps.first.slug}": {
                                       "tag-or-branch": { "version" => "master" }, "allocation": { "value" => allocations("cpu").name }
                                     } } }, "commit": "Submit"
      }

      assert_redirected_to campaign_path(Campaign.first)
      assert_equal Campaign.first.cohort, cohort
      assert_equal 1, cohort.campaigns.first.pipelines.count
    end
  end

  test "GET /campaigns/:campaign_id returns a valid campaign page" do
    campaign = create(:campaign, status: :created, pipelines: [create(:pipeline, status: :waiting)])
    get campaign_path(campaign)

    assert_select "h2", /#{campaign.name}/ do # breadcrumb navigation
      assert_select "a > i:match('class', ?)", /fa-server/
    end

    assert_select "a", { count: 1, text: I18n.t("cohorts.header.name", name: campaign.cohort.name) }
    assert_select "div", I18n.t("campaigns.computation_progress")
    assert_select "tr:match('id', ?)", /patient_\d+/ # patient table
    assert_select "div:match('id', ?) > form > button", "run_button", { count: 1, text: "Run campaign" }
    assert_select "h6", { count: 1, text: I18n.t("campaigns.view.output_files.title") }
  end
end
