# frozen_string_literal: true

require "test_helper"

class DataFileTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("user")
  end

  test "should destroy data file type from owned organization" do
    assert_difference "DataFileType.count", -1 do
      delete data_file_type_url(data_file_types("image"))
    end

    assert_redirected_to data_file_types_url
  end

  test "cannot remove data file type from other organization" do
    other_dft = create(:data_file_type, organization: organizations("other"), user: users("other"))

    assert_no_difference "DataFileType.count" do
      delete data_file_type_url(other_dft)
    end

    assert_redirected_to root_url
  end

  test "cannot remove data file type belonging to another user" do
    other_dft = create(:data_file_type, user: users("admin"))

    assert_no_difference "DataFileType.count" do
      delete data_file_type_url(other_dft)
    end
  end
end
