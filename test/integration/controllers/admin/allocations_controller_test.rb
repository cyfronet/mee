# frozen_string_literal: true

require "test_helper"

class Admin::AllocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "I can remove allocation" do
    assert_difference "Allocation.count", -1 do
      delete admin_allocation_path(allocations("cpu"))
    end

    assert_redirected_to admin_allocations_path
  end

  test "I cannot remove allocation from other organization" do
    other_allocation = create(:allocation, organization: organizations("other"))

    assert_no_difference "Allocation.count" do
      delete admin_allocation_path(other_allocation)
    end
  end
end
