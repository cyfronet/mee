# frozen_string_literal: true

require "test_helper"

class Computations::InputsComputationTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    @computation = create(:computation, secret: SecureRandom.base58, status: :running)
    @pipeline = @computation.pipeline
    @dft = data_file_types("image")
  end

  test "show_by_type returns 404 if file is not found" do
    get computation_type_inputs_path(@computation, secret: @computation.secret, type: "other_type")
    assert_response :not_found
    assert_match "No file found", @response.body
  end

  test "show_by_type serves file from s3 by type" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    get computation_type_inputs_path(@computation, secret: @computation.secret, type: @dft.data_type)
    assert_redirected_to @pipeline.inputs_blobs.first
  end

  test "show_by_filename returns 404 if file is not found" do
    get computation_filename_inputs_path(@computation, secret: @computation.secret, filename: "other_filename")
    assert_response :not_found
    assert_match "No file found", @response.body
  end

  test "show_by_filename serves file from s3 by type" do
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "input")
    get computation_filename_inputs_path(@computation, secret: @computation.secret, filename: "input")
    assert_redirected_to @pipeline.inputs_blobs.first
  end
end
