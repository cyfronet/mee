# frozen_string_literal: true

require "test_helper"

class Computations::RunComputationTest < ActionDispatch::IntegrationTest
  include GitlabHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @step = create(:step)
    create(:flow_step, step: @step)
    @computation = create(:computation, step: @step)
  end

  test "user can set computation tag_or_branch for automatic and start runnable computations" do
    params = {
      computation: {
        parameter_values_attributes: {
          "tag-or-branch": { version: "t1" },
          allocation: { value: allocations("cpu").name },
        }
      }
    }
    stub_repo_versions(@step, { branches: ["master"], tags: ["t1"] })
    Computation.any_instance.expects(:run_now).never

    assert_enqueued_jobs 1, only: Pipelines::StartRunnableJob do
      put computation_path(@computation, params:)
    end
    assert_response :found
    assert_equal "t1", @computation.reload.tag_or_branch
    follow_redirect!
    assert_includes(response.body, "Computation was updated successfully")
    assert_includes(response.body, "<td>Model version</td>\n<td>t1</td>")
  end
end
