# frozen_string_literal: true

require "test_helper"

class RootDomainTest < ActionDispatch::IntegrationTest
  setup do
    in_root!
  end

  test "returns list of registered organizations" do
    get root_path

    assert_includes response.body, organizations("main").name
    assert_includes response.body, organizations("other").name
  end

  test "returns to root domain, when organization is not found" do
    get root_url(subdomain: "organization-not-found")
    follow_redirect!

    assert_includes response.body, "Domain organizations"
  end

  test "allows to logout" do
    login_as(users("user"))
    get root_path

    assert_includes response.body, logout_path
  end

  test "admin can create organization" do
    login_as(users("admin"))
    get root_path

    assert_includes response.body, "New organization"
    assert_includes response.body, new_organization_path
  end

  test "regular user cannot create organization" do
    login_as(users("user"))
    get root_path

    assert_not_includes response.body, "New Organization"
    assert_not_includes response.body, new_organization_path
  end
end
