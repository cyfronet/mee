# frozen_string_literal: true

require "test_helper"

class StepsTest < ActionDispatch::IntegrationTest
  def setup
    @organization = organizations("main")
    in_organization! @organization
    login_as users("admin")
  end


  test "Organization member can see organization steps" do
    other_org_step = create(:step, organization: organizations("other"))

    get steps_path

    assert_includes response.body, steps("first").name
    assert_includes response.body, step_path(steps("first"))
    assert_includes response.body, steps("second").name
    assert_includes response.body, step_path(steps("second"))
    assert_not_includes response.body, other_org_step.name
  end

  test "Should show step configuration errors" do
    step = steps("first")
    step.persistent_errors.create(key: "repository", message: "test invalid")

    get edit_step_path(step)

    assert_includes response.body, "test invalid"
  end

  test "Can switch to default organization git config" do
    # FIXME: check how this can be moved to global mock
    GitRepository::GitlabClient.any_instance.stubs(:key_valid?).returns(false)

    step = steps("second")

    patch step_path(step), params: {
      step: {
        git_config_attributes: nil
      }
    }

    assert_nil step.reload.git_config
  end

  test "show displays step details" do
    step = steps("first")
    step.update(description: "some description")
    get step_path(step)

    assert_includes response.body, step.slug
    assert_includes response.body, step.parameters.first.name
    assert_includes response.body, step.parameters.second.name
    assert_includes response.body, step.description.to_s
  end
end
