# frozen_string_literal: true

require "test_helper"

class CreateOrganizationIntegrationTest < ActionDispatch::IntegrationTest
  setup do
    in_root!
  end

  test "regular user cannot create organization" do
    login_as(users("user"))
    get root_path
    assert_no_match "New organization", @response.body

    get new_organization_path
    assert_redirected_to root_path
  end
end
