# frozen_string_literal: true

require "test_helper"

class QuotaExceededTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    login_as users("user")
    Organization::StorageQuota.any_instance.stubs(:exceeded?).returns(true)
  end

  test "doesn't show run pipeline button" do
    pipeline = create(:pipeline)
    create(:computation, pipeline:)
    get pipeline_path(pipeline)

    assert_no_match "Run pipeline", @response.body
  end

  test "doesn't show artifact upload" do
    get artifacts_path
    assert_no_match "dropzone", @response.body
  end

  test "doesn't show pipeline input upload" do
    pipeline = create(:pipeline)
    get pipeline_path(pipeline)

    assert_no_match "dropzone", @response.body
  end

  test "doesn't show patient input upload" do
    patient = create(:patient)
    get patient_path(patient)
    assert_no_match "dropzone", @response.body
  end
end
