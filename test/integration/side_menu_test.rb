# frozen_string_literal: true

require "test_helper"

class SideMenuTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    login_as users("user")
    get root_path
    follow_redirect!
  end

  test "link to pipelines index" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.research.pipelines")
    assert_includes response.body, pipelines_path
  end

  test "link to patients index" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.research.patients")
    assert_includes response.body, patients_path
  end

  test "link to artifacts" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.research.artifacts")
    assert_includes response.body, artifacts_path
  end

  test "link to cohorts" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.research.cohorts")
    assert_includes response.body, cohorts_path
  end

  test "link to flows" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.pipelines_blueprints.flows")
    assert_includes response.body, flows_path
  end

  test "link to steps" do
    assert_includes response.body, I18n.t("layouts.organization.side_menu.pipelines_blueprints.steps")
    assert_includes response.body, steps_path
  end

  test "link to help pipeline steps" do
    assert_includes response.body, I18n.t("help.manual.step.title")
    assert_includes response.body, help_page_path(category: :manual, file: :step)
  end

  test "storage usage progress bar" do
    Organization::StorageQuota.any_instance.stubs(:exceeded?).returns(true)
    get root_path
    follow_redirect!

    assert_includes response.body, I18n.t("layouts.organization.side_menu.storage_usage.title")
    assert_includes response.body, I18n.t("layouts.organization.side_menu.storage_usage.quota_exceeded")
  end
end
