# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class OrganizationsJobTest < ActiveJob::TestCase
  test "invokes #validate_later" do
    Organization.any_instance.expects(:validate_later).twice # main and other
    OrganizationsJob.perform_now
  end

  test "does not invoke #validate_later for discarded organization" do
    organizations(:main).archive!
    Organization.any_instance.expects(:validate_later).once # only other
    OrganizationsJob.perform_now
  end
end
