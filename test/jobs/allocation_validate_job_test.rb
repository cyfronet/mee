# frozen_string_literal: true

require "test_helper"

class AllocationValidateJobTest < ActiveJob::TestCase
  include ActionMailer::TestHelper

  def setup
    @org = organizations("other")
  end

  test "do nothing when allocation ends not soon" do
    create(:allocation, ends_at: Time.now + 1.year, organization: @org)

    assert_no_emails do
      Organization::ValidateAllocationJob.perform_now(@org)
    end
  end

  test "notify when allocation ends soon and there is no continuation" do
    create(:allocation, ends_at: Time.now + 1.day, organization: @org)

    assert_enqueued_emails 1 do
      Organization::ValidateAllocationJob.perform_now(@org)
    end
  end

  test "do nothing when allocation ends soon, but there is a continuation" do
    create(:allocation, starts_at: Time.now - 1.day, ends_at: Time.now + 1.day, organization: @org)
    create(:allocation, starts_at: Time.now + 1.day, ends_at: Time.now + 1.year, organization: @org)

    assert_no_emails do
      Organization::ValidateAllocationJob.perform_now(@org)
    end
  end
end
