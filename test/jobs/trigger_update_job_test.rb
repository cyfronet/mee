# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class TriggerUpdateJobTest < ActiveJob::TestCase
  test "triggers update for all users with active jobs" do
    p1 = create(:pipeline, user: create(:user))
    p2 = create(:pipeline, user: create(:user))
    create(:computation, status: "running", pipeline: p1)
    create(:computation, status: "finished", pipeline: p2)

    mock = Minitest::Mock.new
    mock.expect(:call, true, [p1.user])

    Computations::UpdateJob.stub(:perform_later, mock) do
      TriggerUpdateJob.perform_now
    end
  end
end
