# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class Computations::StartJobTest < ActiveJob::TestCase
  test "triggers user computations update" do
    computation = create(:computation, started_at: Time.current)
    updated_at = computation.updated_at

    start = Minitest::Mock.new
    start.expect(:call, true)

    Rimrock::Start.stub(:new, start) do
      Computations::StartJob.perform_now(computation)
    end

    assert_not_equal updated_at, computation.updated_at
  end

  # todo refactor this test with pipeline broadcaster
  test "triggers computation update after error" do
    computation = create(:computation, started_at: Time.current)
    updated_at = computation.updated_at

    start = Object.new
    def start.call = raise

    Rimrock::Start.stub(:new, start) do
      Computations::StartJob.perform_now(computation)
    end

    assert_not_equal updated_at, computation.updated_at
  end
end
