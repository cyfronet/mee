# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class Computations::SiteUpdateJobTest < ActiveJob::TestCase
  test "triggers user computations update" do
    update = Minitest::Mock.new
    update.expect(:call, true)

    mock = Minitest::Mock.new
    mock.expect(:call, update, [users("user"), "host"],
                on_finish_callback: PipelineUpdater)

    Rimrock::Update.stub(:new, mock) do
      Computations::SiteUpdateJob.perform_now(users("user"), "host")
    end
  end
end
