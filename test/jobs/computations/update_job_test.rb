# frozen_string_literal: true

require "test_helper"

class Computations::UpdateJobTest < ActiveJob::TestCase
  test "triggers site update jobs for hosts with active user jobs" do
    other_user_pipeline = create(:pipeline, user: users("other"))
    create(:computation, status: "queued", pipeline: other_user_pipeline, job_host: "otherhost")
    create(:computation, status: "finished", job_host: "otherhost")
    create(:computation, status: "queued", job_host: "host1")
    create(:computation, status: "queued", job_host: "host2")

    assert_enqueued_jobs 2 do
      assert_enqueued_with(job: Computations::SiteUpdateJob, args: [users("user"), "host1"]) do
        assert_enqueued_with(job: Computations::SiteUpdateJob, args: [users("user"), "host2"]) do
          Computations::UpdateJob.perform_now(users("user"))
        end
      end
    end
  end
end
