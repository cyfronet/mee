# frozen_string_literal: true

require "test_helper"

class CampaignTest < ActiveSupport::TestCase
  test "deleting pipeline updates campaign count" do
    campaign = create(:campaign, pipelines: [create(:pipeline)])
    assert_changes "campaign.desired_pipelines" do
      campaign.pipelines.first.destroy
    end
  end

  test "deleting patient updates campaign count" do
    pipeline = create(:pipeline, runnable: create(:patient))
    campaign = create(:campaign, pipelines: [pipeline]) # rubocop:disable Lint/UselessAssignment
    assert_changes "campaign.reload.desired_pipelines" do
      pipeline.runnable.destroy
    end
  end
  test "calls StatusCalculator when asked for status" do
    Campaign::StatusCalculator.any_instance.expects(:calculate)
    campaign = create(:campaign)
    campaign.update_status!
  end
end
