# frozen_string_literal: true

require "test_helper"

class AllocationTypingTest < ActiveSupport::TestCase
  setup do
    @duplicated = allocations("cpu").allocation_typings.first.dup
  end

  test "prevents duplicated ownership records through validation" do
    assert_not @duplicated.valid?
    assert_includes @duplicated.errors[:resource_type], "has already been taken"
  end

  test "prevents duplicated ownership records through DB index" do
    assert_raise ActiveRecord::RecordNotUnique do
      @duplicated.save(validate: false)
    end
  end
end
