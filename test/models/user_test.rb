# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "returns users with submitted Rimrock computations" do
    u1, u2, u3 = create_list(:user, 3)

    p1 = create(:pipeline, user: u1)
    p2 = create(:pipeline, user: u2)
    p3 = create(:pipeline, user: u3)
    create(:computation, status: :script_generated, pipeline: p1)
    create(:computation, status: :finished, pipeline: p1)
    create(:computation, status: :queued, pipeline: p2)
    create(:computation, status: :running, pipeline: p3)

    assert_equal [u2, u3].sort,
                 User.with_submitted_computations.sort
  end
end
