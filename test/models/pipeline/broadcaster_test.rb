# frozen_string_literal: true

require "test_helper"

class Pipeline::BroadcasterTest < ActiveSupport::TestCase
  include ActionCable::TestHelper

  test "broadcasts status to campaign after status update" do
    pipeline = create(:pipeline, status: :running, campaign: create(:campaign))

    assert_broadcasts(pipeline.to_gid_param, 1) do
      pipeline.success!
    end
  end

  test "doesn't broadcast status without campaign" do
    pipeline = create(:pipeline, status: :running)

    assert_no_broadcasts(pipeline.to_gid_param) do
      pipeline.success!
    end
  end

  test "doesn't broadcast status for other updates" do
    pipeline = create(:pipeline, status: :running, campaign: create(:campaign))

    assert_no_broadcasts(pipeline.to_gid_param) do
      pipeline.update(name: "new_name")
    end
  end
end
