# frozen_string_literal: true

require "test_helper"

class Pipeline::DataFilesTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include ActiveJob::TestHelper

  def setup
    @patient = create(:patient)
    @pipeline = create(:pipeline, runnable: @patient)
    @image_type = data_file_types("image")
  end

  test "nil when data file is not found in pipeline and patient" do
    assert_nil @pipeline.pick_file_by_type(@image_type.data_type)
  end

  test "#pick_file_by_type takes patient input file when no other options" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    assert_equal @patient.inputs_blobs.first, @pipeline.pick_file_by_type(@image_type.data_type)
  end

  test "#pick_file_by_type preffer pipeline input to the patient input" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_2.zip")

    assert_equal @pipeline.inputs_blobs.first, @pipeline.pick_file_by_type(@image_type.data_type),
      "Pipeline input should be picked before patient input"
  end

  test "#pick_file_by_type preffer pipeline output to the pipeline input and patient input" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_2.zip")
    @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_3.zip")

    assert_equal @pipeline.outputs_blobs.first, @pipeline.pick_file_by_type(@image_type.data_type),
      "Pipeline output should be picket before pipeline input or patient input"
  end

  test "#pick_file_by_type takes older file from group" do
    @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_old.zip")
    @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_new.zip")

    assert_equal @pipeline.outputs_blobs.first, @pipeline.pick_file_by_type(@image_type.data_type),
      "Oldest pipeline output should be picked"
  end

  test "one data file can have many types" do
    first_dft = data_file_types("image")
    second_dft = create(:data_file_type, name: "second_image", pattern: "^imaging.*\.zip$")

    @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    data_file = @pipeline.outputs_blobs.first

    assert_equal data_file, @pipeline.pick_file_by_type(first_dft.data_type), "Data file should have image data type"
    assert_equal data_file, @pipeline.pick_file_by_type(second_dft.data_type), "Data file should have second image data type"
  end

  test "#pick_file_by_filename nil when filename is not found in pipeline and patient" do
    assert_nil @pipeline.pick_file_by_filename("non-existing.zip")
  end

  test "#pick_file_by_filename takes patient input file when no other options" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    assert_equal @patient.inputs_blobs.first, @pipeline.pick_file_by_filename("imaging_1.zip")
  end

  test "#pick_file_by_filename preffer pipeline input to the patient input" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    assert_equal @pipeline.inputs_blobs.first, @pipeline.pick_file_by_filename("imaging_1.zip"),
      "Pipeline input should be picked before patient input"
  end

  test "#pick_file_by_filename preffer pipeline output to the pipeline input and patient input" do
    @patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    @pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    @pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    assert_equal @pipeline.outputs_blobs.first, @pipeline.pick_file_by_filename("imaging_1.zip"),
      "Pipeline output should be picket before pipeline input or patient input"
  end

  test "attaching a file for pipeline queues Pipelines::StartRunnableJob" do
    assert_enqueued_with(job: Pipelines::StartRunnableJob, args: [@pipeline]) do
      @pipeline.add_input(fixture_file_upload("id_rsa"))
    end
  end
end
