# frozen_string_literal: true

require "test_helper"

class MembershipTest < ActiveSupport::TestCase
  test "admins needs to be approved" do
    organization = organizations(:main)

    assert_equal [ users(:admin) ], organization.admins

    memberships(:admin).update(state: :new_account)
    assert_empty organization.admins.reload,
      "New memberships should not be taked into account"

    memberships(:admin).update(state: :blocked)
    assert_empty organization.admins.reload,
      "Blocked membership should not be taked into account"
  end
end
