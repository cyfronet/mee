# frozen_string_literal: true

require "test_helper"

class Patient::UploaderTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include ActiveJob::TestHelper

  test "enqueues Patient::UploaderJob after creation" do
    assert_enqueued_jobs(1, only: Patients::UploadJob) do
      create(:patient_uploader, patient_archive: fixture_file_upload("patients.zip"))
    end
  end
end
