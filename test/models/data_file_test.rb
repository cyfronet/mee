# frozen_string_literal: true

require "test_helper"

class DataFileTest < ActiveSupport::TestCase
  test "discover data file types" do
    blob = ActiveStorage::Blob.new(filename: "imaging_1.zip")

    image_type = data_file_types("image")
    second_type = create(:data_file_type, pattern: "ima.*\.zip")

    discovered_types = DataFile.new(blob, organizations("main")).types

    assert_equal 2, discovered_types.size
    assert_includes discovered_types, image_type
    assert_includes discovered_types, second_type
  end
end
