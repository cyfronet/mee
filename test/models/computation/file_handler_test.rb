# frozen_string_literal: true

require "test_helper"

class Computation::FileHandlerTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  include ActiveJob::TestHelper

  def setup
    @computation = create(:computation)
    Current.organization = organizations("main")
  end

  test "attaching output queues Pipelines::StartRunnableJob" do
    assert_enqueued_with(job: Pipelines::StartRunnableJob) do
      @computation.add_output(fixture_file_upload("id_rsa"))
    end
  end
end
