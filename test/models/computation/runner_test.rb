# frozen_string_literal: true

require "test_helper"

class Computation::RunTest < ActiveSupport::TestCase
  include GitlabHelper
  include ActiveJob::TestHelper

  setup do
    @step = steps("first")
    @computation = create(:computation,
      step: @step,
      job_id: "some_id",
      parameter_values_attributes: {
        "tag-or-branch" => { version: "master" },
        "allocation" => { value: allocations("cpu").name },
      })
  end

  test "starts the computation" do
    stub_get_repo_file(@step, "master", Base64.encode64("script payload"))

    assert_enqueued_jobs 1, only: Computations::StartJob do
      freeze_time do
        Computation::Run.new(@computation).call
        assert_equal "script payload", @computation.script
        assert_equal "script_generated", @computation.status
        assert_equal Time.current, @computation.started_at
        assert_nil @computation.job_id
        assert_not_nil @computation.secret
      end
    end
  end

  test "transfers script generation errors to computation errors" do
    stub_get_repo_file(@step, "master", Base64.encode64("{% license_for ansys %}"))

    Computation::Run.new(@computation).call

    assert_not @computation.valid?
    assert_includes @computation.errors[:script], "cannot find requested ansys license"
  end
end
