# frozen_string_literal: true

require "test_helper"

class Computation::AbortTest < ActiveSupport::TestCase
  test "aborts active computation and notify about this event" do
    computation = create_computation(status: :running)
    Plgrid::HpcClientMock.stub_abort(computation.job_id)

    assert_changes "computation.status", to: "aborted" do
      call(computation)
    end
  end

  test "does nothing for non running computations" do
    computation = create_computation(status: :finished)
    Plgrid::HpcClientMock.stub_abort(computation.job_id)

    assert_no_changes "computation.status" do
      call(computation)
    end
  end

  test "does nothing if  proxy is outdated" do
    computation = create_computation(status: :running)

    travel 2.days do
      call(computation)
    end

    assert_equal "running", computation.status
  end

  test "does nothing when abort fails" do
    computation = create_computation(status: :running)
    Plgrid::HpcClientMock.stub_abort(computation.job_id, status: 500)

    assert_no_changes "computation.status" do
      call(computation)
    end
  end

  private
    def create_computation(status:)
      create(:computation, status:, job_id: "jid", started_at: Time.current)
    end

    def call(computation)
      Computation::Abort.new(computation).call
    end
end
