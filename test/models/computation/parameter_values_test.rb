# frozen_string_literal: true

require "test_helper"

class Computation::ParameterValuesTest < ActiveSupport::TestCase
  include GitlabHelper

  setup do
    @step = create(:step, parameters: [
      build(:model_version_parameter, key: "tag-or-branch"),
      build(:model_version_parameter, key: "second")
    ])
    stub_repo_versions(@step, branches: ["a1"], tags: ["b2"])

    @computation = create(:computation,
      step: @step,
      parameter_values_attributes: {
        "tag-or-branch" => { version: "a1" },
        "allocation" => { value: allocations("cpu").name },
        "second" => { version: "b2" }
      })
  end

  test "creates parameter values" do
    assert_predicate @computation, :valid?
    assert_equal 3, @computation.parameter_values.size
    assert_equal "a1", @computation.parameter_value_for("tag-or-branch").value
    assert_equal allocations("cpu").name, @computation.parameter_value_for("allocation").value
    assert_equal "b2", @computation.parameter_value_for("second").value
  end

  test "updates existing parameters" do
    @computation.update(parameter_values_attributes: {
                         "tag-or-branch" => { version: "b2" },
                         "second" => { version: "a1" }
                       })

    assert_predicate @computation, :valid?
    assert_equal 3, @computation.parameter_values.size
    assert_equal "b2", @computation.parameter_value_for("tag-or-branch").value
    assert_equal "a1", @computation.parameter_value_for("second").value
  end
end
