# frozen_string_literal: true

require "test_helper"

class Computation::CheckerTest < ActiveSupport::TestCase
  def setup
    @computation = create(:computation)
    @checker = Computation::Checker.new(@computation)
  end

  test "runnable? returns false when computation is not configured" do
    @computation.pipeline.stubs(:archived?).returns(true)

    assert_equal false, @checker.runnable?
  end

  test "runnable? returns false when quota is exceeded" do
    @computation.organization.quota.stubs(:exceeded?).returns(true)

    assert_equal false, @checker.runnable?
  end

  test "runnable? returns false when previous not accounted" do
    FlowStep.any_instance.stubs(:wait_for_previous).returns(true)
    create(:computation, pipeline: @computation.pipeline, created_at: @computation.created_at - 1.hour, status: :running)

    assert_equal false, @checker.runnable?
  end

  test "runnable? returning true" do
    @computation.stubs(:tag_or_branch).returns("master")
    @computation.stubs(:inputs_present?).returns(true)

    FlowStep.any_instance.stubs(:wait_for_previous).returns(false)

    assert_predicate(@checker, :runnable?)
  end

  test "configured? returning true" do
    @computation.stubs(:tag_or_branch).returns("master")
    @computation.stubs(:inputs_present?).returns(true)

    assert_predicate(@checker, :configured?)
  end

  test "configured? returns false when pipeline is archived" do
    @computation.stubs(:archived?).returns(true)

    assert_equal false, @checker.configured?
  end

  test "configured? returns false when tag_or_branch is missing" do
    @computation.stubs(:inputs_present?).returns(true)

    assert_equal false, @checker.configured?
  end

  test "configured? returns false when input is missing" do
    @computation.stubs(:tag_or_branch).returns("master")
    @computation.stubs(:inputs_present?).returns(false)

    assert_equal false, @checker.configured?
  end

  test "configurable? returns false when pipeline is archived" do
    @computation.pipeline.stubs(:archived?).returns(true)

    assert_equal false, @checker.configurable?
  end


  test "configurable? returns false when input is not present" do
    @computation.pipeline.stubs(:data_files_present?).returns(false)

    assert_equal false, @checker.configurable?
  end

  test "configurable? returning true" do
    @computation.pipeline.stubs(:data_files_present?).returns(true)
    FlowStep.any_instance.stubs(:wait_for_previous).returns(false)

    assert_predicate(@checker, :configurable?)
  end
end
