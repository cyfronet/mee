# frozen_string_literal: true

require "test_helper"

class StepTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile

  setup do
    @pipeline = create(:pipeline)
  end

  test "is runnable when no prerequisites" do
    step = build(:step)

    assert @pipeline.data_files_present?(step.required_file_types)
  end

  test "is runnable when all prerequisites are in place" do
    image, segmentation = data_file_types("image", "segmentation")
    step = build(:step, required_file_types: [image, segmentation])

    assert_not @pipeline.data_files_present?(step.required_file_types)

    @pipeline.runnable.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")

    assert_not @pipeline.data_files_present?(step.required_file_types)

    @pipeline.runnable.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "segmentation.zip")

    assert @pipeline.data_files_present?(step.required_file_types)
  end

  test "#slug is organization scoped" do
    first = steps("first")
    step = create(:step, name: " #{first.name}  ",
                  organization: organizations("other"))

    assert_equal first.slug, step.slug
  end

  test "#name is organization scoped" do
    first = steps("first")
    step = build(:step, name: " #{first.name}  ")

    assert_not step.valid?
    assert_predicate step.errors["name"], :present?, "Name error should be present"

    step.organization = organizations("other")
    assert_predicate step, :valid?
  end

  test "Shouldn't discard or destroy when active flows" do
    step = steps("second")

    assert_no_difference "Step.count" do
      assert_not step.discard_or_destroy
    end
  end

  test "#matching allocations" do
    assert_equal [allocations(:cpu)], steps(:first).matching_allocations

    create(:expired_allocation)
    create(:future_allocation)
    create(:allocation, resource_types: [resource_types(:gpu)])
    create(:allocation, sites: [sites(:athena)])
    matching_allocation = create(:allocation)

    assert_equal [allocations(:cpu), matching_allocation], steps(:first).matching_allocations
  end
end
