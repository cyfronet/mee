# frozen_string_literal: true

require "test_helper"

class Parameter::RequiredDataFileTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile

  setup do
    @data_file_type = data_file_types("image")
  end

  test "generates correct name based on data_file_type" do
    parameter = Parameter::RequiredDataFile.new(data_file_type: @data_file_type, pipeline: Pipeline.new)

    assert_equal @data_file_type.name, parameter.name
  end

  test "generates correct key based on data_file_type" do
    parameter = Parameter::RequiredDataFile.new(data_file_type: @data_file_type, pipeline: Pipeline.new)

    assert_equal "#{Parameter::RequiredDataFile::PREREQUISITE}#{@data_file_type.data_type}", parameter.key
  end

  test "load data files from pipeline" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient)


    patient.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_1.zip")
    pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_2.zip")
    pipeline.outputs.attach(io: fixture_file_upload("id_rsa"), filename: "imaging_3.zip")

    parameter = Parameter::RequiredDataFile.new(data_file_type: @data_file_type, pipeline:)
    data_files = parameter.data_files.to_h

    assert_equal 1, data_files["Pipeline inputs"].size
    assert_equal pipeline.inputs_blobs.first, data_files["Pipeline inputs"].first

    assert_equal 1, data_files["Pipeline outputs"].size
    assert_equal pipeline.outputs_blobs.first, data_files["Pipeline outputs"].first

    assert_equal 1, data_files["Patient inputs"].size
    assert_equal patient.inputs_blobs.first, data_files["Patient inputs"].first
  end
end
