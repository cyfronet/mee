# frozen_string_literal: true

require "test_helper"

class Parameter::NumberTest < ActiveSupport::TestCase
  setup do
    @parameter = build(:number_parameter, min: 1, max: 3)
  end

  test "validates max is less than min" do
    @parameter.max = 0

    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:max]
  end

  test "validates default value between min, max" do
    @parameter.default_value = 2

    @parameter.min, @parameter.max = nil, nil
    assert_predicate @parameter, :valid?

    @parameter.min, @parameter.max = 1, 3
    assert_predicate @parameter, :valid?

    @parameter.min, @parameter.max = 3, nil
    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:default_value]

    @parameter.min, @parameter.max = nil, 1
    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:default_value]

    @parameter.min, @parameter.max = 0, 1
    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:default_value]

    @parameter.min, @parameter.max = 3, 4
    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:default_value]
  end
end
