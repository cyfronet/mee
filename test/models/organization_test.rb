# frozen_string_literal: true

require "test_helper"

class OrganizationTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "has many approved users" do
    org = organizations("main")
    Membership.create!(user: build(:user), organization: org, state: :new_account)
    Membership.create!(user: build(:user), organization: org, state: :blocked)

    assert_equal users("admin", "other", "user").sort,
                 org.approved_users.sort
  end

  test "organization administrators" do
    user = users(:user)
    user.update(roles: [:admin]) # globals admin are not taked into account

    organization = organizations(:main)

    assert_equal [ users(:admin) ], organization.admins
  end

  test "blocked administrators are not returned" do
    membership = memberships(:admin)
    membership.update(state: :blocked)

    assert_empty organizations(:main).admins
  end

  test "validate only active steps" do
    first, second, multi_select = steps("first", "second", "multi_select")
    organization = organizations("main")

    second.discard!
    multi_select.discard!

    assert_enqueued_jobs 1, only: Step::ValidateRepositoryJob do
      assert_enqueued_with job: Step::ValidateRepositoryJob, args: [first] do
        organization.validate_later
      end
    end
  end
end
