# frozen_string_literal: true

require "test_helper"

class Step::GitConfigTest < ActiveSupport::TestCase
  test "step uses organization default when override not defined" do
    step = build(:step)

    assert_nil step.git_config
    assert_instance_of GitRepository::GitlabClient, step.git_repository
  end

  test "creates git repository with local git config" do
    step = build(:step, git_config: build(:git_config))

    assert_not_nil step.git_config
    assert_instance_of GitRepository::NativeClient, step.git_repository
  end

  test "validates git config" do
    step = build(:step,
                 git_config: build(:gitlab_git_config,
                 host: "   ", download_key: ""))

    assert_not step.valid?
    assert_not step.git_config.valid?
  end
end
