# frozen_string_literal: true

require "test_helper"

class Step::StepTest < ActiveSupport::TestCase
  test "creates parameters while creating step" do
    step = step_with_select(select_key: "select1", values: ["1", "2"])
    select = step.parameter_for("select1")

    assert_predicate step, :persisted?
    assert_equal 3, step.parameters.size

    assert_equal "select1 name", select.name
    assert_equal 2, select.values.size
    assert_equal ["1", "2"], select.values.map(&:value)
  end

  test "updates existing parameter" do
    step = step_with_select(select_key: "select1", values: ["1", "2"])
    select = step.parameter_for("select1")

    step.update(parameters_attributes: {
      "0" =>
        select_hsh(select_key: "new_key", values: ["1", "2", "3"])
          .merge("id" => select.id.to_s)
    })
    updated_select = step.parameter_for("new_key")

    assert_predicate step, :valid?
    assert_equal 3, step.parameters.size

    assert_equal select.id, updated_select.id
    assert_equal "new_key name", updated_select.name
    assert_equal 3, updated_select.values.size
    assert_equal ["1", "2", "3"], updated_select.values.map(&:value)
  end

  test "deletes existing parameter" do
    step = step_with_select(select_key: "select1", values: ["1", "2"])
    select = step.parameter_for("select1")

    assert select, "Parameter should exist"

    step.update(parameters_attributes: {
      "0" => { "id" => select.id.to_s, "_destroy" => true }
    })

    assert_not step.parameter_for("select1"), "Parameter should be deleted"
  end

  test "validates step uniqueness" do
    step = build(:step, parameters_attributes: {
      "0" => select_hsh(select_key: "select", values: ["1", "2"]),
      "1" => select_hsh(select_key: "select", values: ["3"])
    })

    assert_not step.valid?
    assert step.parameters.first.errors[:key]
    assert step.parameters.second.errors[:key]
  end

  private
    def step_with_select(select_key:, values:)
      create(:step, parameters_attributes: {
        "0" => select_hsh(select_key:, values:)
      })
    end

    def select_hsh(select_key:, values:)
      {
        "type" => "select",
        "name" => "#{select_key} name",
        "key" => select_key,
        "values_attributes" =>
          values.index_with { |v| { name: "Option #{v}", value: v } }
      }
    end
end
