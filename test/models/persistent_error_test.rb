# frozen_string_literal: true

require "test_helper"

class PersistentErrorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  def setup
    @organization = organizations("main")
    @persistent_error = PersistentError.new(
      key: "some_key",
      message: "some_message",
      errorable: @organization
    )
  end

  test "should notify admins" do
    @persistent_error.message = "token has expired"

    assert_enqueued_jobs 1, only: ActionMailer::MailDeliveryJob do
      @persistent_error.save
    end
  end
end
