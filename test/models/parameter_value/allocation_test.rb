# frozen_string_literal: true

require "test_helper"

class ParameterValue::AllocationTest < ActiveSupport::TestCase
  setup do
    @pv = build(:allocation_parameter_value)
  end

  test "allows only active allocations" do
    @pv.value = "nonexisting"
    assert_not @pv.valid?
    assert_match "not included in the list", @pv.errors[:value].first

    @pv.value = allocations("cpu").name
    assert_predicate @pv, :valid?
  end

  test "allows only allocations of correct type" do
    # step that is created in fixtures allows only cpu allocations
    @pv.value = allocations("gpu").name
    assert_not @pv.valid?
  end

  test "default allocation name is returned when value is not set and there is only one allocation" do
    assert_equal allocations("cpu").name, @pv.value
  end

  test "returns value when it is set" do
    @pv.value = "othervalue"
    assert_equal "othervalue", @pv.value
  end

  test "returns nothing when value is not set and there is more than one allocation" do
    create(:allocation)

    assert_nil @pv.value
  end
end
