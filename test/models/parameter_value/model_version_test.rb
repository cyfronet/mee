# frozen_string_literal: true

require "test_helper"

class ParameterValue::ModelVersionTest < ActiveSupport::TestCase
  include GitlabHelper

  setup do
    @pv = build(:model_version_parameter_value)
  end

  test "allows only version by step repository" do
    Rails.cache.clear
    stub_repo_versions(@pv.parameter.step,
                       { branches: ["master"], tags: ["T1"] })

    @pv.version = "nonexisting"
    assert_not @pv.valid?
    assert_match "select correct model version", @pv.errors[:version].first

    @pv.version = "master"
    assert_predicate @pv, :valid?

    @pv.version = "T1"
    assert_predicate @pv, :valid?
  end
end
