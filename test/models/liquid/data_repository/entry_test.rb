# frozen_string_literal: true

require "test_helper"

class Liquid::DataRepository::EntryTest < ActiveSupport::TestCase
  setup do
    @organization = organizations("main")
  end

  test "prepare data repository entry" do
    entry = Liquid::DataRepository::Entry.new(@organization, users("user"))
    assert_equal entry.url, @organization.data_repository_config.url
    assert_equal entry.token, memberships("user").data_repository_token
  end
end
