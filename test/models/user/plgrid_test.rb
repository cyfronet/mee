# frozen_string_literal: true

require "test_helper"
require "ostruct"

CcmHelpers.default_ccm_stubs!

class User::PlgridTest < ActiveSupport::TestCase
  test "plgrid login creates new user if does not exist" do
    assert_difference "User.count", 1, "Expected a new user to be created" do
      User.from_plgrid_omniauth(auth("plgnewuser", token: CcmHelpers::VALID_TOKEN)).save
    end
  end

  test "plgrid login uses auth info to populate user data" do
    user = User.from_plgrid_omniauth(auth("plgnewuser", token: CcmHelpers::VALID_TOKEN))

    assert_equal "plgnewuser", user.plgrid_login
    assert_equal "plgnewuser@b.c", user.email
    assert_equal "plgnewuser", user.first_name
    assert_equal "Last Name", user.last_name
    assert_equal CredentialsProvider.key, user.ssh_key
    assert_equal CredentialsProvider.cert, user.ssh_certificate
  end

  test "raise exception when openid connect token is not valid" do
    assert_raise Plgrid::Ccm::FetchError do
      User.from_plgrid_omniauth(auth("plgnewuser", token: "invalid"))
    end
  end

  test "plgrid login creates organization membership" do
    org = organizations("main")
    user = User.from_plgrid_omniauth(
      auth("plgnewuser", token: CcmHelpers::VALID_TOKEN, groups: [org.plgrid_team_id]))

    assert_equal org, user.memberships.first.organization
  end

  private
    def auth(plglogin, token:, groups: [])
      OpenStruct.new(
        info: OpenStruct.new(
          nickname: plglogin,
          email: "#{plglogin}@b.c",
          name: "#{plglogin} Last Name"
        ),
        credentials: OpenStruct.new(
          token:
        ),
        extra: OpenStruct.new(
          raw_info: OpenStruct.new(groups:)
        )
      )
    end
end
