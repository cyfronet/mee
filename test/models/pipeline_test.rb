# frozen_string_literal: true

require "test_helper"

class PipelineTest < ActiveSupport::TestCase
  test "generates relative pipeline id" do
    patient = create(:patient)

    p1 = create(:pipeline, runnable: patient)
    p2 = create(:pipeline, runnable: patient)

    assert_equal 1, p1.iid
    assert_equal 2, p2.iid
  end

  test "calls StatusCalculator when asked for status" do
    Pipeline::StatusCalculator.any_instance.expects(:calculate)
    pipeline = create(:pipeline)
    pipeline.update_status!
  end

  test "returns creator name" do
    user = users("user")
    pipeline = create(:pipeline, user:)

    assert_equal user.name, pipeline.owner_name
  end

  test "returns information about deleted user when owner is nil" do
    user = users("admin")
    pipeline = create(:pipeline, user:)

    user.destroy!
    pipeline.reload

    assert_equal "(deleted user)", pipeline.owner_name
  end

  test "destroys discarded flow when it has only one pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)

    assert_changes "Flow.count", -1 do
      pipeline.destroy
    end
  end

  test "doesn't destroy the flow when it has more pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)
    create(:pipeline, flow:)

    assert_no_changes "Flow.count" do
      pipeline.destroy
    end
  end
end
