# frozen_string_literal: true

require "test_helper"

class AllocationTest < ActiveSupport::TestCase
  test "#active returns only active allocations" do
    create(:expired_allocation)
    create(:future_allocation)

    assert_equal allocations("cpu", "gpu").sort, Allocation.active.sort
  end
end
