# frozen_string_literal: true

require "test_helper"

class ArtifactTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile

  def setup
    Current.organization = organizations("main")
    pipeline = create(:pipeline)
    pipeline.inputs.attach(io: fixture_file_upload("id_rsa"), filename: "sth")
    @attachment_id = pipeline.inputs.first.id
  end

  test "#create creates file from file on the server" do
    assert_difference "Current.organization.artifacts.size", 1 do
      Artifact.create(filename: "test", attachment_id: @attachment_id)
    end
  end

  test "when creating artifact with same name, deletes old file" do
    old = Artifact.create(filename: "test", attachment_id: @attachment_id)
    Current.organization.reload
    assert_equal 1, Current.organization.artifacts.size
    assert_no_difference "Current.organization.artifacts.size" do
      new = Artifact.create(filename: "test", attachment_id: @attachment_id)
      assert_equal Current.organization.artifacts.first, new.attachment
    end
    assert_raises ActiveRecord::RecordNotFound do
      old.attachment
    end
  end

  test "deletes file" do
    artifact = Artifact.create(filename: "test", attachment_id: @attachment_id)
    Current.organization.reload
    assert_difference "Current.organization.artifacts.size", -1 do
      artifact.delete
    end
  end

  test ".upload" do
    assert_difference "Current.organization.artifacts.size", 1 do
      Artifact.upload(files: [fixture_file_upload("id_rsa")])
    end
  end

  test ".attach" do
    assert_difference "Current.organization.artifacts.size", 1 do
      Artifact.attach([ActiveStorage::Blob.first.signed_id])
    end
  end

  test "when uploading artifact with same name, deletes old file" do
    Artifact.upload(files: [fixture_file_upload("id_rsa")])
    old = Current.organization.artifacts.first
    Current.organization.reload
    assert_equal 1, Current.organization.artifacts.size
    assert_no_difference "Current.organization.artifacts.size" do
      Artifact.upload(files: [fixture_file_upload("id_rsa")])
      assert_not_equal Current.organization.artifacts.first, old
    end
    assert_raises ActiveRecord::RecordNotFound do
      old.reload
    end
  end

  test "#update" do
    artifact = Artifact.create(filename: "test", attachment_id: @attachment_id)
    artifact.update(filename: "new name")
    assert_equal "new name", artifact.attachment.blob.filename.to_s
    assert_equal "new name", artifact.filename
  end
end
