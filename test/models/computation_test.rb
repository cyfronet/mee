# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class ComputationTest < ActiveSupport::TestCase
  test "#active returns only script_generated, queued or running computations" do
    computation = create(:computation, status: "script_generated")
    assert_equal [computation], Computation.active

    computation.update(started_at: Time.current, status: "queued")
    assert_equal [computation], Computation.active

    computation.update(status: "running")
    assert_equal [computation], Computation.active

    computation.update(status: "finished")
    assert_equal [], Computation.active
  end

  test "#submitted returns only queued and running computations" do
    create(:computation, status: "script_generated")
    queued = create(:computation, status: "queued")
    running = create(:computation, status: "running")

    assert_equal [queued, running].sort, Computation.submitted.sort
  end

  test "Computation is configured only if tag or branch is set" do
    assert_not build(:computation).configured?
    assert_predicate build(:computation, parameter_values_attributes: { tag_or_branch: { value: "master" } }), :configured?
  end

  test "runnable? returns correct values with wait for previous set to true" do
    flow = create(:flow)
    step1 = create(:step)
    step2 = create(:step)
    create(:flow_step, flow:, step: step1)
    create(:flow_step, flow:, step: step2, wait_for_previous: true)
    pipeline = create(:pipeline, flow:)
    computation1 = create(:computation, pipeline:, step: step1, status: :running, started_at: Time.now)
    computation2 = create(:computation, pipeline:, step: step2, status: :created,
      parameter_values: [build(:model_version_parameter_value, parameter: parameters("first_tag_or_branch"))])

    assert_equal false, computation2.runnable?
    computation1.update(status: :finished)
    assert_predicate computation2, :runnable?
  end

  test "campaign computation #runnable? only when campaign is running" do
    campaign = create(:campaign, status: :created)
    pipeline = create(:pipeline, mode: :automatic, campaign:)
    computation = create(:computation,
      step: steps("first"), pipeline:,
      parameter_values: [
        build(:model_version_parameter_value, parameter: parameters("second_tag_or_branch"))
      ]
    )

    assert_not computation.runnable?

    campaign.running!
    assert_predicate computation, :runnable?
  end

  test "fetch logs" do
    c = create(:computation)

    Plgrid::HpcMock.stub_stdout(c, "stdout log")
    Plgrid::HpcMock.stub_stderr(c, "stderr log")

    assert_equal "stdout log", c.stdout_live.string
    assert_equal "stderr log", c.stderr_live.string
    assert_nil c.stdout_blob
    assert_nil c.stderr_blob

    c.fetch_logs

    assert_equal "stdout log", c.stdout.download
    assert_equal "stderr log", c.stderr.download
  end

  test "destroying active computation calls abort" do
    computation = create(:computation, status: "created")
    active_computation = create(:computation, status: "script_generated", started_at: Time.current)

    mock = Minitest::Mock.new
    mock.expect(:call, true)
    Computation::Abort.expects(:new).with(active_computation).returns(mock)
    Computation::Abort.expects(:new).with(computation).never

    computation.destroy
    active_computation.destroy
  end
end
