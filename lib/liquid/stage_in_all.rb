# frozen_string_literal: true

require_relative "../link_helper"

module Liquid
  class StageInAll < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, parameters, tokens)
      super
      @type = parameters.strip # "pipeline_inputs", "pipeline_outputs", "patient_inputs"
    end

    def render(context)
      computation = context.registers[:computation]
      if computation
        all_files_url = computation_all_filenames_url(computation, secret: computation.secret,
          type: @type, script_name: script_name(computation), protocol: :https)
        download_url = computation_filename_inputs_url(computation, secret: computation.secret,
          filename: "tmp", script_name: script_name(computation), protocol: :https).delete_suffix("tmp")

        <<~BASH
          echo Downloading #{@type} list
          if ! module list 2>&1 | grep jq > /dev/null; then
            module load jq
          fi

          filenames=$(curl --retry 5 --retry-max-time 600 --retry-connrefused #{all_files_url} | jq -r '.files[]')
          for filename in ${filenames[@]}; do
            curl --retry 5 --retry-max-time 600 --retry-connrefused  -o $filename -s #{download_url}$filename?type=#{@type} &
          done
          echo All #{@type} downloaded
        BASH

      else
        context.registers[:errors]
          .add(:script, "cannot find #{@data_file_type_string} data file in patient or pipeline directories")
      end
    end
  end
end
# for filename in ${filenames[@]}; do curl -O "https://e790-188-146-144-25.ngrok-free.app/0000001/computations/294/files/djCPZaXDbRPnZPaR/filename/$filename?type=pipeline_inputs"; done
