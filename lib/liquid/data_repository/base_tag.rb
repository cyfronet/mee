# frozen_string_literal: true

module Liquid
  module DataRepository
    class BaseTag < Liquid::Tag
      def initialize(tag_name, parameters, tokens)
        super
      end

      def type
        self.class.module_parent
      end

      def render(context)
        computation = context.registers[:computation]
        entry = "#{type}::Entry".constantize.new(computation.organization, computation.user)
        if entry.valid?
          curl_cmd(entry)
        else
          entry.errors.each { |error| context.registers[:errors].add(:script, error.message) }
        end
      end

      def curl_cmd(_entry)
        raise NotImplementedError, "Please implement this method in a subclass"
      end
    end
  end
end
