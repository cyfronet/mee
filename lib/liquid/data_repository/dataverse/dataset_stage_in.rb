# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Dataverse
      class DatasetStageIn < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @persistent_id, @filename = parameters.split
        end

        def curl_cmd(entry)
          url = entry.dataset_url(@persistent_id)
          file = @filename.blank? ? "$(basename #{url})" : @filename

          <<~COMMAND
            RESPONSE=$(curl -L -w "%{http_code}" -o #{file} -H "X-Dataverse-key:#{entry.token}" #{url})
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to download Dataverse dataset with persistent identifier: #{@persistent_id}"
              >&2 cat #{file}
              rm #{file}
              exit 1
            elif [ -z "#{@filename}" ]
            then
              unzip #{file}
              rm #{file} MANIFEST.TXT
            fi
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
