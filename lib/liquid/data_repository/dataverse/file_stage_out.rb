# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Dataverse
      class FileStageOut < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @relative_path, @persistent_id, @metadata = parameters.split(" ", 3)
        end

        def curl_cmd(entry)
          url = entry.add_datafile_url(@persistent_id)
          response = "$(basename #{url})"

          unless @metadata.blank?
            json_data = "-F jsonData='" + @metadata.to_s + "' "
          end

          <<~COMMAND
            RESPONSE=$(curl -w "%{http_code}" -o #{response} -H "X-Dataverse-key:#{entry.token}" -X POST -F file=@#{@relative_path} #{json_data}#{url})
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to upload file: #{@relative_path}: "
              >&2 cat #{response}
              rm #{response}
              exit 1
            fi
            rm -f #{response}
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
