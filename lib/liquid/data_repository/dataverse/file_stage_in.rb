# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Dataverse
      class FileStageIn < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @persistent_id, @filename = parameters.split
        end

        def curl_cmd(entry)
          url = entry.datafile_url(@persistent_id)
          file = @filename.blank? ? "$(basename #{url})" : @filename
          output_option = @filename.blank? ? "-O" : "-o #{file}"

          <<~COMMAND
            LOCATION="$(pwd)"
            RESPONSE=$(curl -L #{output_option} -J -w "%{http_code}" -H "X-Dataverse-key:#{entry.token}" #{url})
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to download Dataverse file with persistent identifier: #{@persistent_id}"
              >&2 cat #{file}
              rm #{file}
              exit 1
            fi
            cd $LOCATION
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
