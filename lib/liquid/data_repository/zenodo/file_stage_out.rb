# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Zenodo
      class FileStageOut < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @relative_path, @persistent_id = parameters.split
        end

        def curl_cmd(entry)
          url = entry.add_datafile_url(@persistent_id)
          response = "$(basename #{url})"

          <<~COMMAND
            RESPONSE=$(curl -w "%{http_code}" -o #{response} -H "Authorization: Bearer #{entry.token}" -X POST -F file=@#{@relative_path} #{url})
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "201" ]
            then
              >&2 echo "Failed to upload file: #{@relative_path}: "
              >&2 cat #{response}
              rm #{response}
              exit 1
            fi
            rm -f #{response}
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
