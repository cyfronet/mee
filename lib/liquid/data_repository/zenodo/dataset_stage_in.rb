# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Zenodo
      class DatasetStageIn < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @persistent_id, @filename = parameters.split
        end

        def curl_cmd(entry)
          file = @filename.blank? ? @persistent_id : @filename

          <<~COMMAND
            CHECK_PUB=$(curl -L -w "%{http_code}" -o /dev/null -H "Authorization: Bearer #{entry.token}" #{entry.check_access_url(@persistent_id)})
            if [ "${CHECK_PUB}" == "404" ]
            then
              URL=#{entry.draft_dataset_url(@persistent_id)}
            else
              URL=#{entry.public_dataset_url(@persistent_id)}
            fi

            RESPONSE=$(curl -L -w "%{http_code}" -o #{file} -H "Authorization: Bearer #{entry.token}" $URL)
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to download Zenodo's record/deposition: #{@persistent_id}"
              >&2 cat #{file}
              rm #{file}
              exit 1
            elif [ -z "#{@filename}" ]
            then
              unzip #{file}
            fi
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
