# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Zenodo
      class FileStageIn < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @persistent_id, @filename, @target = parameters.split
        end

        def curl_cmd(entry)
          file = @target.blank? ? @filename : @target

          <<~COMMAND
            CHECK_PUB=$(curl -L -w "%{http_code}" -o /dev/null -H "Authorization: Bearer #{entry.token}" #{entry.check_access_url(@persistent_id)})
            HTTP_CODE=${CHECK_PUB: -3}
            if [ "${HTTP_CODE}" == "404" ]
            then
              URL=#{entry.draft_datafile_url(@persistent_id, @filename)}
            else
              URL=#{entry.public_datafile_url(@persistent_id, @filename)}
            fi

            RESPONSE=$(curl -L -o #{file} -J -w "%{http_code}" -H "Authorization: Bearer #{entry.token}" $URL)
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to download file: #{@filename} from Zenodo's record/deposition: #{@persistent_id}"
              >&2 cat #{file}
              rm #{file}
              exit 1
            fi
          COMMAND
        end

        def render(context)
          super(context)
        end
      end
    end
  end
end
