# frozen_string_literal: true

require_relative "../base_tag"

module Liquid
  module DataRepository
    module Invenio
      class FileStageOut < Liquid::DataRepository::BaseTag
        def initialize(tag_name, parameters, tokens)
          super
          @relative_path, @persistent_id = parameters.split
        end

        # https://inveniordm.docs.cern.ch/reference/rest_api_quickstart/
        def curl_cmd(entry)
          <<~COMMAND
            filename=`basename #{@relative_path}`

            echo "Staging out #{@relative_path}:"
            echo "  1. Initialize the file upload for #{@relative_path} file."
            response=`mktemp`
          RESPONSE=$(curl -k -w "%{http_code}" -o $response -H "Content-Type: application/json" -H "Authorization: Bearer #{entry.token}" -X POST #{entry.draft_files(@persistent_id)} -d "[{\\"key\\": \\"$filename\\"}]")
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "201" ]
            then
              >&2 echo "Failed to initialize file upload for #{@relative_path} file: "
              >&2 cat $response
              exit 1
            fi

            echo "  2. Upload file #{@relative_path} content."
            response=`mktemp`
            RESPONSE=$(curl -k -w "%{http_code}" -o $response -H "Authorization: Bearer #{entry.token}" -H "Content-Type: application/octet-stream" -X PUT --data-binary @#{@relative_path} #{entry.draft_files(@persistent_id)}/$filename/content)
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to upload file #{@relative_path} content: "
              >&2 cat $response
              exit 1
            fi

            echo "  3. Commit uploaded #{@relative_path} file."
            response=`mktemp`
            RESPONSE=$(curl -k -w "%{http_code}" -o $response -H "Authorization: Bearer #{entry.token}" -X POST #{entry.draft_files(@persistent_id)}/$filename/commit)
            HTTP_CODE=${RESPONSE: -3}
            if [ "${HTTP_CODE}" != "200" ]
            then
              >&2 echo "Failed to upload file #{@relative_path} content: "
              >&2 cat $response
              exit 1
            fi
          COMMAND
        end
      end
    end
  end
end
