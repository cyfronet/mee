# frozen_string_literal: true

module Components::DropzoneComponent
  def dropzone_placeholder(wrapper_options = nil)
    template.content_tag(:div, class: "dropzone-msg dz-message needsclick text-gray-600") do
      image + title + description
    end
  end

  private
    def image
      template.content_tag(:span, class: "flex justify-center mb-2") do
        options[:image] || template.inline_svg_tag("dropzone_placeholder.svg")
      end
    end

    def title
      template.content_tag(:h3, class: "dropzone-msg-title") do
        options[:title] || I18n.t("dropzone.inputs.title")
      end
    end

    def description
      template.content_tag(:span, class: "dropzone-msg-desc text-sm") do
        options[:description] || I18n.t("dropzone.inputs.description")
      end
    end
end

SimpleForm.include_component(Components::DropzoneComponent)
