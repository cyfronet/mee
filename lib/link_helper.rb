# frozen_string_literal: true

module LinkHelper
  include Rails.application.routes.url_helpers

  def default_url_options
    Rails.application.config.action_mailer.default_url_options
  end

  def script_name(computation)
    "/#{OrganizationSlug.encode(computation.pipeline.runnable.organization_id)}"
  end
end
