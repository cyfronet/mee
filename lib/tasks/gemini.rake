# frozen_string_literal: true

namespace :gemini do
  desc "Setup Gemini organization"
  task setup: :environment do
    DemoOrganization.create! name: "Gemini", logo_path: "db/logos/gemini.png",
      ssh_key_path: ENV["PIPELINE_SSH_KEY"],
      gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
      github_api_private_token: ENV["GITHUB_API_PRIVATE_TOKEN"],
      plgrid_team_id: "plgggemini",
      allocations: [
        {
          name: "plggemini2025-cpu",
          starts_at: Date.parse("2025-02-04"),
          ends_at: Date.parse("2026-02-04"),
          sites: Site.where(name: ["ares", "helios"]),
          resource_types: [ResourceType.find_by(name: "cpu")]
        },
        {
          name: "plggemini2025-gpu-a100",
          starts_at: Date.parse("2025-02-04"),
          ends_at: Date.parse("2026-02-04"),
          sites: [Site.find_by(name: "athena")],
          resource_types: [ResourceType.find_by(name: "gpu")]
        }
      ]
  end
end
