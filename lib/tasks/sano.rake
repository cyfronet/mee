# frozen_string_literal: true

namespace :sano do
  desc "Setup sano organization"
  task setup: :environment do
    DemoOrganization.create! name: "Sano",
      ssh_key_path: ENV["PIPELINE_SSH_KEY"],
      gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
      github_api_private_token: ENV["GITHUB_API_PRIVATE_TOKEN"],
      plgrid_team_id: "plggsano",
      allocations: { plgsano2: Date.parse("2022-03-31"),
                plgsano3: Date.parse("2022-03-31"),
                "plgsano3-cpu": Date.parse("2022-03-31"),
                "plgsano3-gpu": Date.parse("2022-03-31"),
                "plgsanoathena-gpu-a100": Date.parse("2022-08-31") }
  end
end
