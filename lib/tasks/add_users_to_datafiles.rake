# frozen_string_literal: true

task add_users_to_datafiles: :environment do
  Organization.includes(:data_file_types, :users).find_each do |organization|
    admin_user = organization.admins.first
    organization.data_file_types.update_all(user_id: admin_user.id) if admin_user
  end
end
