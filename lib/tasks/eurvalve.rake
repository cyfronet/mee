# frozen_string_literal: true

namespace :eurvalve do
  desc "Setup EurValve organization"
  task setup: :environment do
    eurvalve = DemoOrganization.new name: "EurValve",
      ssh_key_path: ENV["PIPELINE_SSH_KEY"],
      gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
      plgrid_team_id: "plggeurvalve",
      allocations: { plgeurvalve7: Date.parse("2021-01-11") }

    def eurvalve.create_flows(org, types)
      steps_map = create_steps(org, types)
      names = {
        crc_model_cfd: "CRC model CFD",
        crc_model_rom: "CRC model ROM",
        avr_surgical_preparation: "AVR surgical prep",
        avr_from_scan_rom: "AVR from Scan (ROM)",
        avr_from_scan_cfd: "AVR from Scan (CFD)"
      }

      {
        crc_model_cfd: %w[cfd parameter_optimization uncertainty_analysis],
        crc_model_rom: %w[rom parameter_optimization uncertainty_analysis],
        avr_surgical_preparation: %w[rom],
        avr_from_scan_rom: %w[rom parameter_optimization 0d_models
                              uncertainty_analysis pressure_volume_display],
        avr_from_scan_cfd: %w[cfd parameter_optimization 0d_models
                              uncertainty_analysis pressure_volume_display]
      }.each_with_index do |(flow_slug, slugs), position|
        flow = Flow.create!(name: names[flow_slug], organization: org, position:)
        slugs.
          map { |slug| steps_map[slug.to_s] }.
          each_with_index do |step, i|
            FlowStep.create!(flow:, step:, position: i)
          end
      end
    end

    def eurvalve.create_steps(org, types)
      {
        "cfd" => Step.create!(name: "Computational Fluid Dynamics simulation",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/cfd", file: "cfd.sh.liquid",
            required_file_types: [types[:truncated_off_mesh]]),
        "parameter_optimization" => Step.create!(name: "Parameter Optimization",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/0dmodel", file: "parameter_optimization.sh.liquid",
            required_file_types: [types[:pressure_drops]]),
        "pressure_volume_display" => Step.create!(name: "PV Loop Comparison",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/0dmodel", file: "pv_display.sh.liquid",
            required_file_types: [types[:data_series_1], types[:data_series_2],
                                  types[:data_series_3], types[:data_series_4]]),
        "rom" => Step.create!(name: "Reduced Order Model",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/cfd", file: "rom.sh.liquid",
            required_file_types: [types[:rom_params]]),
        "uncertainty_analysis" => Step.create!(name: "Uncertainty Analysis",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/0dmodel", file: "uncertainty_analysis.sh.liquid",
            required_file_types: [types[:output_image_prov],
                                  types[:output_meas_prov]]),
        "0d_models" => Step.create!(name: "0D Model x4",
            organization: org, resource_type: cpu_resource_type,
            repository: "eurvalve/0dmodel", file: "0d_scenarios.sh.liquid",
            required_file_types: [types[:parameter_optimization_result]])
      }
    end

    def eurvalve.create_data_types(eurvalve)
      [
          [:segmentation_result, /^segmentation_.*\.zip$/, nil, "Segmentation output"],
          [:fluid_virtual_model, /^fluidFlow\.cas$/, nil, "3D heart model CFD input"],
          [:ventricle_virtual_model, /^structural_vent\.dat$/, nil, "Ventricle model"],
          [:blood_flow_result, /^fluidFlow.*\.dat$/, nil, "3D CFD output"],
          [:blood_flow_model, /^fluidFlow.*\.cas$/, nil, "Blood flow model"],
          [:estimated_parameters, /^0DModel_input\.csv$/, nil, "Estimated parameters"],
          [:heart_model_output, /^Outfile\.csv$/, nil, "0D heart model output"],
          [:truncated_off_mesh, /^.*Trunc.*off$/, nil, "Truncated 3D mesh"],
          [:off_mesh, /^.*\.off$/, nil, "3D mesh"],
          [:graphics, /^.*\.\b(png|bmp|jpg)\b$/, nil, "Visual image"],
          [:response_surface, /^.*\.dxrom$/, nil, "Response surface"],
          [:pressure_drops, /^ValveChar\.dat$/, nil, "Pressure drops"],
          [:parameter_optimization_result, /^OutFileGA\.csv$/, nil, "Parameter optimization result"],
          [:data_series_1, /^OutSeries1\.csv$/, nil, "Data series 1"],
          [:data_series_2, /^OutSeries2\.csv$/, nil, "Data series 2"],
          [:data_series_3, /^OutSeries3\.csv$/, nil, "Data series 3"],
          [:data_series_4, /^OutSeries4\.csv$/, nil, "Data series 4"],
          [:provenance, /^ProvFile\.txt$/, nil, "Provenance log"],
          [:rom_params, /^param\.txt$/, nil, "Rom parameters"],
          [:output_image_prov, /^OutPutImageProv\.csv$/, nil, "Output image provenance"],
          [:output_meas_prov, /^OutPutMeasProv\.csv$/, nil, "Output meas provenance"]
        ].to_h do |record|
          dft = DataFileType.find_or_initialize_by(data_type: record[0], organization: eurvalve)
          dft.pattern = record[1].source
          dft.name = record[3]
          dft.viewer = record[2]
          dft.save!

          [record[0], dft]
        end
    end

    eurvalve.save!
  end
end
