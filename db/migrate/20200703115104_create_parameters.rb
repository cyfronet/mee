# frozen_string_literal: true

class CreateParameters < ActiveRecord::Migration[6.0]
  def change
    create_table :parameters do |t|
      t.string :slug, null: false
      t.string :label, null: false
      t.string :hint
      t.boolean :required, null: false, default: false
      t.jsonb :config, null: false, default: {}

      t.string :type, null: false

      t.belongs_to :step

      t.timestamps
    end
  end
end
