# frozen_string_literal: true

class CreatePersistentErrors < ActiveRecord::Migration[7.0]
  def change
    create_table :persistent_errors do |t|
      t.string :message, null: false
      t.string :key, null: false
      t.string :child

      t.references :errorable, polymorphic: true
      t.timestamps
    end
  end
end
