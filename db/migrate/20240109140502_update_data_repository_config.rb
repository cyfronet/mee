# frozen_string_literal: true

class UpdateDataRepositoryConfig < ActiveRecord::Migration[7.0]
  def change
    remove_column :organizations, :data_repository_url, :string

    add_column :organizations, :data_repository_config, :jsonb
  end
end
