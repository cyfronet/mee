# frozen_string_literal: true

class ChangeParameteValueLabelAndSlugToNameAndKey < ActiveRecord::Migration[6.0]
  def change
    rename_column :parameter_values, :label, :name
    rename_column :parameter_values, :slug, :key
  end
end
