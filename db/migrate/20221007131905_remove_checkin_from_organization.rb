# frozen_string_literal: true

class RemoveCheckinFromOrganization < ActiveRecord::Migration[7.0]
  def change
    remove_column :organizations, :checkin_enabled, :boolean, default: false, null: false
    remove_column :organizations, :checkin_autoaccept_urn, :string
  end
end
