# frozen_string_literal: true

class RenameSiteUrlToHost < ActiveRecord::Migration[7.0]
  def change
    rename_column :sites, :url, :host
  end
end
