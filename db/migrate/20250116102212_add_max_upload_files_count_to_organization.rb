# frozen_string_literal: true

class AddMaxUploadFilesCountToOrganization < ActiveRecord::Migration[8.0]
  def change
    add_column :organizations, :max_files_upload_count, :integer, default: 20, null: false
  end
end
