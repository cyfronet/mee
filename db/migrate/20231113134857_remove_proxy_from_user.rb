# frozen_string_literal: true

class RemoveProxyFromUser < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :proxy, :text
  end
end
