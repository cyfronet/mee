# frozen_string_literal: true

class AddStepsWithErrorsCountToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :steps_persistent_errors_count, :integer, null: false, default: 0
  end
end
