# frozen_string_literal: true

class RemoveTagOrBranchFromComputation < ActiveRecord::Migration[7.0]
  def change
    remove_column :computations, :tag_or_branch, :string
  end
end
