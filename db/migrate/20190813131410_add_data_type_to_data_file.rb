# frozen_string_literal: true

class AddDataTypeToDataFile < ActiveRecord::Migration[5.2]
  def change
    rename_column :data_files, :data_type, :data_type_enum
    change_column :data_files, :data_type_enum, :integer, null: true
    add_reference :data_files, :data_type, foreign_key: { to_table: :data_file_types }
  end
end
