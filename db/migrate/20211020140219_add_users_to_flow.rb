# frozen_string_literal: true

class AddUsersToFlow < ActiveRecord::Migration[6.1]
  def change
    add_reference :flows, :user, null: true, foreign_key: true
  end
end
