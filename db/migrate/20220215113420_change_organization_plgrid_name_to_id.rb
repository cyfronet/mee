# frozen_string_literal: true

class ChangeOrganizationPlgridNameToId < ActiveRecord::Migration[6.1]
  def change
    rename_column :organizations, :plgrid_team_name, :plgrid_team_id
  end
end
