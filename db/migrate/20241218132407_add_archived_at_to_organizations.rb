# frozen_string_literal: true

class AddArchivedAtToOrganizations < ActiveRecord::Migration[8.0]
  def change
    add_column :organizations, :archived_at, :datetime
    add_index :organizations, :archived_at
  end
end
