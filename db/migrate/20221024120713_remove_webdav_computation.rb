# frozen_string_literal: true

class RemoveWebdavComputation < ActiveRecord::Migration[7.0]
  def up
    ActiveRecord::Base.connection.execute(
      <<~SQL
        DELETE FROM flow_steps
        WHERE step_id IN (SELECT id FROM steps WHERE type = 'WebdavStep')
      SQL
    )

    ActiveRecord::Base.connection.execute(
      <<~SQL
        DELETE FROM prerequisites
        WHERE step_id IN (SELECT id FROM steps WHERE type = 'WebdavStep')
      SQL
    )

    ActiveRecord::Base.connection.execute(
      "DELETE FROM computations WHERE type = 'WebdavComputation'"
    )

    ActiveRecord::Base.connection.execute(
      "DELETE FROM steps WHERE type = 'WebdavStep'"
    )

    remove_column :computations, :run_mode, :string
    remove_column :computations, :input_path, :string
    remove_column :computations, :output_path, :string
    remove_column :computations, :working_file_name, :string
  end
end
