# frozen_string_literal: true

class ChangeNameToNotNullInDataFileType < ActiveRecord::Migration[5.2]
  def change
    change_column :data_file_types, :name, :string, null: false
  end
end
