# frozen_string_literal: true

class CreatePatientUploader < ActiveRecord::Migration[7.0]
  def change
    create_table :patient_uploaders do |t|
      t.references :organization
      t.references :cohort
      t.integer :status, default: 0
      t.string :error_message
      t.timestamps
    end
  end
end
