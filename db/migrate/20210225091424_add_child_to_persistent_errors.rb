# frozen_string_literal: true

class AddChildToPersistentErrors < ActiveRecord::Migration[6.1]
  def change
    add_column :organization_errors, :child, :string
    add_column :step_errors, :child, :string
  end
end
