# frozen_string_literal: true

class RenameTimeScopeGrantFields < ActiveRecord::Migration[7.0]
  def change
    rename_column :grants, :start_at, :starts_at
    rename_column :grants, :end_at, :ends_at
  end
end
