# frozen_string_literal: true

class AddDefaultValueToDesiredPipelinesInCampaign < ActiveRecord::Migration[7.1]
  def change
    change_column_default :campaigns, :desired_pipelines, 0
  end
end
