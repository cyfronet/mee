# frozen_string_literal: true

class RemoveStepNamesFromFlow < ActiveRecord::Migration[5.2]
  def change
    remove_column :flows, :step_names, array: true, default: []
  end
end
