# frozen_string_literal: true

class CreateAllocationSites < ActiveRecord::Migration[8.0]
  def up
    create_table :allocation_sites do |t|
      t.references :site, null: false
      t.references :allocation, null: false
      t.timestamps
    end

    execute(<<~SQL
      WITH ares_id AS (
        SELECT id from sites where name = 'ares'
      ), helios_id AS (
        SELECT id from sites where name = 'helios'
      ), athena_id AS (
        SELECT id from sites where name = 'athena'
      ), cpu_allocations AS (
        SELECT id FROM allocations WHERE name LIKE '%cpu'
      ), gpu_athena AS (
        SELECT id FROM allocations WHERE name LIKE '%gpu-a100'
      ), gpu_helios AS (
        SELECT id FROM allocations WHERE name LIKE '%gpu-gh200'
      )
      INSERT INTO allocation_sites (allocation_id, site_id, created_at, updated_at)
      SELECT allocation_id, site_id, NOW(), NOW() FROM (
        SELECT cpu_allocations.id AS allocation_id, ares_id.id AS site_id FROM cpu_allocations, ares_id
        UNION
        SELECT cpu_allocations.id AS allocation_id, helios_id.id AS site_id  FROM cpu_allocations, helios_id
        UNION
        SELECT gpu_helios.id AS allocation_id, helios_id.id AS site_id FROM gpu_helios, helios_id
        UNION
        SELECT gpu_athena.id AS allocation_id, athena_id.id AS site_id  FROM gpu_athena, athena_id
        ) AS combined_attrs
    SQL
           )
  end

  def down
    drop_table :allocation_sites
  end
end
