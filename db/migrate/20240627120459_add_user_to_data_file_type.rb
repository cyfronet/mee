# frozen_string_literal: true

class AddUserToDataFileType < ActiveRecord::Migration[7.1]
  def change
    add_reference :data_file_types, :user, foreign_key: true
  end
end
