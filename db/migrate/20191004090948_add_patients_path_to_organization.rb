# frozen_string_literal: true

class AddPatientsPathToOrganization < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations, :patients_path, :string
  end
end
