# frozen_string_literal: true

class AddWaitForPreviousToFlowStep < ActiveRecord::Migration[7.0]
  def change
    add_column :flow_steps, :wait_for_previous, :boolean, default: false
  end
end
