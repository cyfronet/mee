# frozen_string_literal: true

class AddFlowIdToPipelines < ActiveRecord::Migration[5.2]
  def up
    rename_column :pipelines, :flow, :flow_name
    add_reference :pipelines, :flow, index: true

    change_column_null :pipelines, :flow_id, false
    remove_column :pipelines, :flow_name
  end

  def down
    add_column :pipelines, :flow, :string, default: "full_body_scan", null: false

    remove_reference :pipelines, :flow
  end
end
