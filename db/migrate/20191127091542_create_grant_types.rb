# frozen_string_literal: true

class CreateGrantTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :grant_types do |t|
      t.string :name, null: false

      t.timestamps
    end

    add_index :grant_types, :name, unique: true
  end
end
