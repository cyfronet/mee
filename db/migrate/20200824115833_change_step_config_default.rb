# frozen_string_literal: true

class ChangeStepConfigDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:steps, :config, from: nil, to: {})
  end
end
