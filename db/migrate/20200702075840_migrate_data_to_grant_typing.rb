# frozen_string_literal: true

class MigrateDataToGrantTyping < ActiveRecord::Migration[6.0]
  def up
    execute("INSERT INTO grant_typings (grant_id, grant_type_id, created_at, updated_at)
            SELECT id, grant_type_id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
            FROM grants")
    remove_reference :grants, :grant_type
  end

  def down
    add_reference :grants, :grant_type, index: true, foreign_key: true, null: true
    execute("UPDATE grants
            SET grant_type_id = grant_typings.grant_type_id
            FROM grant_typings
            WHERE grants.id = grant_typings.grant_id")
    change_column_null :grants, :grant_type_id, false
  end
end
