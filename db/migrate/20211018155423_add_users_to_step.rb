# frozen_string_literal: true

class AddUsersToStep < ActiveRecord::Migration[6.1]
  def change
    add_reference :steps, :user, null: true, foreign_key: true
  end
end
