# frozen_string_literal: true

class AddNullFalseToStepsGrantTypeId < ActiveRecord::Migration[7.0]
  def change
    change_column_null :steps, :grant_type_id, false
  end
end
