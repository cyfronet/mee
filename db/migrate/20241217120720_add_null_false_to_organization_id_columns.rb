# frozen_string_literal: true

class AddNullFalseToOrganizationIdColumns < ActiveRecord::Migration[8.0]
  def change
    change_column_null :steps, :organization_id, false
    change_column_null :flows, :organization_id, false
    change_column_null :patients, :organization_id, false
    change_column_null :patient_uploaders, :organization_id, false
  end
end
