# frozen_string_literal: true

class RenameDataverseToDataRegistryIntegration < ActiveRecord::Migration[7.0]
  def change
    rename_column :organizations, :dataverse_url, :data_repository_url
    rename_column :memberships, :dataverse_token, :data_repository_token
  end
end
