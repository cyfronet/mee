# frozen_string_literal: true

class AddOrganizationToDataFileType < ActiveRecord::Migration[5.2]
  def change
    add_reference :data_file_types, :organization, foreign_key: true, index: true
  end
end
