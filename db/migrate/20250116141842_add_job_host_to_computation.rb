# frozen_string_literal: true

class AddJobHostToComputation < ActiveRecord::Migration[8.0]
  def change
    add_column :computations, :job_host, :string, null: true

    up_only do
      execute <<~SQL
        UPDATE computations
          SET job_host = sites.host FROM steps
            INNER JOIN sites ON steps.site_id = sites.id
            WHERE computations.step_id = steps.id
      SQL
    end
  end
end
