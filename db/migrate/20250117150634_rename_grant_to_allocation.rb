# frozen_string_literal: true

class RenameGrantToAllocation < ActiveRecord::Migration[8.0]
  def up
    rename_table :grants, :allocations
    rename_table :grant_typings, :allocation_typings
    rename_table :grant_types, :resource_types

    rename_column :allocation_typings, :grant_id, :allocation_id
    rename_column :allocation_typings, :grant_type_id, :resource_type_id
    rename_column :steps, :grant_type_id, :resource_type_id

    execute <<~SQL
      UPDATE parameters SET type = 'Parameter::Allocation', key = 'allocation', name = 'Allocation' WHERE type = 'Parameter::Grant';
      UPDATE parameter_values SET type = 'ParameterValue::Allocation', key = 'allocation', name = 'Allocation' WHERE type = 'ParameterValue::Grant';
    SQL
  end

  def down
    rename_table :allocations, :grants
    rename_table :allocation_typings, :grant_typings
    rename_table :resource_types, :grant_types

    rename_column :grant_typings, :allocation_id, :grant_id
    rename_column :grant_typings, :resource_type_id, :grant_type_id
    rename_column :steps, :resource_type_id, :grant_type_id

    execute <<~SQL
      UPDATE parameters SET type = 'Parameter::Grant', key = 'grant', name = 'Grant' WHERE type = 'Parameter::Allocation';
      UPDATE parameter_values SET type = 'ParameterValue::Grant', key = 'grant', name = 'Grant' WHERE type = 'ParameterValue::Allocation';
    SQL
  end
end
