# frozen_string_literal: true

class ChangeStepNameTypeAndMakeItUnique < ActiveRecord::Migration[6.1]
  def change
    enable_extension :citext
    change_column :steps, :name, :citext
    add_index :steps, [:name, :organization_id], unique: true
  end
end
