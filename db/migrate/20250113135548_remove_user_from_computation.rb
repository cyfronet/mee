# frozen_string_literal: true

class RemoveUserFromComputation < ActiveRecord::Migration[8.0]
  def change
    remove_reference :computations, :user
  end
end
