# frozen_string_literal: true

class RenameOrderToPoisitionInFlow < ActiveRecord::Migration[5.2]
  def change
    rename_column :flows, :order, :position
  end
end
