# frozen_string_literal: true

class ChangeOrganizationPatientsPathToPath < ActiveRecord::Migration[7.0]
  def change
    rename_column :organizations, :patients_path, :path
  end
end
