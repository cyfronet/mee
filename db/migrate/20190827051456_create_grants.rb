# frozen_string_literal: true

class CreateGrants < ActiveRecord::Migration[5.2]
  def change
    create_table :grants do |t|
      t.string :name, null: false
      t.date :start_at, null: false
      t.date :end_at, null: false
      t.belongs_to :organization, foreign_key: true, null: false

      t.timestamps
    end
  end
end
