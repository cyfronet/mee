# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2025_02_10_141422) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "pg_catalog.plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "allocation_sites", force: :cascade do |t|
    t.bigint "site_id", null: false
    t.bigint "allocation_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["allocation_id"], name: "index_allocation_sites_on_allocation_id"
    t.index ["site_id"], name: "index_allocation_sites_on_site_id"
  end

  create_table "allocation_typings", force: :cascade do |t|
    t.bigint "allocation_id", null: false
    t.bigint "resource_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["allocation_id", "resource_type_id"], name: "index_allocation_typings_on_allocation_id_and_resource_type_id", unique: true
    t.index ["allocation_id"], name: "index_allocation_typings_on_allocation_id"
    t.index ["resource_type_id"], name: "index_allocation_typings_on_resource_type_id"
  end

  create_table "allocations", force: :cascade do |t|
    t.string "name", null: false
    t.date "starts_at", null: false
    t.date "ends_at", null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["organization_id"], name: "index_allocations_on_organization_id"
  end

  create_table "artifacts", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_artifacts_on_name", unique: true
    t.index ["organization_id"], name: "index_artifacts_on_organization_id"
  end

  create_table "campaigns", force: :cascade do |t|
    t.bigint "cohort_id"
    t.string "name", null: false
    t.bigint "organization_id", null: false
    t.integer "failed_pipelines", default: 0, null: false
    t.integer "desired_pipelines", default: 0, null: false
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cohort_id"], name: "index_campaigns_on_cohort_id"
    t.index ["organization_id"], name: "index_campaigns_on_organization_id"
  end

  create_table "cohort_patients", force: :cascade do |t|
    t.bigint "cohort_id", null: false
    t.bigint "patient_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cohort_id"], name: "index_cohort_patients_on_cohort_id"
    t.index ["patient_id", "cohort_id"], name: "index_cohort_patients_on_patient_id_and_cohort_id", unique: true
    t.index ["patient_id"], name: "index_cohort_patients_on_patient_id"
  end

  create_table "cohorts", force: :cascade do |t|
    t.string "name"
    t.bigint "organization_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "campaigns_count", default: 0, null: false
    t.index ["organization_id"], name: "index_cohorts_on_organization_id"
  end

  create_table "computations", id: :serial, force: :cascade do |t|
    t.string "job_id"
    t.text "script"
    t.string "working_directory"
    t.string "stdout_path"
    t.string "stderr_path"
    t.text "standard_output"
    t.text "error_output"
    t.string "error_message"
    t.integer "exit_code"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "pipeline_step"
    t.integer "pipeline_id"
    t.datetime "started_at", precision: nil
    t.string "revision"
    t.bigint "step_id"
    t.string "secret"
    t.integer "status", default: 0
    t.string "job_host"
    t.index ["pipeline_id"], name: "index_computations_on_pipeline_id"
    t.index ["step_id"], name: "index_computations_on_step_id"
  end

  create_table "data_file_types", force: :cascade do |t|
    t.string "data_type", null: false
    t.text "pattern", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "name", null: false
    t.string "viewer"
    t.bigint "organization_id", null: false
    t.bigint "user_id"
    t.index ["organization_id"], name: "index_data_file_types_on_organization_id"
    t.index ["user_id"], name: "index_data_file_types_on_user_id"
  end

  create_table "flow_steps", force: :cascade do |t|
    t.bigint "flow_id", null: false
    t.bigint "step_id", null: false
    t.integer "position", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "wait_for_previous", default: false
    t.index ["flow_id", "step_id"], name: "index_flow_steps_on_flow_id_and_step_id", unique: true
    t.index ["flow_id"], name: "index_flow_steps_on_flow_id"
    t.index ["position"], name: "index_flow_steps_on_position"
    t.index ["step_id"], name: "index_flow_steps_on_step_id"
  end

  create_table "flows", force: :cascade do |t|
    t.string "name", null: false
    t.integer "position", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "organization_id", null: false
    t.datetime "discarded_at", precision: nil
    t.bigint "user_id"
    t.index ["discarded_at"], name: "index_flows_on_discarded_at"
    t.index ["name"], name: "index_flows_on_name"
    t.index ["organization_id"], name: "index_flows_on_organization_id"
    t.index ["position"], name: "index_flows_on_position"
    t.index ["user_id"], name: "index_flows_on_user_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at", precision: nil
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "licenses", force: :cascade do |t|
    t.string "name", null: false
    t.date "starts_at", null: false
    t.date "ends_at", null: false
    t.jsonb "config", default: {}, null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["organization_id"], name: "index_licenses_on_organization_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "roles_mask"
    t.integer "state", default: 0, null: false
    t.bigint "user_id", null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "data_repository_token"
    t.index ["organization_id"], name: "index_memberships_on_organization_id"
    t.index ["user_id", "organization_id"], name: "index_memberships_on_user_id_and_organization_id", unique: true
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "description"
    t.integer "persistent_errors_count", default: 0, null: false
    t.integer "steps_persistent_errors_count", default: 0, null: false
    t.jsonb "git_config"
    t.string "plgrid_team_id", null: false
    t.boolean "migrated", default: false
    t.integer "storage_quota", default: 5, null: false
    t.jsonb "data_repository_config"
    t.datetime "archived_at"
    t.integer "max_files_upload_count", default: 20, null: false
    t.index ["archived_at"], name: "index_organizations_on_archived_at"
    t.index ["slug"], name: "index_organizations_on_slug", unique: true
  end

  create_table "parameter_values", force: :cascade do |t|
    t.string "key", null: false
    t.string "name", null: false
    t.jsonb "value_store", default: {}, null: false
    t.string "type"
    t.bigint "computation_id"
    t.bigint "parameter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["computation_id"], name: "index_parameter_values_on_computation_id"
    t.index ["parameter_id"], name: "index_parameter_values_on_parameter_id"
  end

  create_table "parameters", force: :cascade do |t|
    t.string "key", null: false
    t.string "name", null: false
    t.string "hint"
    t.boolean "required", default: false, null: false
    t.jsonb "config", default: {}, null: false
    t.string "type", null: false
    t.bigint "step_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["step_id"], name: "index_parameters_on_step_id"
  end

  create_table "patient_uploaders", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "cohort_id"
    t.integer "status", default: 0
    t.string "error_message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cohort_id"], name: "index_patient_uploaders_on_cohort_id"
    t.index ["organization_id"], name: "index_patient_uploaders_on_organization_id"
  end

  create_table "patients", id: :serial, force: :cascade do |t|
    t.string "case_number", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "organization_id", null: false
    t.string "slug"
    t.index ["case_number", "organization_id"], name: "index_patients_on_case_number_and_organization_id", unique: true
    t.index ["organization_id"], name: "index_patients_on_organization_id"
    t.index ["slug", "organization_id"], name: "index_patients_on_slug_and_organization_id", unique: true
  end

  create_table "persistent_errors", force: :cascade do |t|
    t.string "message", null: false
    t.string "key", null: false
    t.string "child"
    t.string "errorable_type"
    t.bigint "errorable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["errorable_type", "errorable_id"], name: "index_persistent_errors_on_errorable"
  end

  create_table "pipelines", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.integer "iid", null: false
    t.integer "runnable_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "mode", default: 0, null: false
    t.bigint "flow_id", null: false
    t.string "runnable_type", null: false
    t.bigint "campaign_id"
    t.integer "status", default: 0
    t.index ["campaign_id"], name: "index_pipelines_on_campaign_id"
    t.index ["flow_id"], name: "index_pipelines_on_flow_id"
    t.index ["iid"], name: "index_pipelines_on_iid"
    t.index ["runnable_id", "runnable_type", "iid"], name: "index_pipelines_on_runnable_id_and_runnable_type_and_iid", unique: true
    t.index ["runnable_id", "runnable_type"], name: "index_pipelines_on_runnable_id_and_runnable_type"
    t.index ["user_id"], name: "index_pipelines_on_user_id"
  end

  create_table "prerequisites", force: :cascade do |t|
    t.bigint "step_id", null: false
    t.bigint "data_file_type_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["data_file_type_id"], name: "index_prerequisites_on_data_file_type_id"
    t.index ["step_id", "data_file_type_id"], name: "index_prerequisites_on_step_id_and_data_file_type_id", unique: true
    t.index ["step_id"], name: "index_prerequisites_on_step_id"
  end

  create_table "resource_types", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["name"], name: "index_resource_types_on_name", unique: true
  end

  create_table "sites", force: :cascade do |t|
    t.string "name", null: false
    t.string "host", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "host_key", null: false
  end

  create_table "steps", force: :cascade do |t|
    t.citext "name", null: false
    t.string "slug", null: false
    t.jsonb "config", default: {}, null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "resource_type_id", null: false
    t.integer "persistent_errors_count", default: 0, null: false
    t.jsonb "git_config"
    t.jsonb "repo_config", default: {}, null: false
    t.datetime "discarded_at", precision: nil
    t.bigint "user_id"
    t.bigint "site_id"
    t.index ["discarded_at"], name: "index_steps_on_discarded_at"
    t.index ["name", "organization_id"], name: "index_steps_on_name_and_organization_id", unique: true
    t.index ["organization_id"], name: "index_steps_on_organization_id"
    t.index ["resource_type_id"], name: "index_steps_on_resource_type_id"
    t.index ["site_id"], name: "index_steps_on_site_id"
    t.index ["slug", "organization_id"], name: "index_steps_on_slug_and_organization_id", unique: true
    t.index ["user_id"], name: "index_steps_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "plgrid_login"
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.datetime "credentials_expired_at", precision: nil
    t.integer "roles_mask"
    t.text "ssh_key"
    t.text "ssh_certificate"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["plgrid_login"], name: "index_users_on_plgrid_login", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "allocations", "organizations"
  add_foreign_key "artifacts", "organizations"
  add_foreign_key "campaigns", "cohorts"
  add_foreign_key "campaigns", "organizations"
  add_foreign_key "cohort_patients", "cohorts"
  add_foreign_key "cohort_patients", "patients"
  add_foreign_key "cohorts", "organizations"
  add_foreign_key "computations", "pipelines"
  add_foreign_key "computations", "steps"
  add_foreign_key "data_file_types", "organizations"
  add_foreign_key "data_file_types", "users"
  add_foreign_key "flow_steps", "flows"
  add_foreign_key "flow_steps", "steps"
  add_foreign_key "flows", "organizations"
  add_foreign_key "flows", "users"
  add_foreign_key "licenses", "organizations"
  add_foreign_key "patients", "organizations"
  add_foreign_key "pipelines", "campaigns"
  add_foreign_key "prerequisites", "data_file_types"
  add_foreign_key "prerequisites", "steps"
  add_foreign_key "steps", "resource_types"
  add_foreign_key "steps", "sites"
  add_foreign_key "steps", "users"
end
